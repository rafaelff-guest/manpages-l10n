# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-02-10 09:31+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SHMOP"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "2019-08-02"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "shmat, shmdt - System V shared memory operations"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/shm.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<void *shmat(int >I<shmid>B<, const void *>I<shmaddr>B<, int >I<shmflg>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<int shmdt(const void *>I<shmaddr>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "shmat()"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<shmat>()  attaches the System\\ V shared memory segment identified by "
"I<shmid> to the address space of the calling process.  The attaching address "
"is specified by I<shmaddr> with one of the following criteria:"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "*"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If I<shmaddr> is NULL, the system chooses a suitable (unused) page-aligned "
"address to attach the segment."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If I<shmaddr> isn't NULL and B<SHM_RND> is specified in I<shmflg>, the "
"attach occurs at the address equal to I<shmaddr> rounded down to the nearest "
"multiple of B<SHMLBA>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Otherwise, I<shmaddr> must be a page-aligned address at which the attach "
"occurs."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"In addition to B<SHM_RND>, the following flags may be specified in the "
"I<shmflg> bit-mask argument:"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<SHM_EXEC> (Linux-specific; since Linux 2.6.9)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Allow the contents of the segment to be executed.  The caller must have "
"execute permission on the segment."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<SHM_RDONLY>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Attach the segment for read-only access.  The process must have read "
"permission for the segment.  If this flag is not specified, the segment is "
"attached for read and write access, and the process must have read and write "
"permission for the segment.  There is no notion of a write-only shared "
"memory segment."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<SHM_REMAP> (Linux-specific)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This flag specifies that the mapping of the segment should replace any "
"existing mapping in the range starting at I<shmaddr> and continuing for the "
"size of the segment.  (Normally, an B<EINVAL> error would result if a "
"mapping already exists in this address range.)  In this case, I<shmaddr> "
"must not be NULL."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<brk>(2)  value of the calling process is not altered by the attach.  "
"The segment will automatically be detached at process exit.  The same "
"segment may be attached as a read and as a read-write one, and more than "
"once, in the process's address space."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"A successful B<shmat>()  call updates the members of the I<shmid_ds> "
"structure (see B<shmctl>(2))  associated with the shared memory segment as "
"follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<shm_atime> is set to the current time."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<shm_lpid> is set to the process-ID of the calling process."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<shm_nattch> is incremented by one."
msgstr ""

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "shmdt()"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<shmdt>()  detaches the shared memory segment located at the address "
"specified by I<shmaddr> from the address space of the calling process.  The "
"to-be-detached segment must be currently attached with I<shmaddr> equal to "
"the value returned by the attaching B<shmat>()  call."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"On a successful B<shmdt>()  call, the system updates the members of the "
"I<shmid_ds> structure associated with the shared memory segment as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<shm_dtime> is set to the current time."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"I<shm_nattch> is decremented by one.  If it becomes 0 and the segment is "
"marked for deletion, the segment is deleted."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"On success, B<shmat>()  returns the address of the attached shared memory "
"segment; on error, I<(void\\ *)\\ -1> is returned, and I<errno> is set to "
"indicate the cause of the error."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"On success, B<shmdt>()  returns 0; on error -1 is returned, and I<errno> is "
"set to indicate the cause of the error."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "When B<shmat>()  fails, I<errno> is set to one of the following:"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EACCES>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The calling process does not have the required permissions for the requested "
"attach type, and does not have the B<CAP_IPC_OWNER> capability in the user "
"namespace that governs its IPC namespace."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EIDRM>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<shmid> points to a removed identifier."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Invalid I<shmid> value, unaligned (i.e., not page-aligned and B<SHM_RND> was "
"not specified) or invalid I<shmaddr> value, or can't attach segment at "
"I<shmaddr>, or B<SHM_REMAP> was specified and I<shmaddr> was NULL."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Could not allocate memory for the descriptor or for the page tables."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "When B<shmdt>()  fails, I<errno> is set as follows:"
msgstr ""

#.  The following since 2.6.17-rc1:
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"There is no shared memory segment attached at I<shmaddr>; or, I<shmaddr> is "
"not aligned on a page boundary."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#.  SVr4 documents an additional error condition EMFILE.
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, SVr4."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"In SVID 3 (or perhaps earlier), the type of the I<shmaddr> argument was "
"changed from I<char\\ *> into I<const void\\ *>, and the returned type of "
"B<shmat>()  from I<char\\ *> into I<void\\ *>."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"After a B<fork>(2), the child inherits the attached shared memory segments."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"After an B<execve>(2), all attached shared memory segments are detached from "
"the process."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Upon B<_exit>(2), all attached shared memory segments are detached from the "
"process."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Using B<shmat>()  with I<shmaddr> equal to NULL is the preferred, portable "
"way of attaching a shared memory segment.  Be aware that the shared memory "
"segment attached in this way may be attached at different addresses in "
"different processes.  Therefore, any pointers maintained within the shared "
"memory must be made relative (typically to the starting address of the "
"segment), rather than absolute."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"On Linux, it is possible to attach a shared memory segment even if it is "
"already marked to be deleted.  However, POSIX.1 does not specify this "
"behavior and many other implementations do not support it."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The following system parameter affects B<shmat>():"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<SHMLBA>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Segment low boundary address multiple.  When explicitly specifying an attach "
"address in a call to B<shmat>(), the caller should ensure that the address "
"is a multiple of this value.  This is necessary on some architectures, in "
"order either to ensure good CPU cache performance or to ensure that "
"different attaches of the same segment have consistent views within the CPU "
"cache.  B<SHMLBA> is normally some multiple of the system page size.  (On "
"many Linux architectures, B<SHMLBA> is the same as the system page size.)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The implementation places no intrinsic per-process limit on the number of "
"shared memory segments (B<SHMSEG>)."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"B<brk>(2), B<mmap>(2), B<shmctl>(2), B<shmget>(2), B<capabilities>(7), "
"B<shm_overview>(7), B<sysvipc>(7)"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: archlinux debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"This page is part of release 5.05 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2017-09-15"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"B<brk>(2), B<mmap>(2), B<shmctl>(2), B<shmget>(2), B<capabilities>(7), "
"B<shm_overview>(7), B<svipc>(7)"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
