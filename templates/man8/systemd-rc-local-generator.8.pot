# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-01-05 22:00+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYSTEMD-RC-LOCAL-GENERATOR"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "systemd 244"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "systemd-rc-local-generator"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"systemd-rc-local-generator - Compatibility generator for starting /etc/rc\\&."
"local during boot"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "/usr/lib/systemd/system-generators/systemd-rc-local-generator"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"systemd-rc-local-generator is a generator that checks whether /etc/rc\\&."
"local exists and is executable, and if it is pulls the rc-local\\&.service "
"unit into the boot process\\&. This unit is responsible for running this "
"script during late boot\\&. Note that the script will be run with slightly "
"different semantics than the original System V version, which was run \"last"
"\" in the boot process, which is a concept that does not translate to systemd"
"\\&. The script is run after network\\&.target, but in parallel with most "
"other regular system services\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"Support for /etc/rc\\&.local is provided for compatibility with specific "
"System V systems only\\&. However, it is strongly recommended to avoid "
"making use of this script today, and instead provide proper unit files with "
"appropriate dependencies for any scripts to run during the boot process\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "systemd-rc-local-generator implements B<systemd.generator>(7)\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<systemd>(1), B<systemctl>(1)"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "/lib/systemd/system-generators/systemd-rc-local-generator"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 241"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"systemd-rc-local-generator - Compatibility generator for starting /etc/rc\\&."
"local and /usr/sbin/halt\\&.local during boot and shutdown"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"systemd-rc-local-generator also checks whether /usr/sbin/halt\\&.local "
"exists and is executable, and if it is pulls the halt-local\\&.service unit "
"into the shutdown process\\&. This unit is responsible for running this "
"script during later shutdown\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Support for both /etc/rc\\&.local and /usr/sbin/halt\\&.local is provided "
"for compatibility with specific System V systems only\\&. However, it is "
"strongly recommended to avoid making use of these scripts today, and instead "
"provide proper unit files with appropriate dependencies for any scripts to "
"run during the boot or shutdown processes\\&."
msgstr ""
