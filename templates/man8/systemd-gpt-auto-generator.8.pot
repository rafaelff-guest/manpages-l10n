# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-01-05 21:59+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYSTEMD-GPT-AUTO-GENERATOR"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "systemd 244"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "systemd-gpt-auto-generator"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"systemd-gpt-auto-generator - Generator for automatically discovering and "
"mounting root, /home and /srv partitions, as well as discovering and "
"enabling swap partitions, based on GPT partition type GUIDs\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "/usr/lib/systemd/system-generators/systemd-gpt-auto-generator"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"systemd-gpt-auto-generator is a unit generator that automatically discovers "
"root, /home/, /srv/, the EFI System Partition, the Extended Boot Loader "
"Partition and swap partitions and creates mount and swap units for them, "
"based on the partition type GUIDs of GUID partition tables (GPT), see "
"\\m[blue]B<UEFI Specification>\\m[]\\&\\s-2\\u[1]\\d\\s+2, chapter 5\\&. It "
"implements the \\m[blue]B<Discoverable Partitions Specification>\\m[]\\&"
"\\s-2\\u[2]\\d\\s+2\\&. Note that this generator has no effect on non-GPT "
"systems, and on specific mount points that are directories already "
"containing files\\&. Also, on systems where the units are explicitly "
"configured (for example, listed in B<fstab>(5)), the units this generator "
"creates are overridden, but additional implicit dependencies might be created"
"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This generator will only look for root partitions on the same physical disk "
"the EFI System Partition (ESP) is located on\\&. It will only look for the "
"other partitions on the same physical disk the root file system is located on"
"\\&. These partitions will not be searched for on systems where the root "
"file system is distributed on multiple disks, for example via btrfs RAID\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"systemd-gpt-auto-generator is useful for centralizing file system "
"configuration in the partition table and making configuration in /etc/fstab "
"unnecessary\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This generator looks for the partitions based on their partition type GUID"
"\\&. The following partition type GUIDs are identified:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<Table\\ \\&1.\\ \\&Partition Type GUIDs>"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Partition Type GUID"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Name"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "Mount Point"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Explanation"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ".T&"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "l l l l"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "l l l l."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "44479540-f297-41b2-9af7-d131d5f0458a"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Root Partition (x86)"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "/"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "On 32-bit x86 systems, the first x86 root partition on the disk the EFI ESP is located on is mounted to the root directory /\\&."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "4f68bce3-e8cd-4db1-96e7-fbcaf984b709"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Root Partition (x86-64)"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "On 64-bit x86 systems, the first x86-64 root partition on the disk the EFI ESP is located on is mounted to the root directory /\\&."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "69dad710-2ce4-4e3c-b16c-21a1d49abed3"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Root Partition (32-bit ARM)"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "On 32-bit ARM systems, the first ARM root partition on the disk the EFI ESP is located on is mounted to the root directory /\\&."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "b921b045-1df0-41c3-af44-4c6f280d3fae"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Root Partition (64-bit ARM)"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "On 64-bit ARM systems, the first ARM root partition on the disk the EFI ESP is located on is mounted to the root directory /\\&."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "993d8d3d-f80e-4225-855a-9daf8ed7ea97"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Root Partition (Itanium/IA-64)"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "On Itanium systems, the first Itanium root partition on the disk the EFI ESP is located on is mounted to the root directory /\\&."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "933ac7e1-2eb4-4f13-b844-0e14e2aef915"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Home Partition"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "/home/"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "The first home partition on the disk the root partition is located on is mounted to /home\\&."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "3b8f8425-20e0-4f3b-907f-1a25a76f98e8"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Server Data Partition"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "/srv/"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "The first server data partition on the disk the root partition is located on is mounted to /srv\\&."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "0657fd6d-a4ab-43c4-84e5-0933c84b4f4f"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Swap"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "n/a"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "All swap partitions located on the disk the root partition is located on are enabled\\&."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "c12a7328-f81f-11d2-ba4b-00a0c93ec93b"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EFI System Partition (ESP)"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "/efi/ or /boot/"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "The first ESP located on the disk the root partition is located on is mounted to /boot or /efi, see below\\&."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "bc13c2ff-59e6-4262-a352-b275fd6f7172"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "Extended Boot Loader Partition"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "/boot/"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "The first Extended Boot Loader Partition is mounted to /boot, see below\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This generator understands the following attribute flags for partitions:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<Table\\ \\&2.\\ \\&Partition Attributes>"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Applicable to"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<GPT_FLAG_READ_ONLY>"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "0x1000000000000000"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "/, /home/, /srv/, Extended Boot Loader Partition"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Partition is mounted read-only"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<GPT_FLAG_NO_AUTO>"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "0x8000000000000000"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Partition is not mounted automatically"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<GPT_FLAG_NO_BLOCK_IO_PROTOCOL>"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "0x0000000000000002"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"The /home/ and /srv/ partitions may be encrypted in LUKS format\\&. In this "
"case, a device mapper device is set up under the names /dev/mapper/home and /"
"dev/mapper/srv\\&. Note that this might create conflicts if the same "
"partition is listed in /etc/crypttab with a different device mapper device "
"name\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"When systemd is running in the initrd the / partition may be encrypted in "
"LUKS format as well\\&. In this case, a device mapper device is set up under "
"the name /dev/mapper/root, and a sysroot\\&.mount is set up that mounts the "
"device under /sysroot\\&. For more information, see B<bootup>(7)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"Mount and automount units for the EFI System Partition (ESP) are generated "
"on EFI systems\\&. The ESP is mounted to /boot/ (except if an Extended Boot "
"Loader partition exists, see below), unless a mount point directory /efi/ "
"exists, in which case it is mounted there\\&. Since this generator creates "
"an automount unit, the mount will only be activated on-demand, when accessed"
"\\&. On systems where /boot/ (or /efi/ if it exists) is an explicitly "
"configured mount (for example, listed in B<fstab>(5)) or where the /boot/ "
"(or /efi/) mount point is non-empty, no mount units are generated\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"If the disk contains an Extended Boot Loader partition, as defined in the "
"\\m[blue]B<Boot Loader Specification>\\m[]\\&\\s-2\\u[3]\\d\\s+2, it is made "
"available at /boot (by means of an automount point, similar to the ESP, see "
"above)\\&. If both an EFI System Partition and an Extended Boot Loader "
"partition exist the latter is preferably mounted to /boot/\\&. Make sure to "
"create both /efi/ and /boot/ to ensure both partitions are mounted\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"When using this generator in conjunction with btrfs file systems, make sure "
"to set the correct default subvolumes on them, using B<btrfs subvolume set-"
"default>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "systemd-gpt-auto-generator implements B<systemd.generator>(7)\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "KERNEL COMMAND LINE"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"systemd-gpt-auto-generator understands the following kernel command line "
"parameters:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid "I<systemd\\&.gpt_auto>, I<rd\\&.systemd\\&.gpt_auto>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"Those options take an optional boolean argument, and default to yes\\&. The "
"generator is enabled by default, and a negative value may be used to disable "
"it\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid "I<root=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"When used with the special value \"gpt-auto\", automatic discovery of the "
"root partition based on the GPT partition type is enabled\\&. Any other "
"value disables this generator\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid "I<rw>, I<ro>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid "Mount the root partition read-write or read-only I<initially>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"Note that unlike most kernel command line options these settings do not "
"override configuration in the file system, and the file system may be "
"remounted later\\&. See B<systemd-remount-fs.service>(8)\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<systemd>(1), B<systemd.mount>(5), B<systemd.swap>(5), B<systemd-fstab-"
"generator>(8), B<systemd-cryptsetup@.service>(8), B<cryptsetup>(8), "
"B<fstab>(5), B<btrfs>(8)"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "UEFI Specification"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid "\\%https://uefi.org/specifications"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Discoverable Partitions Specification"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"\\%https://www.freedesktop.org/wiki/Specifications/"
"DiscoverablePartitionsSpec/"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid " 3."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid "Boot Loader Specification"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid "\\%https://systemd.io/BOOT_LOADER_SPECIFICATION"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "/lib/systemd/system-generators/systemd-gpt-auto-generator"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 241"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"systemd-gpt-auto-generator is a unit generator that automatically discovers "
"root, /home, /srv and swap partitions and creates mount and swap units for "
"them, based on the partition type GUIDs of GUID partition tables (GPT), see "
"\\m[blue]B<UEFI Specification>\\m[]\\&\\s-2\\u[1]\\d\\s+2, chapter 5\\&. It "
"implements the \\m[blue]B<Discoverable Partitions Specification>\\m[]\\&"
"\\s-2\\u[2]\\d\\s+2\\&. Note that this generator has no effect on non-GPT "
"systems, or where the directories under the mount points are already non-"
"empty\\&. Also, on systems where the units are explicitly configured (for "
"example, listed in B<fstab>(5)), the units this generator creates are "
"overridden, but additional implicit dependencies might be created\\&."
msgstr ""

#. type: tbl table
#: mageia-cauldron
#, no-wrap
msgid "l l l"
msgstr ""

#. type: tbl table
#: mageia-cauldron
#, no-wrap
msgid "l l l."
msgstr ""

#. type: tbl table
#: mageia-cauldron
#, no-wrap
msgid "/, /srv, /home"
msgstr ""

#. type: tbl table
#: mageia-cauldron
#, no-wrap
msgid "ESP"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The /home and /srv partitions may be encrypted in LUKS format\\&. In this "
"case, a device mapper device is set up under the names /dev/mapper/home and /"
"dev/mapper/srv\\&. Note that this might create conflicts if the same "
"partition is listed in /etc/crypttab with a different device mapper device "
"name\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Mount and automount units for the EFI System Partition (ESP) are generated "
"on EFI systems\\&. The ESP is mounted to /boot, unless a mount point "
"directory /efi exists, in which case it is mounted there\\&. Since this "
"generator creates an automount unit, the mount will only be activated on-"
"demand, when accessed\\&. On systems where /boot (or /efi if it exists) is "
"an explicitly configured mount (for example, listed in B<fstab>(5)) or where "
"the /boot (or /efi) mount point is non-empty, no mount units are generated"
"\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "\\%http://www.uefi.org/specifications"
msgstr ""
