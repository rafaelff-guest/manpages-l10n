# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-01-25 22:27+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "NOLOGIN"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "November 2019"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "nologin - politely refuse a login"
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<nologin> [B<-V>] [B<-h>]"
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"B<nologin> displays a message that an account is not available and exits non-"
"zero.  It is intended as a replacement shell field to deny login access to "
"an account."
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"If the file /etc/nologin.txt exists, nologin displays its contents to the "
"user instead of the default message."
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "The exit code returned by B<nologin> is always 1."
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<-c>, B<--command> I<command>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<--init-file>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<-i> B<--interactive>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<--init-file> I<file>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<-i>, B<--interactive>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<-l>, B<--login>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<--noprofile>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<--norc>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<--posix>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<--rcfile> I<file>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<-r>, B<--restricted>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "These shell command-line options are ignored to avoid nologin error."
msgstr ""

#. type: IP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "Display help text and exit."
msgstr ""

#. type: IP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "Display version information and exit."
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"B<nologin> is a per-account way to disable login (usually used for system "
"accounts like http or ftp).  B<nologin>(8)  uses /etc/nologin.txt as an "
"optional source for a non-default message, the login access is always "
"refused independently of the file."
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"B<pam_nologin>(8)  PAM module usually prevents all non-root users from "
"logging into the system.  B<pam_nologin>(8)  functionality is controlled by /"
"var/run/nologin or the /etc/nologin file."
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "E<.UR kzak@redhat.com> Karel Zak E<.UE>"
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<login>(1), B<passwd>(5), B<pam_nologin>(8)"
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "The B<nologin> command appeared in 4.4BSD."
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"The nologin command is part of the util-linux package and is available from "
"E<.UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> "
"Linux Kernel Archive E<.UE .>"
msgstr ""
