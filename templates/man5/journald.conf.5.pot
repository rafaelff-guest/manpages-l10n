# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-02-08 16:57+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "JOURNALD\\&.CONF"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable
#, no-wrap
msgid "systemd 244"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "journald.conf"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "journald.conf, journald.conf.d - Journal service configuration files"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "/etc/systemd/journald\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "/etc/systemd/journald\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "/run/systemd/journald\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "/usr/lib/systemd/journald\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"These files configure various parameters of the systemd journal service, "
"B<systemd-journald.service>(8)\\&. See B<systemd.syntax>(5)  for a general "
"description of the syntax\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The default configuration is defined during compilation, so a configuration "
"file is only needed when it is necessary to deviate from those defaults\\&. "
"By default, the configuration file in /etc/systemd/ contains commented out "
"entries showing the defaults as a guide to the administrator\\&. This file "
"can be edited to create local overrides\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"When packages need to customize the configuration, they can install "
"configuration snippets in /usr/lib/systemd/*\\&.conf\\&.d/ or /usr/local/lib/"
"systemd/*\\&.conf\\&.d/\\&. Files in /etc/ are reserved for the local "
"administrator, who may use this logic to override the configuration files "
"installed by vendor packages\\&. The main configuration file is read before "
"any of the configuration directories, and has the lowest precedence; entries "
"in a file in any configuration directory override entries in the single "
"configuration file\\&. Files in the *\\&.conf\\&.d/ configuration "
"subdirectories are sorted by their filename in lexicographic order, "
"regardless of which of the subdirectories they reside in\\&. When multiple "
"files specify the same option, for options which accept just a single value, "
"the entry in the file with the lexicographically latest name takes precedence"
"\\&. For options which accept a list of values, entries are collected as "
"they occur in files sorted lexicographically\\&. It is recommended to prefix "
"all filenames in those subdirectories with a two-digit number and a dash, to "
"simplify the ordering of the files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"To disable a configuration file supplied by the vendor, the recommended way "
"is to place a symlink to /dev/null in the configuration directory in /etc/, "
"with the same filename as the vendor configuration file\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "All options are configured in the \"[Journal]\" section:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<Storage=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Controls where to store journal data\\&. One of \"volatile\", \"persistent"
"\", \"auto\" and \"none\"\\&. If \"volatile\", journal log data will be "
"stored only in memory, i\\&.e\\&. below the /run/log/journal hierarchy "
"(which is created if needed)\\&. If \"persistent\", data will be stored "
"preferably on disk, i\\&.e\\&. below the /var/log/journal hierarchy (which "
"is created if needed), with a fallback to /run/log/journal (which is created "
"if needed), during early boot and if the disk is not writable\\&.  \"auto\" "
"is similar to \"persistent\" but the directory /var/log/journal is not "
"created if needed, so that its existence controls where log data goes\\&.  "
"\"none\" turns off all storage, all log data received will be dropped\\&. "
"Forwarding to other targets, such as the console, the kernel log buffer, or "
"a syslog socket will still work however\\&. Defaults to \"auto\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<Compress=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Can take a boolean value\\&. If enabled (the default), data objects that "
"shall be stored in the journal and are larger than the default threshold of "
"512 bytes are compressed before they are written to the file system\\&. It "
"can also be set to a number of bytes to specify the compression threshold "
"directly\\&. Suffixes like K, M, and G can be used to specify larger units"
"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<Seal=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Takes a boolean value\\&. If enabled (the default), and a sealing key is "
"available (as created by B<journalctl>(1)\\*(Aqs B<--setup-keys> command), "
"Forward Secure Sealing (FSS) for all persistent journal files is enabled\\&. "
"FSS is based on \\m[blue]B<Seekable Sequential Key Generators>\\m[]\\&"
"\\s-2\\u[1]\\d\\s+2 by G\\&. A\\&. Marson and B\\&. Poettering "
"(doi:10\\&.1007/978-3-642-40203-6_7) and may be used to protect journal "
"files from unnoticed alteration\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<SplitMode=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"Controls whether to split up journal files per user, either \"uid\" or \"none"
"\"\\&. Split journal files are primarily useful for access control: on UNIX/"
"Linux access control is managed per file, and the journal daemon will assign "
"users read access to their journal files\\&. If \"uid\", all regular users "
"(with UID outside the range of system users, dynamic service users, and the "
"nobody user) will each get their own journal files, and system users will "
"log to the system journal\\&. See \\m[blue]B<Users, Groups, UIDs and GIDs on "
"systemd systems>\\m[]\\&\\s-2\\u[2]\\d\\s+2 for more details about UID ranges"
"\\&. If \"none\", journal files are not split up by user and all messages "
"are instead stored in the single system journal\\&. In this mode "
"unprivileged users generally do not have access to their own log data\\&. "
"Note that splitting up journal files by user is only available for journals "
"stored persistently\\&. If journals are stored on volatile storage (see "
"I<Storage=> above), only a single journal file is used\\&. Defaults to \"uid"
"\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<RateLimitIntervalSec=>, I<RateLimitBurst=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Configures the rate limiting that is applied to all messages generated on "
"the system\\&. If, in the time interval defined by I<RateLimitIntervalSec=>, "
"more messages than specified in I<RateLimitBurst=> are logged by a service, "
"all further messages within the interval are dropped until the interval is "
"over\\&. A message about the number of dropped messages is generated\\&. "
"This rate limiting is applied per-service, so that two services which log do "
"not interfere with each other\\*(Aqs limits\\&. Defaults to 10000 messages "
"in 30s\\&. The time specification for I<RateLimitIntervalSec=> may be "
"specified in the following units: \"s\", \"min\", \"h\", \"ms\", \"us\"\\&. "
"To turn off any kind of rate limiting, set either value to 0\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If a service provides rate limits for itself through "
"I<LogRateLimitIntervalSec=> and/or I<LogRateLimitBurst=> in B<systemd."
"exec>(5), those values will override the settings specified here\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"I<SystemMaxUse=>, I<SystemKeepFree=>, I<SystemMaxFileSize=>, "
"I<SystemMaxFiles=>, I<RuntimeMaxUse=>, I<RuntimeKeepFree=>, "
"I<RuntimeMaxFileSize=>, I<RuntimeMaxFiles=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Enforce size limits on the journal files stored\\&. The options prefixed "
"with \"System\" apply to the journal files when stored on a persistent file "
"system, more specifically /var/log/journal\\&. The options prefixed with "
"\"Runtime\" apply to the journal files when stored on a volatile in-memory "
"file system, more specifically /run/log/journal\\&. The former is used only "
"when /var is mounted, writable, and the directory /var/log/journal exists"
"\\&. Otherwise, only the latter applies\\&. Note that this means that during "
"early boot and if the administrator disabled persistent logging, only the "
"latter options apply, while the former apply if persistent logging is "
"enabled and the system is fully booted up\\&.  B<journalctl> and B<systemd-"
"journald> ignore all files with names not ending with \"\\&.journal\" or "
"\"\\&.journal~\", so only such files, located in the appropriate "
"directories, are taken into account when calculating current disk usage\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"I<SystemMaxUse=> and I<RuntimeMaxUse=> control how much disk space the "
"journal may use up at most\\&.  I<SystemKeepFree=> and I<RuntimeKeepFree=> "
"control how much disk space systemd-journald shall leave free for other uses"
"\\&.  B<systemd-journald> will respect both limits and use the smaller of "
"the two values\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The first pair defaults to 10% and the second to 15% of the size of the "
"respective file system, but each value is capped to 4G\\&. If the file "
"system is nearly full and either I<SystemKeepFree=> or I<RuntimeKeepFree=> "
"are violated when systemd-journald is started, the limit will be raised to "
"the percentage that is actually free\\&. This means that if there was enough "
"free space before and journal files were created, and subsequently something "
"else causes the file system to fill up, journald will stop using more space, "
"but it will not be removing existing files to reduce the footprint again, "
"either\\&. Also note that only archived files are deleted to reduce the "
"space occupied by journal files\\&. This means that, in effect, there might "
"still be more space used than I<SystemMaxUse=> or I<RuntimeMaxUse=> limit "
"after a vacuuming operation is complete\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"I<SystemMaxFileSize=> and I<RuntimeMaxFileSize=> control how large "
"individual journal files may grow at most\\&. This influences the "
"granularity in which disk space is made available through rotation, i\\&.e"
"\\&. deletion of historic data\\&. Defaults to one eighth of the values "
"configured with I<SystemMaxUse=> and I<RuntimeMaxUse=>, so that usually "
"seven rotated journal files are kept as history\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Specify values in bytes or use K, M, G, T, P, E as units for the specified "
"sizes (equal to 1024, 1024\\(S2, \\&... bytes)\\&. Note that size limits are "
"enforced synchronously when journal files are extended, and no explicit "
"rotation step triggered by time is needed\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"I<SystemMaxFiles=> and I<RuntimeMaxFiles=> control how many individual "
"journal files to keep at most\\&. Note that only archived files are deleted "
"to reduce the number of files until this limit is reached; active files will "
"stay around\\&. This means that, in effect, there might still be more "
"journal files around in total than this limit after a vacuuming operation is "
"complete\\&. This setting defaults to 100\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<MaxFileSec=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The maximum time to store entries in a single journal file before rotating "
"to the next one\\&. Normally, time-based rotation should not be required as "
"size-based rotation with options such as I<SystemMaxFileSize=> should be "
"sufficient to ensure that journal files do not grow without bounds\\&. "
"However, to ensure that not too much data is lost at once when old journal "
"files are deleted, it might make sense to change this value from the default "
"of one month\\&. Set to 0 to turn off this feature\\&. This setting takes "
"time values which may be suffixed with the units \"year\", \"month\", \"week"
"\", \"day\", \"h\" or \"m\" to override the default time unit of seconds\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<MaxRetentionSec=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The maximum time to store journal entries\\&. This controls whether journal "
"files containing entries older than the specified time span are deleted\\&. "
"Normally, time-based deletion of old journal files should not be required as "
"size-based deletion with options such as I<SystemMaxUse=> should be "
"sufficient to ensure that journal files do not grow without bounds\\&. "
"However, to enforce data retention policies, it might make sense to change "
"this value from the default of 0 (which turns off this feature)\\&. This "
"setting also takes time values which may be suffixed with the units \"year"
"\", \"month\", \"week\", \"day\", \"h\" or \" m\" to override the default "
"time unit of seconds\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<SyncIntervalSec=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The timeout before synchronizing journal files to disk\\&. After syncing, "
"journal files are placed in the OFFLINE state\\&. Note that syncing is "
"unconditionally done immediately after a log message of priority CRIT, ALERT "
"or EMERG has been logged\\&. This setting hence applies only to messages of "
"the levels ERR, WARNING, NOTICE, INFO, DEBUG\\&. The default timeout is 5 "
"minutes\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"I<ForwardToSyslog=>, I<ForwardToKMsg=>, I<ForwardToConsole=>, "
"I<ForwardToWall=>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Control whether log messages received by the journal daemon shall be "
"forwarded to a traditional syslog daemon, to the kernel log buffer (kmsg), "
"to the system console, or sent as wall messages to all logged-in users\\&. "
"These options take boolean arguments\\&. If forwarding to syslog is enabled "
"but nothing reads messages from the socket, forwarding to syslog has no "
"effect\\&. By default, only forwarding to wall is enabled\\&. These settings "
"may be overridden at boot time with the kernel command line options \"systemd"
"\\&.journald\\&.forward_to_syslog\", \"systemd\\&.journald\\&.forward_to_kmsg"
"\", \"systemd\\&.journald\\&.forward_to_console\", and \"systemd\\&.journald"
"\\&.forward_to_wall\"\\&. If the option name is specified without \"=\" and "
"the following argument, true is assumed\\&. Otherwise, the argument is "
"parsed as a boolean\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"When forwarding to the console, the TTY to log to can be changed with "
"I<TTYPath=>, described below\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"When forwarding to the kernel log buffer (kmsg), make sure to select a "
"suitably large size for the log buffer, and ensure the kernel\\*(Aqs rate-"
"limiting applied to userspace processes is turned off\\&. Specifically, add "
"\"log_buf_len=8M\" and \"printk\\&.devkmsg=on\" (or similar) to the kernel "
"command line\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"I<MaxLevelStore=>, I<MaxLevelSyslog=>, I<MaxLevelKMsg=>, "
"I<MaxLevelConsole=>, I<MaxLevelWall=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"Controls the maximum log level of messages that are stored in the journal, "
"forwarded to syslog, kmsg, the console or wall (if that is enabled, see "
"above)\\&. As argument, takes one of \"emerg\", \"alert\", \"crit\", \"err"
"\", \"warning\", \"notice\", \"info\", \"debug\", or integer values in the "
"range of 0\\(en7 (corresponding to the same levels)\\&. Messages equal or "
"below the log level specified are stored/forwarded, messages above are "
"dropped\\&. Defaults to \"debug\" for I<MaxLevelStore=> and "
"I<MaxLevelSyslog=>, to ensure that the all messages are stored in the "
"journal and forwarded to syslog\\&. Defaults to \"notice\" for "
"I<MaxLevelKMsg=>, \"info\" for I<MaxLevelConsole=>, and \"emerg\" for "
"I<MaxLevelWall=>\\&. These settings may be overridden at boot time with the "
"kernel command line options \"systemd\\&.journald\\&.max_level_store=\", "
"\"systemd\\&.journald\\&.max_level_syslog=\", \"systemd\\&.journald\\&."
"max_level_kmsg=\", \"systemd\\&.journald\\&.max_level_console=\", \"systemd"
"\\&.journald\\&.max_level_wall=\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<ReadKMsg=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Takes a boolean value\\&. If enabled (the default), journal reads /dev/kmsg "
"messages generated by the kernel\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<TTYPath=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Change the console TTY to use if I<ForwardToConsole=yes> is used\\&. "
"Defaults to /dev/console\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<LineMax=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The maximum line length to permit when converting stream logs into record "
"logs\\&. When a systemd unit\\*(Aqs standard output/error are connected to "
"the journal via a stream socket, the data read is split into individual log "
"records at newline (\"\\en\", ASCII 10) and NUL characters\\&. If no such "
"delimiter is read for the specified number of bytes a hard log record "
"boundary is artificially inserted, breaking up overly long lines into "
"multiple log records\\&. Selecting overly large values increases the "
"possible memory usage of the Journal daemon for each stream client, as in "
"the worst case the journal daemon needs to buffer the specified number of "
"bytes in memory before it can flush a new log record to disk\\&. Also note "
"that permitting overly large line maximum line lengths affects compatibility "
"with traditional log protocols as log records might not fit anymore into a "
"single B<AF_UNIX> or B<AF_INET> datagram\\&. Takes a size in bytes\\&. If "
"the value is suffixed with K, M, G or T, the specified size is parsed as "
"Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), "
"respectively\\&. Defaults to 48K, which is relatively large but still small "
"enough so that log records likely fit into network datagrams along with "
"extra room for metadata\\&. Note that values below 79 are not accepted and "
"will be bumped to 79\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "FORWARDING TO TRADITIONAL SYSLOG DAEMONS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Journal events can be transferred to a different logging daemon in two "
"different ways\\&. With the first method, messages are immediately forwarded "
"to a socket (/run/systemd/journal/syslog), where the traditional syslog "
"daemon can read them\\&. This method is controlled by the "
"I<ForwardToSyslog=> option\\&. With a second method, a syslog daemon behaves "
"like a normal journal client, and reads messages from the journal files, "
"similarly to B<journalctl>(1)\\&. With this, messages do not have to be read "
"immediately, which allows a logging daemon which is only started late in "
"boot to access all messages since the start of the system\\&. In addition, "
"full structured meta-data is available to it\\&. This method of course is "
"available only if the messages are stored in a journal file at all\\&. So it "
"will not work if I<Storage=none> is set\\&. It should be noted that usually "
"the I<second> method is used by syslog daemons, so the I<Storage=> option, "
"and not the I<ForwardToSyslog=> option, is relevant for them\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<systemd>(1), B<systemd-journald.service>(8), B<journalctl>(1), B<systemd."
"journal-fields>(7), B<systemd-system.conf>(5)"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Seekable Sequential Key Generators"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "\\%https://eprint.iacr.org/2013/397"
msgstr ""

#. type: IP
#: archlinux debian-unstable
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "Users, Groups, UIDs and GIDs on systemd systems"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "\\%https://systemd.io/UIDS-GIDS"
msgstr ""

#. type: Plain text
#: debian-buster mageia-cauldron
msgid ""
"Controls whether to split up journal files per user, either \"uid\" or \"none"
"\"\\&. Split journal files are primarily useful for access control: on UNIX/"
"Linux access control is managed per file, and the journal daemon will assign "
"users read access to their journal files\\&. If \"uid\", all regular users "
"will each get their own journal files, and system users will log to the "
"system journal\\&. If \"none\", journal files are not split up by user and "
"all messages are instead stored in the single system journal\\&. In this "
"mode unprivileged users generally do not have access to their own log data"
"\\&. Note that splitting up journal files by user is only available for "
"journals stored persistently\\&. If journals are stored on volatile storage "
"(see I<Storage=> above), only a single journal file is used\\&. Defaults to "
"\"uid\"\\&."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Control whether log messages received by the journal daemon shall be "
"forwarded to a traditional syslog daemon, to the kernel log buffer (kmsg), "
"to the system console, or sent as wall messages to all logged-in users\\&. "
"These options take boolean arguments\\&. If forwarding to syslog is enabled "
"but nothing reads messages from the socket, forwarding to syslog has no "
"effect\\&. By default, only forwarding to syslog and wall is enabled\\&. "
"These settings may be overridden at boot time with the kernel command line "
"options \"systemd\\&.journald\\&.forward_to_syslog\", \"systemd\\&.journald"
"\\&.forward_to_kmsg\", \"systemd\\&.journald\\&.forward_to_console\", and "
"\"systemd\\&.journald\\&.forward_to_wall\"\\&. If the option name is "
"specified without \"=\" and the following argument, true is assumed\\&. "
"Otherwise, the argument is parsed as a boolean\\&."
msgstr ""

#. type: Plain text
#: debian-buster mageia-cauldron
msgid ""
"Controls the maximum log level of messages that are stored on disk, "
"forwarded to syslog, kmsg, the console or wall (if that is enabled, see "
"above)\\&. As argument, takes one of \"emerg\", \"alert\", \"crit\", \"err"
"\", \"warning\", \"notice\", \"info\", \"debug\", or integer values in the "
"range of 0\\(en7 (corresponding to the same levels)\\&. Messages equal or "
"below the log level specified are stored/forwarded, messages above are "
"dropped\\&. Defaults to \"debug\" for I<MaxLevelStore=> and "
"I<MaxLevelSyslog=>, to ensure that the all messages are written to disk and "
"forwarded to syslog\\&. Defaults to \"notice\" for I<MaxLevelKMsg=>, \"info"
"\" for I<MaxLevelConsole=>, and \"emerg\" for I<MaxLevelWall=>\\&. These "
"settings may be overridden at boot time with the kernel command line options "
"\"systemd\\&.journald\\&.max_level_store=\", \"systemd\\&.journald\\&."
"max_level_syslog=\", \"systemd\\&.journald\\&.max_level_kmsg=\", \"systemd"
"\\&.journald\\&.max_level_console=\", \"systemd\\&.journald\\&."
"max_level_wall=\"\\&."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 241"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"When packages need to customize the configuration, they can install "
"configuration snippets in /usr/lib/systemd/*\\&.conf\\&.d/\\&. Files in /"
"etc/ are reserved for the local administrator, who may use this logic to "
"override the configuration files installed by vendor packages\\&. The main "
"configuration file is read before any of the configuration directories, and "
"has the lowest precedence; entries in a file in any configuration directory "
"override entries in the single configuration file\\&. Files in the *\\&.conf"
"\\&.d/ configuration subdirectories are sorted by their filename in "
"lexicographic order, regardless of which of the subdirectories they reside in"
"\\&. When multiple files specify the same option, for options which accept "
"just a single value, the entry in the file with the lexicographically latest "
"name takes precedence\\&. For options which accept a list of values, entries "
"are collected as they occur in files sorted lexicographically\\&. It is "
"recommended to prefix all filenames in those subdirectories with a two-digit "
"number and a dash, to simplify the ordering of the files\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Control whether log messages received by the journal daemon shall be "
"forwarded to a traditional syslog daemon, to the kernel log buffer (kmsg), "
"to the system console, or sent as wall messages to all logged-in users\\&. "
"These options take boolean arguments\\&. If forwarding to syslog is enabled "
"but nothing reads messages from the socket, forwarding to syslog has no "
"effect\\&. By default, only forwarding to wall is enabled\\&. These settings "
"may be overridden at boot time with the kernel command line options \"systemd"
"\\&.journald\\&.forward_to_syslog\", \"systemd\\&.journald\\&.forward_to_kmsg"
"\", \"systemd\\&.journald\\&.forward_to_console\", and \"systemd\\&.journald"
"\\&.forward_to_wall\"\\&. If the option name is specified without \"=\" and "
"the following argument, true is assumed\\&. Otherwise, the argument is "
"parsed as a boolean\\&. When forwarding to the console, the TTY to log to "
"can be changed with I<TTYPath=>, described below\\&."
msgstr ""
