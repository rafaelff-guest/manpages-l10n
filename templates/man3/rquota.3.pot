# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-12-08 13:38+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RQUOTA"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "rquota - implement quotas on remote machines"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "PROTOCOL"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B</usr/include/rpcsvc/rquota.x>"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: IX
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "rquota()"
msgstr ""

#. type: IX
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "\\fLrquota()\\fR \\(em implement quotas on remote machines"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<rquota(\\|)> protocol inquires about quotas on remote machines.  It is "
"used in conjunction with E<.SM NFS\\s0,> since E<.SM NFS> itself does not "
"implement quotas."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "PROGRAMMING"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<#include E<lt>rpcsvc/rquota.hE<gt>>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The following E<.SM XDR> routines are available in B<librpcsvc>:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<xdr_getquota_arg>\n"
"B<xdr_getquota_rslt>\n"
"B<xdr_rquota>\n"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<quota>(1), B<quotactl>(2)"
msgstr ""
