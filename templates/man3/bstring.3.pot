# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-11-29 11:39+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "BSTRING"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2019-03-06"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"bcmp, bcopy, bzero, memccpy, memchr, memcmp, memcpy, memfrob, memmem, "
"memmove, memset - byte string operations"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<int bcmp(const void *>I<s1>B<, const void *>I<s2>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<void bcopy(const void *>I<src>B<, void *>I<dest>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<void bzero(void *>I<s>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<void *memccpy(void *>I<dest>B<, const void *>I<src>B<, int >I<c>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<void *memchr(const void *>I<s>B<, int >I<c>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<int memcmp(const void *>I<s1>B<, const void *>I<s2>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<void *memcpy(void *>I<dest>B<, const void *>I<src>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<void *memfrob(void *>I<s>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<void *memmem(const void *>I<haystack>B<, size_t >I<haystacklen>B<,>\n"
"B<             const void *>I<needle>B<, size_t >I<needlelen>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<void *memmove(void *>I<dest>B<, const void *>I<src>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<void *memset(void *>I<s>B<, int >I<c>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The byte string functions perform operations on strings (byte arrays)  that "
"are not necessarily null-terminated.  See the individual man pages for "
"descriptions of each function."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#.  The old functions are not even available on some non-GNU/Linux systems.
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The functions B<bcmp>(), B<bcopy>()  and B<bzero>()  are obsolete.  Use "
"B<memcmp>(), B<memcpy>()  and B<memset>()  instead."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<bcmp>(3), B<bcopy>(3), B<bzero>(3), B<memccpy>(3), B<memchr>(3), "
"B<memcmp>(3), B<memcpy>(3), B<memfrob>(3), B<memmem>(3), B<memmove>(3), "
"B<memset>(3)"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
