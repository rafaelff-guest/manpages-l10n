# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-11-16 21:21+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COMM"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2019"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.31"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "comm - compare two sorted files line by line"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<comm> [I<\\,OPTION\\/>]... I<\\,FILE1 FILE2\\/>"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Compare sorted files FILE1 and FILE2 line by line."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "When FILE1 or FILE2 (not both) is -, read standard input."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"With no options, produce three-column output.  Column one contains lines "
"unique to FILE1, column two contains lines unique to FILE2, and column three "
"contains lines common to both files."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-1>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "suppress column 1 (lines unique to FILE1)"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-2>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "suppress column 2 (lines unique to FILE2)"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-3>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "suppress column 3 (lines that appear in both files)"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--check-order>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"check that the input is correctly sorted, even if all input lines are "
"pairable"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--nocheck-order>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "do not check that the input is correctly sorted"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--output-delimiter>=I<\\,STR\\/>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "separate columns with STR"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--total>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output a summary"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "line delimiter is NUL, not newline"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display this help and exit"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output version information and exit"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Note, comparisons honor the rules specified by 'LC_COLLATE'."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "comm -12 file1 file2"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print only lines present in both file1 and file2."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "comm -3 file1 file2"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print lines in file1 not in file2, and vice versa."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Written by Richard M. Stallman and David MacKenzie."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2019 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "join(1), uniq(1)"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/commE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) comm invocation\\(aq"
msgstr ""

#. type: TH
#: debian-buster
#, no-wrap
msgid "February 2019"
msgstr ""

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Report comm translation bugs to E<lt>https://translationproject.org/team/"
"E<gt>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/commE<gt>"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr ""
