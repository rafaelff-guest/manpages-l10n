# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-01-25 22:08+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "COLRM"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "September 2011"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr ""

#. #-#-#-#-#  archlinux: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-buster: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  debian-unstable: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  mageia-cauldron: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "colrm - remove columns from a file"
msgstr ""

#. #-#-#-#-#  archlinux: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-buster: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  debian-unstable: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  mageia-cauldron: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<colrm> [I<first>\\ [I<last>]]"
msgstr ""

#. #-#-#-#-#  archlinux: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-buster: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  debian-unstable: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  mageia-cauldron: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"B<colrm> removes selected columns from a file.  Input is taken from standard "
"input.  Output is sent to standard output."
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"If called with one parameter the columns of each line will be removed "
"starting with the specified I<first> column.  If called with two parameters "
"the columns from the I<first> column to the I<last> column will be removed."
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "Column numbering starts with column 1."
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "Display version information and exit."
msgstr ""

#. type: TP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "Display help text and exit."
msgstr ""

#. #-#-#-#-#  archlinux: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-buster: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  debian-unstable: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  mageia-cauldron: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<awk>(1p), B<column>(1), B<expand>(1), B<paste>(1)"
msgstr ""

#. #-#-#-#-#  archlinux: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-buster: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  debian-unstable: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  mageia-cauldron: colrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "The B<colrm> command appeared in 3.0BSD."
msgstr ""

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"The colrm command is part of the util-linux package and is available from E<."
"UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Linux "
"Kernel Archive E<.UE .>"
msgstr ""

#. type: Dd
#: debian-buster debian-unstable
#, no-wrap
msgid "June 6, 2015"
msgstr ""

#. type: Dt
#: debian-buster debian-unstable
#, no-wrap
msgid "COLRM 1"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "E<.Nm colrm>"
msgstr ""

#. type: Nd
#: debian-buster debian-unstable
#, no-wrap
msgid "remove columns from a file"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "E<.Nm> E<.Op Ar start Op Ar stop>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"The E<.Nm> utility removes selected columns from the lines of a file.  A "
"column is defined as a single character in a line.  Input is read from the "
"standard input.  Output is written to the standard output."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"If only the E<.Ar start> column is specified, columns numbered less than the "
"E<.Ar start> column will be written.  If both E<.Ar start> and E<.Ar stop> "
"columns are specified, columns numbered less than the E<.Ar start> column or "
"greater than the E<.Ar stop> column will be written.  Column numbering "
"starts with one, not zero."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Tab characters increment the column count to the next multiple of eight.  "
"Backspace characters decrement the column count by one."
msgstr ""

#. type: Sh
#: debian-buster debian-unstable
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"The E<.Ev LANG , LC_ALL> and E<.Ev LC_CTYPE> environment variables affect "
"the execution of E<.Nm> as described in E<.Xr environ 7>."
msgstr ""

#. type: Sh
#: debian-buster debian-unstable
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "E<.Ex -std>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "E<.Xr awk 1>, E<.Xr column 1>, E<.Xr cut 1>, E<.Xr paste 1>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "The E<.Nm> utility first appeared in E<.Bx 1>."
msgstr ""

#. type: Sh
#: debian-buster debian-unstable
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"E<.An Jeff Schriebman> wrote the original version of E<.Nm> in November 1974."
msgstr ""
