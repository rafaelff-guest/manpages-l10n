# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-11-16 21:21+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B2SUM"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2019"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.31"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "b2sum - compute and check BLAKE2 message digest"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<b2sum> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print or check BLAKE2 (512-bit) checksums."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-b>, B<--binary>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "read in binary mode"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-c>, B<--check>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "read BLAKE2 sums from the FILEs and check them"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-l>, B<--length>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"digest length in bits; must not exceed the maximum for the blake2 algorithm "
"and must be a multiple of 8"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--tag>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "create a BSD-style checksum"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-t>, B<--text>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "read in text mode (default)"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-z>, B<--zero>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"end each output line with NUL, not newline, and disable file name escaping"
msgstr ""

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "The following five options are useful only when verifying checksums:"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--ignore-missing>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "don't fail or report status for missing files"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--quiet>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "don't print OK for each successfully verified file"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--status>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "don't output anything, status code shows success"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--strict>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "exit non-zero for improperly formatted checksum lines"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-w>, B<--warn>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "warn about improperly formatted checksum lines"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display this help and exit"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output version information and exit"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The sums are computed as described in RFC 7693.  When checking, the input "
"should be a former output of this program.  The default mode is to print a "
"line with checksum, a space, a character indicating input mode ('*' for "
"binary, \\&' ' for text or where binary is insignificant), and name for each "
"FILE."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Written by Padraig Brady and Samuel Neves."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2019 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/b2sumE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) b2sum invocation\\(aq"
msgstr ""

#. type: TH
#: debian-buster
#, no-wrap
msgid "February 2019"
msgstr ""

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Report b2sum translation bugs to E<lt>https://translationproject.org/team/"
"E<gt>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/b2sumE<gt>"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "B<--color>"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "print with colors"
msgstr ""
