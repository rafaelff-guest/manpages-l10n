# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-11-16 21:22+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "STAT"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2019"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.31"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "stat - display file or file system status"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<stat> [I<\\,OPTION\\/>]... I<\\,FILE\\/>..."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display file or file system status."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-L>, B<--dereference>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "follow links"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-f>, B<--file-system>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display file system status instead of file status"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-c>  B<--format>=I<\\,FORMAT\\/>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"use the specified FORMAT instead of the default; output a newline after each "
"use of FORMAT"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--printf>=I<\\,FORMAT\\/>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"like B<--format>, but interpret backslash escapes, and do not output a "
"mandatory trailing newline; if you want a newline, include \\en in FORMAT"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-t>, B<--terse>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "print the information in terse form"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display this help and exit"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output version information and exit"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The valid format sequences for files (without B<--file-system>):"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%a"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "access rights in octal (note '#' and '0' printf flags)"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%A"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "access rights in human readable form"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%b"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "number of blocks allocated (see %B)"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%B"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "the size in bytes of each block reported by %b"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%C"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "SELinux security context string"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%d"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "device number in decimal"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%D"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "device number in hex"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%f"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "raw mode in hex"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%F"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "file type"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%g"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "group ID of owner"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%G"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "group name of owner"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%h"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "number of hard links"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%i"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "inode number"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%m"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "mount point"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "file name"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%N"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "quoted file name with dereference if symbolic link"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%o"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "optimal I/O transfer size hint"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%s"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "total size, in bytes"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%t"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "major device type in hex, for character/block device special files"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%T"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "minor device type in hex, for character/block device special files"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%u"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "user ID of owner"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%U"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "user name of owner"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%w"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of file birth, human-readable; - if unknown"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%W"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of file birth, seconds since Epoch; 0 if unknown"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%x"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last access, human-readable"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%X"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last access, seconds since Epoch"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%y"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last data modification, human-readable"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%Y"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last data modification, seconds since Epoch"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%z"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last status change, human-readable"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%Z"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last status change, seconds since Epoch"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Valid format sequences for file systems:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "free blocks available to non-superuser"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "total data blocks in file system"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%c"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "total file nodes in file system"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "free file nodes in file system"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "free blocks in file system"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "file system ID in hex"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%l"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "maximum length of filenames"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "block size (for faster transfers)"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%S"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "fundamental block size (for block counts)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "file system type in hex"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "file system type in human readable form"
msgstr ""

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "--terse is equivalent to the following FORMAT:"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o"
msgstr ""

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "--terse --file-system is equivalent to the following FORMAT:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "%n %i %l %t %s %S %b %f %a %c %d"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"NOTE: your shell may have its own version of stat, which usually supersedes "
"the version described here.  Please refer to your shell's documentation for "
"details about the options it supports."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Written by Michael Meskes."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2019 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "stat(2), statfs(2)"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/statE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) stat invocation\\(aq"
msgstr ""

#. type: TH
#: debian-buster
#, no-wrap
msgid "February 2019"
msgstr ""

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o %C"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Report stat translation bugs to E<lt>https://translationproject.org/team/"
"E<gt>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/statE<gt>"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr ""
