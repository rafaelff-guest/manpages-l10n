# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-01-25 22:12+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SU"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "July 2014"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "su - run a command with substitute user and group ID"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<su> [options] [B<->] [I<user> [I<argument>...]]"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<su> allows to run commands with a substitute user and group ID."
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"When called with no I<user> specified, B<su> defaults to running an "
"interactive shell as I<root>.  When I<user> is specified, additional "
"I<argument>s can be supplied, in which case they are passed to the shell."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For backward compatibility, B<su> defaults to not change the current "
"directory and to only set the environment variables B<HOME> and B<SHELL> "
"(plus B<USER> and B<LOGNAME> if the target I<user> is not root).  It is "
"recommended to always use the B<--login> option (instead of its shortcut B<-"
">)  to avoid side effects caused by mixing environments."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This version of B<su> uses PAM for authentication, account and session "
"management.  Some configuration options found in other B<su> "
"implementations, such as support for a wheel group, have to be configured "
"via PAM."
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"B<su> is mostly designed for unprivileged users, the recommended solution "
"for privileged users (e.g., scripts executed by root) is to use non-set-user-"
"ID command B<runuser>(1)  that does not require authentication and provide "
"separate PAM configuration. If the PAM session is not required at all then "
"the recommend solution is to use command B<setpriv>(1)."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-c>,B< --command>=I<command>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Pass I<command> to the shell with the B<-c> option."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-f>,B< --fast>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Pass B<-f> to the shell, which may or may not be useful, depending on the "
"shell."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-g>,B< --group>=I<group>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Specify the primary group.  This option is available to the root user only."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-G>,B< --supp-group>=I<group>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Specify a supplemental group.  This option is available to the root user "
"only.  The first specified supplementary group is also used as a primary "
"group if the option B<--group> is unspecified."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<->,B< -l>,B< --login>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Start the shell as a login shell with an environment similar to a real login:"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "o"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"clears all the environment variables except B<TERM> and variables specified "
"by B<--whitelist-environment>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"initializes the environment variables B<HOME>, B<SHELL>, B<USER>, "
"B<LOGNAME>, and B<PATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "changes to the target user's home directory"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"sets argv[0] of the shell to 'B<->' in order to make the shell a login shell"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-m>,B< -p>,B< --preserve-environment>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Preserve the entire environment, i.e., it does not set B<HOME>, B<SHELL>, "
"B<USER> nor B<LOGNAME>.  This option is ignored if the option B<--login> is "
"specified."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-P>,B< --pty>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Create pseudo-terminal for the session. The independent terminal provides "
"better security as user does not share terminal with the original session.  "
"This allow to avoid TIOCSTI ioctl terminal injection and another security "
"attacks against terminal file descriptors. The all session is also possible "
"to move to background (e.g., \"su --pty - username -c application &\"). If "
"the pseudo-terminal is enabled then su command works as a proxy between the "
"sessions (copy stdin and stdout)."
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"This feature is mostly designed for interactive sessions. If the standard "
"input is not a terminal, but for example pipe (e.g., echo \"date\" | su --"
"pty) than ECHO flag for the pseudo-terminal is disabled to avoid messy "
"output."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-s>,B< --shell>=I<shell>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Run the specified I<shell> instead of the default.  The shell to run is "
"selected according to the following rules, in order:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "the shell specified with B<--shell>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"the shell specified in the environment variable B<SHELL>, if the B<--"
"preserve-environment> option is used"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "the shell listed in the passwd entry of the target user"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "/bin/sh"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"If the target user has a restricted shell (i.e., not listed in /etc/shells), "
"the B<--shell> option and the B<SHELL> environment variables are ignored "
"unless the calling user is root."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--session-command=>I<command>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Same as B<-c> but do not create a new session.  (Discouraged.)"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-w>,B< --whitelist-environment>=I<list>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Don't reset environment variables specified in comma separated I<list> when "
"clears environment for B<--login>. The whitelist is ignored for the "
"environment variables B<HOME>, B<SHELL>, B<USER>, B<LOGNAME>, and B<PATH>."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display version information and exit."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display help text and exit."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SIGNALS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Upon receiving either B<SIGINT>, B<SIGQUIT> or B<SIGTERM>, B<su> terminates "
"its child and afterwards terminates itself with the received signal.  The "
"child is terminated by SIGTERM, after unsuccessful attempt and 2 seconds of "
"delay the child is killed by SIGKILL."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFIG FILES"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<su> reads the I</etc/default/su> and I</etc/login.defs> configuration "
"files.  The following configuration items are relevant for B<su>(1):"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<FAIL_DELAY> (number)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Delay in seconds in case of an authentication failure. The number must be a "
"non-negative integer."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<ENV_PATH> (string)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Defines the PATH environment variable for a regular user.  The default value "
"is I</usr/local/bin:\\:/bin:\\:/usr/bin>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<ENV_ROOTPATH> (string)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<ENV_SUPATH> (string)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid ""
"Defines the PATH environment variable for root.  ENV_SUPATH takes "
"precedence.  The default value is I</usr/local/sbin:\\:/usr/local/bin:\\:/"
"sbin:\\:/bin:\\:/usr/sbin:\\:/usr/bin>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<ALWAYS_SET_PATH> (boolean)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If set to I<yes> and --login and --preserve-environment were not specified "
"B<su> initializes B<PATH>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The environment variable PATH may be different on systems where /bin and /"
"sbin are merged into /usr."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<su> normally returns the exit status of the command it executed.  If the "
"command was killed by a signal, B<su> returns the number of the signal plus "
"128."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Exit status generated by B<su> itself:"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "1"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Generic error before executing the requested command"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "126"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The requested command could not be executed"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "127"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The requested command was not found"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "/etc/pam.d/su"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "default PAM configuration file"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "/etc/pam.d/su-l"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "PAM configuration file if --login is specified"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "/etc/default/su"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "command specific logindef config file"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "/etc/login.defs"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "global logindef config file"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For security reasons B<su> always logs failed log-in attempts to the btmp "
"file, but it does not write to the lastlog file at all.  This solution "
"allows to control B<su> behavior by PAM configuration.  If you want to use "
"the pam_lastlog module to print warning message about failed log-in attempts "
"then the pam_lastlog has to be configured to update the lastlog file as "
"well. For example by:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "session required pam_lastlog.so nowtmp"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<setpriv>(1), B<login.defs>(5), B<shells>(5), B<pam>(8), B<runuser>(1)"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This B<su> command was derived from coreutils' B<su>, which was based on an "
"implementation by David MacKenzie. The util-linux has been refactored by "
"Karel Zak."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The su command is part of the util-linux package and is available from E<.UR "
"https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Linux "
"Kernel Archive E<.UE .>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"When called without arguments, B<su> defaults to running an interactive "
"shell as I<root>."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"B<su> is mostly designed for unprivileged users, the recommended solution "
"for privileged users (e.g. scripts executed by root) is to use non-set-user-"
"ID command B<runuser>(1)  that does not require authentication and provide "
"separate PAM configuration. If the PAM session is not required at all then "
"the recommend solution is to use command B<setpriv>(1)."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Preserve the entire environment, i.e. it does not set B<HOME>, B<SHELL>, "
"B<USER> nor B<LOGNAME>.  This option is ignored if the option B<--login> is "
"specified."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Create pseudo-terminal for the session. The independent terminal provides "
"better security as user does not share terminal with the original session.  "
"This allow to avoid TIOCSTI ioctl terminal injection and another security "
"attacks against terminal file descriptors. The all session is also possible "
"to move to background (e.g. \"su --pty - username -c application &\"). If "
"the pseudo-terminal is enabled then su command works as a proxy between the "
"sessions (copy stdin and stdout)."
msgstr ""

#. type: Plain text
#: debian-buster
msgid ""
"This feature is EXPERIMENTAL for now and may be removed in the next releases."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"If the target user has a restricted shell (i.e. not listed in /etc/shells), "
"the B<--shell> option and the B<SHELL> environment variables are ignored "
"unless the calling user is root."
msgstr ""

#. type: Plain text
#: debian-buster
msgid ""
"Defines the PATH environment variable for root.  The default value is I</usr/"
"local/sbin:\\:/usr/local/bin:\\:/sbin:\\:/bin:\\:/usr/sbin:\\:/usr/bin>."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<setpriv>(1), B<login.defs>(5), B<shells>(5), B<pam>(8), B<runuser>(8)"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"This feature is mostly designed for interactive sessions. If the standard "
"input is not a terminal, but for example pipe (e.g. echo \"date\" | su --"
"pty) than ECHO flag for the pseudo-terminal is disabled to avoid messy "
"output."
msgstr ""
