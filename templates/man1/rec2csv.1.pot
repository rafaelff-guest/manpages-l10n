# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-02-08 16:54+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "REC2CSV"
msgstr ""

#. type: TH
#: debian-buster
#, no-wrap
msgid "March 2014"
msgstr ""

#. type: TH
#: debian-buster
#, no-wrap
msgid "rec2csv 1.7"
msgstr ""

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "rec2csv - rec to csv converter"
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-buster
msgid "B<rec2csv> [I<OPTIONS>]... [I<REC_FILE>]"
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "Convert rec data into csv data."
msgstr ""

#. type: TP
#: debian-buster
#, no-wrap
msgid "B<-d>, B<--delim>=I<char>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "sets the deliminator (default ',')"
msgstr ""

#. type: TP
#: debian-buster
#, no-wrap
msgid "B<-t>, B<--type>=I<TYPE>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"record set to convert to csv; if this parameter is omitted then the default "
"record set is used"
msgstr ""

#. type: TP
#: debian-buster
#, no-wrap
msgid "B<-S>, B<--sort>=I<FIELDS>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "sort the output by the specified fields."
msgstr ""

#. type: TP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "print a help message and exit."
msgstr ""

#. type: TP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "show version and exit."
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "Written by Jose E. Marchesi."
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "Report bugs to: bug-recutils@gnu.org"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "GNU recutils home page: E<lt>http://www.gnu.org/software/recutils/E<gt>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: debian-buster
msgid ""
"Copyright \\(co 2010, 2011, 2012, 2013, 2014 Jose E. Marchesi.  License "
"GPLv3+: GNU GPL version 3 or later E<lt>http://gnu.org/licenses/gpl."
"htmlE<gt>."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"The full documentation for B<rec2csv> is maintained as a Texinfo manual.  If "
"the B<info> and B<rec2csv> programs are properly installed at your site, the "
"command"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<info recutils>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "January 2019"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "rec2csv 1.8"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid "B<rec2csv> [I<\\,OPTIONS\\/>]... [I<\\,REC_FILE\\/>]"
msgstr ""

#. type: TP
#: debian-unstable
#, no-wrap
msgid "B<-d>, B<--delim>=I<\\,char\\/>"
msgstr ""

#. type: TP
#: debian-unstable
#, no-wrap
msgid "B<-t>, B<--type>=I<\\,TYPE\\/>"
msgstr ""

#. type: TP
#: debian-unstable
#, no-wrap
msgid "B<-S>, B<--sort>=I<\\,FIELDS\\/>"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"Copyright \\(co 2010-2019 Jose E. Marchesi.  License GPLv3+: GNU GPL version "
"3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
