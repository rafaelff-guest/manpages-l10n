# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-11-16 21:21+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "FMT"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2019"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.31"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "fmt - simple optimal text formatter"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<fmt> [I<\\,-WIDTH\\/>] [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Reformat each paragraph in the FILE(s), writing to standard output.  The "
"option B<-WIDTH> is an abbreviated form of B<--width>=I<\\,DIGITS\\/>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-c>, B<--crown-margin>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "preserve indentation of first two lines"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-p>, B<--prefix>=I<\\,STRING\\/>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"reformat only lines beginning with STRING, reattaching the prefix to "
"reformatted lines"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-s>, B<--split-only>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "split long lines, but do not refill"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-t>, B<--tagged-paragraph>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "indentation of first line different from second"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-u>, B<--uniform-spacing>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "one space between words, two after sentences"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-w>, B<--width>=I<\\,WIDTH\\/>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "maximum line width (default of 75 columns)"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-g>, B<--goal>=I<\\,WIDTH\\/>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "goal width (default of 93% of width)"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display this help and exit"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output version information and exit"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Written by Ross Paterson."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2019 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/fmtE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) fmt invocation\\(aq"
msgstr ""

#. type: TH
#: debian-buster
#, no-wrap
msgid "February 2019"
msgstr ""

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Report fmt translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/fmtE<gt>"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr ""
