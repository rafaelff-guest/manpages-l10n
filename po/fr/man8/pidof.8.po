# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# carmie
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2000.
# Éric Piel <eric.piel@tremplin-utc.net>, 2005.
# Gérard Delafond <gerard@delafond.org>, 2001, 2003.
# Danny <dannybrain@noos.fr>, 2001.
# Christophe Blaess <ccb@club-internet.fr>, 1997.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000-2002.
# François Wendling <frwendling@free.fr>, 2005.
# Philippe Batailler, 2000.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# David Prévot <david@tilapin.org>, 2010-2014.
# Romain Doumenc <rd6137@gmail.com>, 2011.
# Thomas Vincent <thomas@vinc-net.fr>, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra\n"
"POT-Creation-Date: 2019-09-27 22:58+02:00\n"
"PO-Revision-Date: 2014-03-20 18:34+0100\n"
"Last-Translator: Thomas Vincent <thomas@vinc-net.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "PIDOF"
msgstr "PIDOF"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "01 Sep 1998"
msgstr "1er septembre 1998"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "Linux System Administrator's Manual"
msgstr "Manuel de l'administrateur système Linux"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-buster debian-unstable
msgid "pidof -- find the process ID of a running program."
msgstr "pidof - Afficher le PID d'un programme"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-buster
#, fuzzy
msgid ""
"B<pidof> [B<-s>] [B<-c>] [B<-n>] [B<-x>] [B<-o> I<omitpid[,omitpid...]>] [B<-"
"o> I<omitpid[,omitpid...]...>] [B<-f> I<format>] B<program> [B<program...>]"
msgstr ""
"B<pidof> [B<-s>] [B<-c>] [B<-n>] [B<-x>] [B<-o> I<sans_pid[,sans_pid...]>] "
"[B<-o> I<sans_pid[,sans_pid...]>] B<programme> [B<programme...>]"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
msgid ""
"B<Pidof> finds the process id's (PIDs) of the named programs. It prints "
"those id's on the standard output. This program is on some systems used in "
"run-level change scripts, especially when the system has a I<System-V> like "
"I<rc> structure. In that case these scripts are located in /etc/rc?.d, "
"where ? is the runlevel. If the system has a B<start-stop-daemon> (8) "
"program that should be used instead."
msgstr ""
"B<Pidof> cherche les numéros de processus (PID : « Process ID ») portant "
"l'un des noms de programme indiqués. Il affiche ces numéros sur sa sortie "
"standard. Ce programme est utilisé sur certains systèmes dans les scripts de "
"changement de niveau d'exécution, en particulier pour les systèmes utilisant "
"la structure I<rc> de I<Système V>. Dans ce cas, ces scripts sont situés "
"dans /etc/rc?.d où « ? » est le niveau d'exécution. Si le système possède le "
"programme B<start-stop-daemon>(8), celui-ci devrait être utilisé à la place."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "-s"
msgstr "-s"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Single shot - this instructs the program to only return one I<pid>."
msgstr "Un seul coup - I<pidof> ne renverra qu'un seul I<pid>."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "-c"
msgstr "-c"

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
msgid ""
"Only return process PIDs that are running with the same root directory.  "
"This option is ignored for non-root users, as they will be unable to check "
"the current root directory of processes they do not own."
msgstr ""
"Ne renvoyer que les identifiants des processus qui sont exécutés dans le "
"même répertoire racine. Cette option est ignorée pour les utilisateurs "
"autres que le superutilisateur puisqu'ils ne peuvent pas vérifier le "
"répertoire racine en cours des processus dont ils ne sont pas propriétaires."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "-n"
msgstr "-n"

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
msgid ""
"Avoid B<stat>(2)  system function call on all binaries which are located on "
"network based file systems like B<NFS>.  Instead of using this option the "
"variable B<PIDOF_NETFS> may be set and exported."
msgstr ""
"Éviter l'appel à la fonction système B<stat>(2) pour tous les binaires "
"situés sur des systèmes de fichiers en réseau comme B<NFS>. Au lieu "
"d'utiliser cette option, il est possible de configurer et d'exporter la "
"variable B<PIDOF_NETFS>."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "-q"
msgstr "-q"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Do not display matched PIDs to standard out. Simply exit with a status of "
"true or false to indicate whether a matching PID was found."
msgstr ""

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "-x"
msgstr "-x"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Scripts too - this causes the program to also return process id's of shells "
"running the named scripts."
msgstr ""
"Scripts également - I<pidof> renverra également les PID des shells exécutant "
"les scripts indiqués."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "-o I<omitpid>"
msgstr "-o I<sans_pid>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Tells I<pidof> to omit processes with that process id. The special pid B<"
"%PPID> can be used to name the parent process of the I<pidof> program, in "
"other words the calling shell or shell script."
msgstr ""
"Demander à I<pidof> de ne pas tenir compte du PID indiqué. Le PID spécial B<"
"%PPID> peut décrire le processus parent du programme I<pidof>, c'est-à-dire "
"le shell ou le script appelant."

#. type: IP
#: debian-buster
#, no-wrap
msgid "-f I<format>"
msgstr ""

#. type: Plain text
#: debian-buster
msgid ""
"Tells I<pidof> to format the process ids in the given I<printf> style.  For "
"example B<\" -p%d\"> is useful for I<strace>."
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "EXIT STATUS"
msgstr "CODE DE RETOUR"

#. type: TP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "At least one program was found with the requested name."
msgstr "Au moins un programme a été trouvé avec le nom demandé."

#. type: TP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "No program was found with the requested name."
msgstr "Aucun programme trouvé avec le nom demandé."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"I<pidof> is actually the same program as I<killall5>; the program behaves "
"according to the name under which it is called."
msgstr ""
"I<pidof> utilise en fait le même programme que I<killall5> ; la différence "
"de comportement se fait en fonction du nom du programme appelé."

# NOTE: typo: that that
#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
msgid ""
"When I<pidof> is invoked with a full pathname to the program it should find "
"the pid of, it is reasonably safe. Otherwise it is possible that it returns "
"PIDs of running programs that happen to have the same name as the program "
"you're after but are actually other programs. Note that the executable name "
"of running processes is calculated with B<readlink>(2), so symbolic links to "
"executables will also match."
msgstr ""
"Quand I<pidof> est exécuté avec le chemin complet du programme dont il doit "
"chercher le PID, son comportement est fiable. Dans le cas contraire, les PID "
"renvoyés peuvent être ceux de processus portant le même nom mais qui sont en "
"fait des programmes différents. Notez que le nom de l'exécutable des "
"processus en cours d'exécution est obtenu à l'aide de B<readlink>(2), et "
"donc les liens symboliques vers les exécutables correspondront également."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<shutdown>(8), B<init>(8), B<halt>(8), B<reboot>(8), B<killall5>(8)"
msgstr "B<shutdown>(8), B<init>(8), B<halt>(8), B<reboot>(8), B<killall5>(8)"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Miquel van Smoorenburg, miquels@cistron.nl"
msgstr "Miquel van Smoorenburg, miquels@cistron.nl"

#. type: Plain text
#: debian-unstable
#, fuzzy
msgid ""
"B<pidof> [B<-s>] [B<-c>] [B<-n>] [B<-x>] [B<-z>] [B<-o> I<omitpid[,"
"omitpid...]>] [B<-o> I<omitpid[,omitpid...]...>] [B<-d> I<sep>] B<program> "
"[B<program...>]"
msgstr ""
"B<pidof> [B<-s>] [B<-c>] [B<-n>] [B<-x>] [B<-o> I<sans_pid[,sans_pid...]>] "
"[B<-o> I<sans_pid[,sans_pid...]>] B<programme> [B<programme...>]"

#. type: IP
#: debian-unstable
#, no-wrap
msgid "-z"
msgstr "-z"

#. type: Plain text
#: debian-unstable
msgid ""
"Try to detect processes which are stuck in uninterruptible (D) or zombie "
"(Z)  status. Usually these processes are skipped as trying to deal with them "
"can cause pidof to hang."
msgstr ""

#. type: IP
#: debian-unstable
#, fuzzy, no-wrap
msgid "-d I<sep>"
msgstr "B<-t> I<sec>"

#. type: Plain text
#: debian-unstable
msgid ""
"Tells I<pidof> to use I<sep> as an output separator if more than one PID is "
"shown. The default separator is a space."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"Zombie processes or processes in disk sleep (states Z and D, respectively)  "
"are ignored, as attempts to access the stats of these will sometimes fail.  "
"The -z flag (see above) tells pidof to try to detect these sleeping and "
"zombie processes, at the risk of failing or hanging."
msgstr ""
