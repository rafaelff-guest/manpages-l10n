# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Sylvain Cherrier <sylvain.cherrier@free.fr>, 2006, 2007, 2008, 2009.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2007.
# Dominique Simen <dominiquesimen@hotmail.com>, 2009.
# Nicolas Sauzède <nsauzede@free.fr>, 2009.
# Romain Doumenc <rd6137@gmail.com>, 2010, 2011.
# David Prévot <david@tilapin.org>, 2011, 2012, 2014.
# Denis Mugnier <myou72@orange.fr>, 2011.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2012, 2013.
msgid ""
msgstr ""
"Project-Id-Version: nfs-utils\n"
"POT-Creation-Date: 2019-09-27 22:57+02:00\n"
"PO-Revision-Date: 2013-05-30 17:56+0200\n"
"Last-Translator: Cédric Boutillier <boutil@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "nfsstat"
msgstr "nfsstat"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "7 Aug 2007"
msgstr "7 août 2007"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "nfsstat - list NFS statistics"
msgstr "nfsstat - Afficher des statistiques NFS"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<nfsstat> [I<OPTION>]..."
msgstr "B<nfsstat> [I<OPTION>]..."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<nfsstat> displays statistics kept about NFS client and server activity."
msgstr ""
"La commande B<nfsstat> affiche des statistiques concernant l'activité du "
"client et du serveur NFS."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-s, --server>"
msgstr "B<-s>, B<--server>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Print only server-side statistics. The default is to print both server and "
"client statistics."
msgstr ""
"Afficher seulement les informations du côté serveur. Par défaut, les "
"informations concernant à la fois le serveur et le client sont affichées."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-c, --client>"
msgstr "B<-c>, B<--client>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print only client-side statistics."
msgstr "Afficher seulement les informations côté client."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-n, --nfs>"
msgstr "B<-n>, B<--nfs>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Print only NFS statistics. The default is to print both NFS and RPC "
"information."
msgstr ""
"Afficher seulement les statistiques sur NFS. Par défaut, les informations "
"sur RPC et NFS sont affichées."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-2>"
msgstr "B<-2>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Print only NFS v2 statistics. The default is to only print information about "
"the versions of B<NFS> that have non-zero counts."
msgstr ""
"Afficher seulement les statistiques sur NFSv2. Par défaut, seules les "
"informations non nulles sur les différentes versions de B<NFS> sont "
"affichées."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-3>"
msgstr "B<-3>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Print only NFS v3 statistics. The default is to only print information about "
"the versions of B<NFS> that have non-zero counts."
msgstr ""
"Afficher seulement les statistiques sur NFSv3. Par défaut, seules les "
"informations non nulles sur les différentes versions de B<NFS> sont "
"affichées."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-4>"
msgstr "B<-4>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Print only NFS v4 statistics. The default is to only print information about "
"the versions of B<NFS> that have non-zero counts."
msgstr ""
"Afficher seulement les statistiques sur NFSv4. Par défaut, seules les "
"informations non nulles sur les différentes versions de B<NFS> sont "
"affichées."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-m, --mounts>"
msgstr "B<-m>, B<--mounts>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print information about each of the mounted B<NFS> file systems."
msgstr ""
"Afficher les informations sur chacun des systèmes de fichiers B<NFS> montés."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "If this option is used, all other options are ignored."
msgstr "Si cette option est utilisée, toutes les autres sont ignorées."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-r, --rpc>"
msgstr "B<-r, --rpc>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print only RPC statistics."
msgstr "Afficher seulement les statistiques sur les RPC."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-o>I< facility>"
msgstr "B<-o>I< service>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display statistics for the specified facility, which must be one of:"
msgstr ""
"Afficher les statistiques pour le service indiqué. Ce dernier peut être l'un "
"des suivants :"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<nfs>"
msgstr "B<nfs>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "NFS protocol information, split up by RPC call."
msgstr "Informations sur le protocole NFS, découpées par appel RPC."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<rpc>"
msgstr "B<rpc>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "General RPC information."
msgstr "Informations générales sur les RPC."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<net>"
msgstr "B<net>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Network layer statistics, such as the number of received packets, number of "
"TCP connections, etc."
msgstr ""
"Statistiques de la pile réseau telles que le nombre de paquets reçus, le "
"nombre de connexions TCP, etc."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<fh>"
msgstr "B<fh>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Usage information on the server's file handle cache, including the total "
"number of lookups, and the number of hits and misses."
msgstr ""
"Informations sur l'utilisation du cache des descripteurs de fichier du "
"serveur, en particulier le nombre total de recherches, et le nombre de "
"réponses trouvées ou absentes du cache."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<rc>"
msgstr "B<rc>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Usage information on the server's request reply cache, including the total "
"number of lookups, and the number of hits and misses."
msgstr ""
"Informations sur l'utilisation du cache des réponses aux requêtes, y compris "
"le nombre total de recherches et le nombre de réponses trouvées ou absentes "
"du cache."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<io>"
msgstr "B<io>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Usage information on the server's io statistics; bytes read and written."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ra>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"Usage information on the server's read ahead cache, including the ra cache "
"size, the depth of ra cache hits, and ra cache misses."
msgstr ""
"Informations sur l'utilisation du cache des descripteurs de fichier du "
"serveur, en particulier le nombre total de recherches, et le nombre de "
"réponses trouvées ou absentes du cache."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<all>"
msgstr "B<all>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display all of the above facilities."
msgstr "Afficher toutes les informations décrites plus haut."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-v, --verbose>"
msgstr "B<-v, --verbose>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "This is equivalent to B<-o all>."
msgstr "Identique à B<-o all>."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-l, --list>"
msgstr "B<-l, --list>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print information in list form."
msgstr "Afficher les informations sous forme de liste."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-S, --since >I<file>"
msgstr "B<-S>, B<--since >I<fichier>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Instead of printing current statistics, B<nfsstat> imports statistics from "
"I<file> and displays the difference between those and the current "
"statistics.  Valid input I<file>s may be in the form of B</proc/net/rpc/nfs> "
"(raw client stats), B</proc/net/rpc/nfsd> (raw server stats), or saved "
"output from B<nfsstat> itself (client and/or server stats).  Any statistics "
"missing from a saved B<nfsstat> output I<file> are treated as zeroes."
msgstr ""
"Plutôt qu'imprimer les statistiques actuelles, B<nfsstat> importe les "
"statistiques provenant de I<fichier> et affiche les différences entre celles-"
"ci et les valeurs actuelles. Les I<fichier>s acceptés en entrée peuvent être "
"au format de B</proc/net/rpc/nfs> (statistiques clientes brutes), B</proc/"
"net/rpc/nfsd> (statistiques serveur), ou la sortie sauvegardée de B<nfsstat> "
"lui-même (statistiques client et/ou serveur). Toute valeur statistique "
"manquante dans la sauvegarde de la sortie d'un I<fichier> B<nfsstat> est "
"considérée comme égale à zéro."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-Z[interval], --sleep=[interval]>"
msgstr "B<-Z>[I<intervalle>], B<--sleep=>[I<intervalle>]"

#.  --------------------- EXAMPLES -------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Instead of printing current statistics and immediately exiting, B<nfsstat> "
"takes a snapshot of the current statistics and pauses until it receives "
"B<SIGINT> (typically from B<Ctrl-C>), at which point it takes another "
"snapshot and displays the difference between the two.  If I<interval> is "
"specified, B<nfsstat> will print the number of B<NFS> calls made since the "
"previous report.  Stats will be printed repeatedly every I<interval> seconds."
msgstr ""
"Au lieu de donner les états actuels et de quitter immédiatement, B<nfsstat> "
"prend un cliché des états et attend de recevoir B<SIGINT> (dans la plupart "
"des cas avec B<Ctrl-C>), à quel moment il prendra un nouveau cliché et "
"affichera les différences entre les deux. Si I<intervalle> est précisé, "
"B<nfsstat> affichera le nombre d'appels à B<NFS> depuis le rapport précédent "
"Les états seront affichés toutes les I<intervalle> secondes."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<nfsstat -o all -234>"
msgstr "B<nfsstat -o all -234>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Show all information about all versions of B<NFS>."
msgstr ""
"Afficher toutes les informations à propos de toutes les versions de B<NFS>."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<nfsstat --verbose -234>"
msgstr "B<nfsstat --verbose -234>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Same as above."
msgstr "Identique à la commande précédente."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<nfsstat -o all>"
msgstr "B<nfsstat -o all>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Show all information about active versions of B<NFS>."
msgstr ""
"Afficher toutes les informations à propos des versions de B<NFS> en cours "
"d'utilisation."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<nfsstat --nfs --server -3>"
msgstr "B<nfsstat --nfs --server -3>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Show statistics for B<NFS> version 3 server."
msgstr "Afficher les statistiques pour la version 3 du serveur B<NFS>."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<nfsstat -m>"
msgstr "B<nfsstat -m>"

#.  --------------------- DISPLAY --------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Show information about mounted B<NFS> filesystems."
msgstr ""
"Afficher les informations à propos de tous les systèmes de fichiers B<NFS> "
"montés."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DISPLAY"
msgstr "AFFICHAGE"

#.  --------------------- FILES ----------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<Flags> output from the B<-m> option is the same as the flags give to "
"the B<mount> command."
msgstr ""
"Les B<Drapeaux> de sortie de l'option B<-m> sont les mêmes que les drapeaux "
"utilisés avec la commande B<mount>."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B</proc/net/rpc/nfsd>"
msgstr "B</proc/net/rpc/nfsd>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<procfs>-based interface to kernel NFS server statistics."
msgstr ""
"Interface basée sur B<procfs> pour les statistiques du serveur NFS du noyau."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B</proc/net/rpc/nfs>"
msgstr "B</proc/net/rpc/nfs>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<procfs>-based interface to kernel NFS client statistics."
msgstr ""
"Interface basée sur B<procfs> pour les statistiques du client NFS du noyau."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B</proc/mounts>"
msgstr "B</proc/mounts>"

#.  -------------------- SEE ALSO --------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<procfs>-based interface to the mounted filesystems."
msgstr "Interface basée sur B<procfs> pour les systèmes de fichiers montés."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#.  ---------------------- BUGS ----------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<rpc.nfsd>(8).  B<nfs>(5)."
msgstr "B<rpc.nfsd>(8), B<nfs>(5)."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The default output has been changed.  To get the old default output you must "
"run B<nfsstat --auto -2>."
msgstr ""
"Le format de sortie par défaut a changé. Pour obtenir l'ancienne sortie, "
"vous devez utiliser B<nfsstat --auto -2>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The function of the B<-v> and B<-a> options have changed. The B<-a> option "
"is now reserved for future use. The B<-v> does what the B<-a> option used to "
"do, and the new B<-[234]> options replace the B<-v> option."
msgstr ""
"Le rôle des options B<-v> et B<-a> a changé. L'option B<-a> est maintenant "
"réservée pour une future utilisation. B<-v> fait ce que l'option B<-a> "
"faisait, et les nouvelles options B<-[234]> remplacent l'option B<-v>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The B<Display> section should be more complete."
msgstr "La section B<Display> devrait être plus complète."

#.  -------------------- AUTHOR ----------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Further bugs can be found or reported at B<http://nfs.sf.net/>."
msgstr ""
"D'autres bogues pourront être trouvés ou remontés sur B<http://nfs.sf.net/>."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Olaf Kirch, E<lt>okir@suse.deE<gt>"
msgstr "Olaf Kirch, E<lt>okir@suse.deE<gt>"
