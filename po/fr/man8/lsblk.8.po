# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-25 22:27+01:00\n"
"PO-Revision-Date: 2015-07-05 18:06-0400\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "LSBLK"
msgstr "LSBLK"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "February 2013"
msgstr "février 2013"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "System Administration"
msgstr "Administration système"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "lsblk - list block devices"
msgstr "lsblk - Afficher les périphériques bloc"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<lsblk> [options] [I<device>...]"
msgstr "B<lsblk> [I<options>] [I<périphérique> ...]"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"B<lsblk> lists information about all available or the specified block "
"devices.  The B<lsblk> command reads the B<sysfs> filesystem and B<udev db> "
"to gather information. If the udev db is not available or lsblk is compiled "
"without udev support than it tries to read LABELs, UUIDs and filesystem "
"types from the block device. In this case root permissions are necessary."
msgstr ""
"B<lsblk> affiche des renseignements sur tout ou partie des périphériques "
"bloc disponibles. La commande B<lsblk> lit le système de fichiers I<sysfs> "
"et B<udev db> pour obtenir des renseignements."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The command prints all block devices (except RAM disks) in a tree-like "
"format by default.  Use B<lsblk --help> to get a list of all available "
"columns."
msgstr ""
"La commande affiche tous les périphériques bloc (sauf les disques RAM) au "
"format arborescent par défaut. Utilisez B<lsblk --help> pour obtenir une "
"liste de toutes les colonnes disponibles."

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"The default output, as well as the default output from options like B<--fs> "
"and B<--topology>, is subject to change.  So whenever possible, you should "
"avoid using default outputs in your scripts.  Always explicitly define "
"expected columns by using B<--output> I<columns-list> and B<--list> in "
"environments where a stable output is required."
msgstr ""
"La sortie par défaut, ainsi que la sortie par défaut avec des options comme "
"B<--fs> et B<--topology>, est susceptible d’être modifiée. Ainsi, autant que "
"possible, vous devriez éviter d’utiliser les sorties par défaut dans les "
"scripts. Définissez toujours explicitement les colonnes attendues en "
"utilisant B<--output> I<liste> dans les environnements nécessitant une "
"sortie stable."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Note that B<lsblk> might be executed in time when B<udev> does not have all "
"information about recently added or modified devices yet. In this case it is "
"recommended to use B<udevadm settle> before lsblk to synchronize with udev."
msgstr ""
"Remarquez que B<lsblk> pourrait être exécuté au moment où B<udev> n’a pas "
"encore tous les renseignements sur les périphériques récemment ajoutés ou "
"modifiés. Dans ce cas, utiliser B<udevadm settle> avant B<lsblk> est "
"recommandé pour synchroniser avec B<udev>."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-a>,B< --all>"
msgstr "B<-a>,B< --all>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "Also list empty devices and RAM disk devices."
msgstr "Afficher aussi les périphériques vides (ils sont ignorés par défaut)."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-b>,B< --bytes>"
msgstr "B<-b>,B< --bytes>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print the SIZE column in bytes rather than in a human-readable format."
msgstr ""
"Afficher la taille (colonne SIZE) en octet plutôt qu'en format lisible."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-D>,B< --discard>"
msgstr "B<-D>, B<--discard>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Print information about the discarding capabilities (TRIM, UNMAP) for each "
"device."
msgstr ""
"Afficher des renseignements sur les capacités d'abandon (TRIM, UNMAP) de "
"chaque périphérique."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-d>,B< --nodeps>"
msgstr "B<-d>, B<--nodeps>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Do not print holder devices or slaves.  For example, B<lsblk --nodeps /dev/"
"sda> prints information about the sda device only."
msgstr ""
"N'afficher ni les périphériques esclaves, ni les porteurs. Par exemple, "
"B<lsblk --nodeps /dev/sda> n'affiche que les renseignements relatifs au "
"périphérique sda."

#. type: TP
#: archlinux debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<-E>,B< --dedup >I<column>"
msgstr "B<-O>, B<--output> I<colonnes>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid ""
"Use I<column> as a de-duplication key to de-duplicate output tree. If the "
"key is not available for the device, or the device is a partition and "
"parental whole-disk device provides the same key than the device is always "
"printed."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid ""
"The usual use case is to de-duplicate output on system multi-path devices, "
"for example by B<-E WWN>."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-e>,B< --exclude >I<list>"
msgstr "B<-e>, B<--exclude> I<liste>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"Exclude the devices specified by the comma-separated I<list> of major device "
"numbers.  Note that RAM disks (major=1) are excluded by default if B<--all> "
"is not specified.  The filter is applied to the top-level devices only. This "
"may be confusing for B<--list> output format where hierarchy of the devices "
"is not obvious."
msgstr ""
"Exclure les périphériques indiqués par une I<liste> séparée par des virgules "
"de numéros majeurs. Remarquez que par défaut, les disques RAM (majeur=1) "
"sont exclus. Le filtre n’est appliqué qu’aux périphériques de premier niveau."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-f>,B< --fs>"
msgstr "B<-f>, B<--fs>"

#. type: Plain text
#: archlinux mageia-cauldron
#, fuzzy
msgid ""
"Output info about filesystems.  This option is equivalent to B<-o\\ NAME,"
"FSTYPE,LABEL,UUID,FSAVAIL,FSUSE%,MOUNTPOINT>.  The authoritative information "
"about filesystems and raids is provided by the B<blkid>(8)  command."
msgstr ""
"Afficher des renseignements sur les systèmes de fichiers. Cette option est "
"équivalente à B<-o NAME,FSTYPE,LABEL,MOUNTPOINT>. Les renseignements "
"officiels sur les systèmes de fichiers et les RAID sont fournis par la "
"commande B<blkid>(8)."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display help text and exit."
msgstr "Afficher un texte d'aide puis quitter."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-I>,B< --include >I<list>"
msgstr "B<-I>, B<--include> I<liste>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"Include devices specified by the comma-separated I<list> of major device "
"numbers.  The filter is applied to the top-level devices only. This may be "
"confusing for B<--list> output format where hierarchy of the devices is not "
"obvious."
msgstr ""
"Inclure les périphériques indiqués par une I<liste> séparée par des virgules "
"de numéros majeurs. Le filtre n’est appliqué qu’aux périphériques de premier "
"niveau."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-i>,B< --ascii>"
msgstr "B<-i>, B<--ascii>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Use ASCII characters for tree formatting."
msgstr "Utiliser des caractères ASCII pour le formatage d'arborescence."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-J>,B< --json>"
msgstr "B<-J>,B< --json>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid ""
"Use JSON output format.  It's strongly recommended to use B<--output> and "
"also B<--tree> if necessary."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-l>,B< --list>"
msgstr "B<-l>,B< --list>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid ""
"Produce output in the form of a list. The output does not provide "
"information about relationships between devices and since version 2.34 every "
"device is printed only once."
msgstr ""

#. type: TP
#: archlinux debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<-M>,B< --merge>"
msgstr "B<-M>, B<--move>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid ""
"Group parents of sub-trees to provide more readable output for RAIDs and "
"Multi-path devices. The tree-like output is required."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-m>,B< --perms>"
msgstr "B<-m>, B<--perms>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Output info about device owner, group and mode.  This option is equivalent "
"to B<-o\\ NAME,SIZE,OWNER,GROUP,MODE>."
msgstr ""
"Afficher des renseignements sur les propriétaire, groupe et mode et du "
"périphérique. Cette option est équivalente à B<-o NAME,SIZE,OWNER,GROUP,"
"MODE>."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-n>,B< --noheadings>"
msgstr "B<-n>,B< --noheadings>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Do not print a header line."
msgstr "Ne pas afficher de ligne d'en-tête."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-o>,B< --output >I<list>"
msgstr "B<-o>,B< --output >I<liste>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"Specify which output columns to print.  Use B<--help> to get a list of all "
"supported columns.  The columns may affect tree-like output.  The default is "
"to use tree for the column 'NAME' (see also B<--tree>)."
msgstr ""
"Indiquer les colonnes à afficher. Utilisez B<--help> pour obtenir une liste "
"de toutes les colonnes disponibles."

#. type: Plain text
#: archlinux mageia-cauldron
#, fuzzy
#| msgid ""
#| "The default list of columns may be extended if I<list> is specified in "
#| "the format I<+list> (e.g. B<lsblk -o +UUID>)."
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format I<+list> (e.g., B<lsblk -o +UUID>)."
msgstr ""
"La liste de colonnes par défaut peut être étendue si I<liste> est indiquée "
"sous la forme B<+>I<liste> (par exemple, B<lsblk -o +UUID>)."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-O>,B< --output-all >"
msgstr "B<-O>,B< --output-all >"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Output all available columns."
msgstr "Afficher toutes les colonnes disponibles."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-P>,B< --pairs>"
msgstr "B<-P>,B< --pairs>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Produce output in the form of key=\"value\" pairs.  All potentially unsafe "
"characters are hex-escaped (\\exE<lt>codeE<gt>)."
msgstr ""
"Produire la sortie sous la forme de couples I<clef>B<=\">I<valeur>B<\">. "
"Tous les caractères potentiellement non sûrs sont protégés de façon "
"hexadécimale (B<\\ex>I<code>)."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-p>,B< --paths>"
msgstr "B<-p>, B<--paths>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print full device paths."
msgstr "Afficher les chemins complets de périphérique."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-r>,B< --raw>"
msgstr "B<-r>,B< --raw>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Produce output in raw format.  All potentially unsafe characters are hex-"
"escaped (\\exE<lt>codeE<gt>) in the NAME, KNAME, LABEL, PARTLABEL and "
"MOUNTPOINT columns."
msgstr ""
"Produire la sortie au format brut. Tous les caractères potentiellement non "
"sûrs sont protégés de façon hexadécimale (B<\\ex>I<code>) dans les colonnes "
"NAME, KNAME, LABEL, PARTLABEL et MOUNTPOINT."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-S>,B< --scsi>"
msgstr "B<-S>,B< --scsi>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Output info about SCSI devices only.  All partitions, slaves and holder "
"devices are ignored."
msgstr ""
"N’afficher que les renseignements sur les périphériques SCSI. Toutes les "
"partitions et les périphériques esclaves et porteurs sont ignorés."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-s>,B< --inverse>"
msgstr "B<-s>,B< --inverse>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Print dependencies in inverse order. If the B<--list> output is requested "
"then the lines are still ordered by dependencies."
msgstr ""

#. type: TP
#: archlinux debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<-T>,B< --tree>[B<=>I<column>]"
msgstr "B<-x>, B<--sort> I<colonne>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid ""
"Force tree-like output format.  If I<column> is specified, then a tree is "
"printed in the column.  The default is NAME column."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-t>,B< --topology>"
msgstr "B<-t>,B< --topology>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"Output info about block-device topology.  This option is equivalent to B<-o"
"\\ NAME,ALIGNMENT,MIN-IO,OPT-IO,PHY-SEC,LOG-SEC,ROTA,SCHED,RQ-SIZE,RA,WSAME>."
msgstr ""
"Afficher des renseignements sur la topologie du périphérique bloc. Cette "
"option est équivalente à B<-o NAME,ALIGNMENT,MIN-IO,OPT-IO,PHY-SEC,LOG-SEC,"
"ROTA,SCHED,RQ-SIZE,WSAME>."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display version information and exit."
msgstr "Afficher le nom et la version du logiciel et quitter"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-x>,B< --sort >I<column>"
msgstr "B<-x>, B<--sort> I<colonne>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Sort output lines by I<column>. This option enables B<--list> output format "
"by default.  It is possible to use the option I<--tree> to force tree-like "
"output and than the tree branches are sorted by the I<column>."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<-z>,B< --zoned>"
msgstr "B<-z>, B<--zero>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "Print the zone model for each device."
msgstr "Afficher le nom du premier périphérique boucle disponible :"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B< --sysroot >I<directory>"
msgstr "B<-s>, B<--sysroot> I<répertoire>"

#. type: Plain text
#: archlinux mageia-cauldron
#, fuzzy
msgid ""
"Gather data for a Linux instance other than the instance from which the "
"lsblk command is issued.  The specified directory is the system root of the "
"Linux instance to be inspected.  The real device nodes in the target "
"directory can be replaced by text files with udev attributes."
msgstr ""
"Collecter les données de processeur pour une autre instance Linux que celle "
"utilisée pour la commande B<lscpu>. Le I<répertoire> indiqué est la racine "
"du système de l’instance Linux à inspecter."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux mageia-cauldron
#, fuzzy
#| msgid ""
#| "For partitions, some information (e.g. queue attributes) is inherited "
#| "from the parent device."
msgid ""
"For partitions, some information (e.g., queue attributes) is inherited from "
"the parent device."
msgstr ""
"Certains renseignements de partitions (par exemple les attributs de file) "
"sont hérités du périphérique parent."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"The B<lsblk> command needs to be able to look up each block device by major:"
"minor numbers, which is done by using I</sys/dev/block>.  This sysfs block "
"directory appeared in kernel 2.6.27 (October 2008).  In case of problems "
"with a new enough kernel, check that CONFIG_SYSFS was enabled at the time of "
"the kernel build."
msgstr ""
"La commande B<lsblk> doit pouvoir rechercher tous les périphériques blocs "
"par numéros majeur:mineur, ce qui est fait en utilisant I</sys/dev/block>. "
"Ce répertoire de systèmes de fichiers blocs est apparu dans le noyau 2.6.27 "
"(octobre 2008). En cas de problèmes avec un noyau assez récent, vérifiez que "
"CONFIG_SYSFS était activé au moment de la construction du noyau."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN CODES"
msgstr "CODES DE RETOUR"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "success"
msgstr "réussite"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "1"
msgstr "1"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "failure"
msgstr "échec"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "32"
msgstr "32"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "none of specified devices found"
msgstr "aucun des périphériques indiqués n’a été trouvé"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "64"
msgstr "64"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "some specified devices found, some not found"
msgstr "les périphériques indiqués n’ont pas tous été trouvés"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"Milan Broz E<lt>mbroz@redhat.comE<gt>\n"
"Karel Zak E<lt>kzak@redhat.comE<gt>\n"
msgstr ""
"Milan Broz E<lt>I<mbroz@redhat.com>E<gt>\n"
"Karel Zak E<lt>I<kzak@redhat.com>E<gt>\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ENVIRONNEMENT"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "LSBLK_DEBUG=all"
msgstr "B<LIBBLKID_DEBUG=all>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "enables lsblk debug output."
msgstr "Activer la sortie de débogage de libblkid."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "LIBBLKID_DEBUG=all"
msgstr "B<LIBBLKID_DEBUG=all>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "enables libblkid debug output."
msgstr "Activer la sortie de débogage de libblkid."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "LIBMOUNT_DEBUG=all"
msgstr "LIBMOUNT_DEBUG=all"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "enables libmount debug output."
msgstr "Activer la sortie de débogage de libmount."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "LIBSMARTCOLS_DEBUG=all"
msgstr "LIBSMARTCOLS_DEBUG=all"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "enables libsmartcols debug output."
msgstr "Activer la sortie de débogage de libsmartcols."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "LIBSMARTCOLS_DEBUG_PADDING=on"
msgstr "LIBSMARTCOLS_DEBUG_PADDING=on"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "use visible padding characters. Requires enabled LIBSMARTCOLS_DEBUG."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "B<ls>(1), B<blkid>(8), B<findmnt>(8)"
msgstr "B<blkid>(8), B<findfs>(8)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"The lsblk command is part of the util-linux package and is available from "
"https://www.kernel.org/pub/linux/utils/util-linux/."
msgstr ""
"La commande B<lsblk> fait partie du paquet util-linux, elle est disponible "
"sur E<lt>I<ftp://ftp.kernel.org/pub/linux/utils/util-linux/>E<gt>."

#. type: Plain text
#: debian-buster
msgid ""
"The default output, as well as the default output from options like B<--fs> "
"and B<--topology>, is subject to change.  So whenever possible, you should "
"avoid using default outputs in your scripts.  Always explicitly define "
"expected columns by using B<--output> I<columns-list> in environments where "
"a stable output is required."
msgstr ""
"La sortie par défaut, ainsi que la sortie par défaut avec des options comme "
"B<--fs> et B<--topology>, est susceptible d’être modifiée. Ainsi, autant que "
"possible, vous devriez éviter d’utiliser les sorties par défaut dans les "
"scripts. Définissez toujours explicitement les colonnes attendues en "
"utilisant B<--output> I<liste> dans les environnements nécessitant une "
"sortie stable."

#. type: Plain text
#: debian-buster
#, fuzzy
msgid ""
"Exclude the devices specified by the comma-separated I<list> of major device "
"numbers.  Note that RAM disks (major=1) are excluded by default if B<--all> "
"is no specified.  The filter is applied to the top-level devices only. This "
"maybe be confusing for B<--list> output format where hierarchy of the "
"devices is not obvious."
msgstr ""
"Exclure les périphériques indiqués par une I<liste> séparée par des virgules "
"de numéros majeurs. Remarquez que par défaut, les disques RAM (majeur=1) "
"sont exclus. Le filtre n’est appliqué qu’aux périphériques de premier niveau."

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
msgid ""
"Output info about filesystems.  This option is equivalent to B<-o\\ NAME,"
"FSTYPE,LABEL,UUID,MOUNTPOINT>.  The authoritative information about "
"filesystems and raids is provided by the B<blkid>(8)  command."
msgstr ""
"Afficher des renseignements sur les systèmes de fichiers. Cette option est "
"équivalente à B<-o NAME,FSTYPE,LABEL,MOUNTPOINT>. Les renseignements "
"officiels sur les systèmes de fichiers et les RAID sont fournis par la "
"commande B<blkid>(8)."

#. type: Plain text
#: debian-buster
#, fuzzy
msgid ""
"Include devices specified by the comma-separated I<list> of major device "
"numbers.  The filter is applied to the top-level devices only. This maybe be "
"confusing for B<--list> output format where hierarchy of the devices is not "
"obvious."
msgstr ""
"Inclure les périphériques indiqués par une I<liste> séparée par des virgules "
"de numéros majeurs. Le filtre n’est appliqué qu’aux périphériques de premier "
"niveau."

#. type: Plain text
#: debian-buster
#, fuzzy
msgid "Use JSON output format."
msgstr "Utiliser l'affichage au format brut."

#. type: Plain text
#: debian-buster
msgid "Produce output in the form of a list."
msgstr "Produire la sortie sous forme d’une liste."

#. type: Plain text
#: debian-buster
msgid ""
"Specify which output columns to print.  Use B<--help> to get a list of all "
"supported columns."
msgstr ""
"Indiquer les colonnes à afficher. Utilisez B<--help> pour obtenir une liste "
"de toutes les colonnes disponibles."

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format I<+list> (e.g. B<lsblk -o +UUID>)."
msgstr ""
"La liste de colonnes par défaut peut être étendue si I<liste> est indiquée "
"sous la forme B<+>I<liste> (par exemple, B<lsblk -o +UUID>)."

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
msgid ""
"Gather data for a Linux instance other than the instance from which the "
"lsblk command is issued.  The specified directory is the system root of the "
"Linux instance to be inspected.  This option is designed for the testing "
"purpose."
msgstr ""
"Collecter les données de processeur pour une autre instance Linux que celle "
"utilisée pour la commande B<lscpu>. Le I<répertoire> indiqué est la racine "
"du système de l’instance Linux à inspecter."

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"For partitions, some information (e.g. queue attributes) is inherited from "
"the parent device."
msgstr ""
"Certains renseignements de partitions (par exemple les attributs de file) "
"sont hérités du périphérique parent."
