# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2012.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2019-11-29 11:39+01:00\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"math/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CBRT"
msgstr "CBRT"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "cbrt, cbrtf, cbrtl - cube root function"
msgstr "cbrt, cbrtf, cbrtl - Fonction racine cubique"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid ""
"B<double cbrt(double >I<x>B<);>\n"
"B<float cbrtf(float >I<x>B<);>\n"
"B<long double cbrtl(long double >I<x>B<);>\n"
msgstr ""
"B<double erf(double >I<x>B<);>\n"
"B<float erff(float >I<x>B<);>\n"
"B<long double erfl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Link with I<-lm>."
msgstr "Effectuez l'édition des liens avec l'option I<-lm>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consultez "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<cbrt>():"
msgstr "B<cbrt>() :"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<cbrtf>(), B<cbrtl>():"
msgstr "B<cbrtf>(), B<cbrtl>() :"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"These functions return the (real) cube root of I<x>.  This function cannot "
"fail; every representable real value has a representable real cube root."
msgstr ""
"La fonction B<cbrt>() renvoie la racine cubique (réelle) de I<x>. La "
"fonction ne peut pas échouer\\ ; toute valeur réelle possède une racine "
"cubique calculable."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR DE RETOUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "These functions return the cube root of I<x>."
msgstr "Ces fonctions renvoient la racine cubique de I<x>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If I<x> is +0, -0, positive infinity, negative infinity, or NaN, I<x> is "
"returned."
msgstr ""
"Si I<x> vaut +0, -0, une valeur infinie positive ou négative, ou NaN, alors "
"I<x> est renvoyé."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "No errors occur."
msgstr "Aucune erreur ne se produit."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Pour la définition de la fonction Gamma, consultez B<tgamma>(3)."

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid ""
"B<cbrt>(),\n"
"B<cbrtf>(),\n"
"B<cbrtl>()"
msgstr "B<cbrtf>(), B<cbrtl>() :"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#.  .BR cbrt ()
#.  was a GNU extension. It is now a C99 requirement.
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<pow>(3), B<sqrt>(3)"
msgstr "B<pow>(3), B<sqrt>(3)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.01 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."
