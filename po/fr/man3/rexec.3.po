# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2019-11-29 12:35+01:00\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"net/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REXEC"
msgstr "REXEC"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "rexec, rexec_af - return stream to a remote command"
msgstr "rewec, rexec_af - Renvoyer un flux sur une commande distante"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>netdb.hE<gt>>\n"
msgstr "B<#include E<lt>netdb.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<int rexec(char **>I<ahost>B<, int >I<inport>B<, const char *>I<user>B<, >\n"
"B<          const char *>I<passwd>B<, const char *>I<cmd>B<, int *>I<fd2p>B<);>\n"
msgstr ""
"B<int rexec(char **>I<ahost>B<, int >I<inport>B<, const char *>I<user>B<, >\n"
"B<          const char *>I<passwd>B<, const char *>I<cmd>B<, int *>I<fd2p>B<);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<int rexec_af(char **>I<ahost>B<, int >I<inport>B<, const char *>I<user>B<, >\n"
"B<             const char *>I<passwd>B<, const char *>I<cmd>B<, int *>I<fd2p>B<,>\n"
"B<             sa_family_t >I<af>B<);>\n"
msgstr ""
"B<int rexec_af(char **>I<ahost>B<, int >I<inport>B<, const char *>I<user>B<, >\n"
"B<             const char *>I<passwd>B<, const char *>I<cmd>B<, int *>I<fd2p>B<,>\n"
"B<             sa_family_t >I<af>B<);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "B<rexec>(), B<rexec_af>():"
msgstr "B<rcmd>(3), B<rexecd>(8)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    In glibc up to and including 2.19:\n"
"        _BSD_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "This interface is obsoleted by B<rcmd>(3)."
msgstr "Cette interface est rendue obsolète par B<rcmd>(3)."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<rexec>()  function looks up the host I<*ahost> using "
"B<gethostbyname>(3), returning -1 if the host does not exist.  Otherwise, "
"I<*ahost> is set to the standard name of the host.  If a username and "
"password are both specified, then these are used to authenticate to the "
"foreign host; otherwise the environment and then the I<.netrc> file in "
"user's home directory are searched for appropriate information.  If all this "
"fails, the user is prompted for the information."
msgstr ""
"La fonction B<rexec>() cherche l'hôte I<*ahost> en utilisant "
"B<gethostbyname>(3), elle renvoie -1 si l'hôte n'existe pas. Sinon, "
"I<*ahost> est configuré avec le nom standard de l'hôte. Si un nom "
"d'utilisateur et un mot de passe sont également indiqués, ils sont utilisés "
"pour s'authentifier sur l'hôte\\ ; autrement, les informations appropriées "
"sont recherchée dans l'environnement puis dans le fichier I<.netrc> situé "
"dans le répertoire personnel de l'utilisateur. Si cela échoue, les "
"informations sont demandées à l'utilisateur."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The port I<inport> specifies which well-known DARPA Internet port to use for "
"the connection; the call I<getservbyname(\"exec\", \"tcp\")> (see "
"B<getservent>(3))  will return a pointer to a structure that contains the "
"necessary port.  The protocol for connection is described in detail in "
"B<rexecd>(8)."
msgstr ""
"Le port I<inport> indique le port Internet DARPA qui sera utilisé pour la "
"connexion\\ ; l'appel I<getservbyname(\"exec\", \"tcp\")> (consultez "
"B<getservent>(3)) renverra un pointeur sur une structure qui contient le "
"port nécessaire. Le protocole de la connexion est décrit en détails dans "
"B<rexecd>(8)."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"If the connection succeeds, a socket in the Internet domain of type "
"B<SOCK_STREAM> is returned to the caller, and given to the remote command as "
"I<stdin> and I<stdout>.  If I<fd2p> is nonzero, then an auxiliary channel to "
"a control process will be setup, and a file descriptor for it will be placed "
"in I<*fd2p>.  The control process will return diagnostic output from the "
"command (unit 2) on this channel, and will also accept bytes on this channel "
"as being UNIX signal numbers, to be forwarded to the process group of the "
"command.  The diagnostic information returned does not include remote "
"authorization failure, as the secondary connection is set up after "
"authorization has been verified.  If I<fd2p> is 0, then the I<stderr> (unit "
"2 of the remote command) will be made the same as the I<stdout> and no "
"provision is made for sending arbitrary signals to the remote process, "
"although you may be able to get its attention by using out-of-band data."
msgstr ""
"Si la connexion réussit, une socket dans le domaine Internet de type "
"B<SOCK_STREAM> est renvoyée à l'appelant et est fournie à la commande "
"distante comme I<stdin> et I<stdout>. Si I<fd2p> est non nul, un canal "
"auxiliaire sur un processus de contrôle sera configuré et un descripteur de "
"celui-ci sera placé dans I<*fd2p>. Le processus de contrôle renverra une "
"sortie de diagnostic de la commande (unité 2) sur ce canal et acceptera les "
"octets sur ce même canal comme étant des numéros de signaux UNIX à "
"transmettre au groupe du processus de la commande. L'information de "
"diagnostic n'inclut pas l'échec d'autorisation distante. Une connexion "
"secondaire est configurée après que l'autorisation a été vérifiée. Si "
"I<fd2p> vaut 0, I<stderr> (unité 2 de la commande distante) sera la même que "
"I<stdout> et aucun canal supplémentaire ne sera utilisé pour envoyer des "
"signaux arbitraires au processus distant, bien qu'il soit possible d'obtenir "
"son attention en transmettant des données supplémentaires."

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "rexec_af()"
msgstr "rexec_af()"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<rexec>()  function works over IPv4 (B<AF_INET>).  By contrast, the "
"B<rexec_af>()  function provides an extra argument, I<af>, that allows the "
"caller to select the protocol.  This argument can be specified as "
"B<AF_INET>, B<AF_INET6>, or B<AF_UNSPEC> (to allow the implementation to "
"select the protocol)."
msgstr ""
"La fonction B<rexec>() fonctionne en IPv4 (B<AF_INET>). En revanche, la "
"fonction B<rexec_af>() fournit un argument supplémentaire, I<af>, qui permet "
"à l'appelant de sélectionner le protocole. Cet argument peut être indiqué "
"comme B<AF_INET>, B<AF_INET6> ou B<AF_UNSPEC> (pour permettre à "
"l'implémentation de sélectionner le protocole)."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The B<rexec_af>()  function was added to glibc in version 2.2."
msgstr ""
"La fonction B<rexec_af>() a été ajoutée dans la version 2.2 de la glibc."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consultez "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "Interface"
msgstr "Interfaces /proc"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid ""
"B<rexec>(),\n"
"B<rexec_af>()"
msgstr "B<rcmd>(3), B<rexecd>(8)"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "Thread safety"
msgstr "T}\tSécurité multithread\tMT-Safe\n"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MT-Unsafe"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"These functions are not in POSIX.1.  The B<rexec>()  function first appeared "
"in 4.2BSD, and is present on the BSDs, Solaris, and many other systems.  The "
"B<rexec_af>()  function is more recent, and less widespread."
msgstr ""
"Ces fonctions ne sont pas dans POSIX.1-2001. La fonction B<rexec>() est "
"d'abord apparue dans BSD 4.2, et est présente sur BSD, Solaris et bien "
"d'autres systèmes. La fonction B<rexec_af>() est plus récente, donc moins "
"répandue."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<rexec>()  function sends the unencrypted password across the network."
msgstr ""
"La fonction B<rexec>() envoie des mots de passe non chiffrés sur le réseau."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The underlying service is considered a big security hole and therefore not "
"enabled on many sites; see B<rexecd>(8)  for explanations."
msgstr ""
"Le service sous-jacent est considéré comme un gros trou de sécurité et ne "
"devrait pas être activé sur bien des machines\\ ; consultez B<rexecd>(8) "
"pour plus d'explications."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<rcmd>(3), B<rexecd>(8)"
msgstr "B<rcmd>(3), B<rexecd>(8)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.01 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."
