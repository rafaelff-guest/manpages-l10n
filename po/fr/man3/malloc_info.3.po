# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999,2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2012-2014.
# Denis Barbier <barbier@debian.org>, 2006,2010.
# David Prévot <david@tilapin.org>, 2010-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2019-11-29 12:32+01:00\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"memory/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MALLOC_INFO"
msgstr "MALLOC_INFO"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2019-03-06"
msgstr "6 mars 2019"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "malloc_info - export malloc state to a stream"
msgstr "malloc_info - Exporter l'état de malloc vers un flux"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>malloc.hE<gt>>\n"
msgstr "B<#include E<lt>malloc.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<int malloc_info(int >I<options>B<, FILE *>I<stream>B<);>\n"
msgstr "B<int malloc_info(int >I<options>B<, FILE *>I<fp>B<);>\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"The B<malloc_info>()  function exports an XML string that describes the "
"current state of the memory-allocation implementation in the caller.  The "
"string is printed on the file stream I<stream>.  The exported string "
"includes information about all arenas (see B<malloc>(3))."
msgstr ""
"La fonction B<malloc_info>() exporte un flux XML décrivant l'état actuel "
"d'allocation mémoire pour l'appelant. Cette chaîne de caractères est "
"affichée sur le descripteur de fichier I<fp>. Cette chaîne contient les "
"informations concernant tous les enclos mémoires (« arenas »), consultez "
"B<malloc>(3)."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "As currently implemented, I<options> must be zero."
msgstr "Dans la mise en œuvre actuelle, le paramètre I<options> doit être nul."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR DE RETOUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"On success, B<malloc_info>()  returns 0; on error, it returns -1, with "
"I<errno> set to indicate the cause."
msgstr ""
"En cas de succès, B<malloc_info>() renvoie zéro. En cas d'erreur, -1 est "
"renvoyé et I<errno> contient le code d'erreur."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<options> was nonzero."
msgstr "Le paramètre I<options> n'était pas nul."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<malloc_info>()  was added to glibc in version 2.10."
msgstr "B<malloc_info>() est disponible depuis la glibc 2.10."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<malloc_info>()"
msgstr "B<malloc>(3)"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "This function is a GNU extension."
msgstr "Cette fonction est une extension GNU."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The memory-allocation information is provided as an XML string (rather than "
"a C structure)  because the information may change over time (according to "
"changes in the underlying implementation).  The output XML string includes a "
"version field."
msgstr ""
"L'information d'allocation mémoire est fournie sous forme de flux XML "
"(plutôt que de structure C) car les informations fournies pourront évoluer "
"dans le futur, en raison de modifications dans le code mis en œuvre. La "
"sortie XML comporte un champ de version."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<open_memstream>(3)  function can be used to send the output of "
"B<malloc_info>()  directly into a buffer in memory, rather than to a file."
msgstr ""
"La fonction B<open_memstream>(3) peut être utilisée pour envoyer la sortie "
"de B<malloc_info>() directement dans un tampon en mémoire plutôt que dans un "
"fichier."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<malloc_info>()  function is designed to address deficiencies in "
"B<malloc_stats>(3)  and B<mallinfo>(3)."
msgstr ""
"La fonction B<malloc_info>() est conçue pour combler les manques de "
"B<malloc_stats>(3) et B<mallinfo>(3)."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The program below takes up to four command-line arguments, of which the "
"first three are mandatory.  The first argument specifies the number of "
"threads that the program should create.  All of the threads, including the "
"main thread, allocate the number of blocks of memory specified by the second "
"argument.  The third argument controls the size of the blocks to be "
"allocated.  The main thread creates blocks of this size, the second thread "
"created by the program allocates blocks of twice this size, the third thread "
"allocates blocks of three times this size, and so on."
msgstr ""
"Le programme ci-dessous accepte jusqu'à quatre paramètres en ligne de "
"commande, dont les trois premiers sont obligatoires. Le premier paramètre "
"indique le nombre de processus légers (« threads ») que le programme doit "
"créer. Tous les threads, thread principal compris, allouent le nombre de "
"blocs de mémoire indiqué en deuxième paramètre. Le troisième paramètre "
"contrôle la taille des blocs à allouer. Le thread principal crée des blocs "
"de cette taille, le deuxième thread créé par le programme alloue des blocs "
"deux fois plus grands, le troisième thread alloue des blocs trois fois plus "
"grands, ainsi de suite."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The program calls B<malloc_info>()  twice to display the memory-allocation "
"state.  The first call takes place before any threads are created or memory "
"allocated.  The second call is performed after all threads have allocated "
"memory."
msgstr ""
"Le programme appelle B<malloc_info>() deux fois pour afficher l'état de "
"l'allocation mémoire. Le premier appel est effectué avant la création de "
"thread ou l'allocation de mémoire. Le deuxième appel est effectué une fois "
"que tous les threads ont alloué de la mémoire."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"In the following example, the command-line arguments specify the creation of "
"one additional thread, and both the main thread and the additional thread "
"allocate 10000 blocks of memory.  After the blocks of memory have been "
"allocated, B<malloc_info>()  shows the state of two allocation arenas."
msgstr ""
"Dans l'exemple suivant, les paramètres commandent la création d'un thread "
"additionnel, allouant 10000 blocs de mémoire, comme le thread principal. Une "
"fois les blocs de mémoire alloués, B<malloc_info>() affiche l'état des deux "
"enclos mémoire."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"$ B<getconf GNU_LIBC_VERSION>\n"
"glibc 2.13\n"
"$ B<./a.out 1 10000 100>\n"
"============ Before allocating blocks ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"
msgstr ""
"$ B<getconf GNU_LIBC_VERSION>\n"
"glibc 2.13\n"
"$ B<./a.out 1 10000 100>\n"
"============ Avant allocation des blocs ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"============ After allocating blocks ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1081344\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1081344\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>heap nr=\"1\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1032192\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1032192\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"2113536\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"2113536\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"
msgstr ""
"============ Après allocation des blocs ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1081344\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1081344\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>heap nr=\"1\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1032192\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1032192\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"2113536\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"2113536\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
msgstr ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"static size_t blockSize;\n"
"static int numThreads, numBlocks;\n"
msgstr ""
"static size_t tailleBloc;\n"
"static int nbThreads, nbBlocs;\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\e\n"
"                        } while (0)\n"
msgstr ""
"#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\e\n"
"                        } while (0)\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"static void *\n"
"thread_func(void *arg)\n"
"{\n"
"    int j;\n"
"    int tn = (int) arg;\n"
msgstr ""
"static void *\n"
"thread_func(void *arg)\n"
"{\n"
"    int j;\n"
"    int tn = (int) arg;\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    /* The multiplier \\(aq(2 + tn)\\(aq ensures that each thread (including\n"
"       the main thread) allocates a different amount of memory */\n"
msgstr ""
"    /* Le multiplicateur \\(aq(2 + tn)\\(aq s'assure que chaque thread (dont\n"
"       le thread principal) alloue une quantité de mémoire différente */\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    for (j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize * (2 + tn)) == NULL)\n"
"            errExit(\"malloc-thread\");\n"
msgstr ""
"    for (j = 0; j E<lt> nbBlocs; j++)\n"
"        if (malloc(tailleBloc * (2 + tn)) == NULL)\n"
"            errExit(\"malloc-thread\");\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    sleep(100);         /* Sleep until main thread terminates */\n"
"    return NULL;\n"
"}\n"
msgstr ""
"    sleep(100);         /* Attendre que le thread principal se termine */\n"
"    return NULL;\n"
"}\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int j, tn, sleepTime;\n"
"    pthread_t *thr;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int j, tn, tpsSommeil;\n"
"    pthread_t *thr;\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    if (argc E<lt> 4) {\n"
"        fprintf(stderr,\n"
"                \"%s num-threads num-blocks block-size [sleep-time]\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc E<lt> 4) {\n"
"        fprintf(stderr,\n"
"                \"%s nb-threads nb-blocs taille-bloc [tps-sommeil]\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    numThreads = atoi(argv[1]);\n"
"    numBlocks = atoi(argv[2]);\n"
"    blockSize = atoi(argv[3]);\n"
"    sleepTime = (argc E<gt> 4) ? atoi(argv[4]) : 0;\n"
msgstr ""
"    nbThreads = atoi(argv[1]);\n"
"    nbBlocs = atoi(argv[2]);\n"
"    tailleBloc = atoi(argv[3]);\n"
"    tpsSommeil = (argc E<gt> 4) ? atoi(argv[4]) : 0;\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    thr = calloc(numThreads, sizeof(pthread_t));\n"
"    if (thr == NULL)\n"
"        errExit(\"calloc\");\n"
msgstr ""
"    thr = calloc(nbThreads, sizeof(pthread_t));\n"
"    if (thr == NULL)\n"
"        errExit(\"calloc\");\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    printf(\"============ Before allocating blocks ============\\en\");\n"
"    malloc_info(0, stdout);\n"
msgstr ""
"    printf(\"============ Avant allocation des blocs ============\\en\");\n"
"    malloc_info(0, stdout);\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "    /* Create threads that allocate different amounts of memory */\n"
msgstr "    /* Crée les threads allouant des quantités de mémoire différentes */\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    for (tn = 0; tn E<lt> numThreads; tn++) {\n"
"        errno = pthread_create(&thr[tn], NULL, thread_func,\n"
"                               (void *) tn);\n"
"        if (errno != 0)\n"
"            errExit(\"pthread_create\");\n"
msgstr ""
"    for (tn = 0; tn E<lt> nbThreads; tn++) {\n"
"        errno = pthread_create(&thr[tn], NULL, thread_func,\n"
"                               (void *) tn);\n"
"        if (errno != 0)\n"
"            errExit(\"pthread_create\");\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"        /* If we add a sleep interval after the start-up of each\n"
"           thread, the threads likely won\\(aqt contend for malloc\n"
"           mutexes, and therefore additional arenas won\\(aqt be\n"
"           allocated (see malloc(3)). */\n"
msgstr ""
"        /* Si un intervalle de sommeil existe après le démarrage de\n"
"           chaque thread, les threads ne vont sans doute pas se battre\n"
"           pour les mutex malloc, et par conséquent les enclos mémoire\n"
"           supplémentaires ne seront pas alloués (consultez malloc(3)).\n"
"           */\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"        if (sleepTime E<gt> 0)\n"
"            sleep(sleepTime);\n"
"    }\n"
msgstr ""
"        if (tpsSommeil E<gt> 0)\n"
"            sleep(tpsSommeil);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "    /* The main thread also allocates some memory */\n"
msgstr "    /* Le thread principal alloue également de la mémoire */\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    for (j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize) == NULL)\n"
"            errExit(\"malloc\");\n"
msgstr ""
"    for (j = 0; j E<lt> nbBlocs; j++)\n"
"        if (malloc(tailleBloc) == NULL)\n"
"            errExit(\"malloc\");\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    sleep(2);           /* Give all threads a chance to\n"
"                           complete allocations */\n"
msgstr ""
"    sleep(2);           /* Donne à tous les threads une chance\n"
"                           de terminer les allocations */\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    printf(\"\\en============ After allocating blocks ============\\en\");\n"
"    malloc_info(0, stdout);\n"
msgstr ""
"    printf(\"\\en============ Après allocation des blocs ============\\en\");\n"
"    malloc_info(0, stdout);\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<mallinfo>(3), B<malloc>(3), B<malloc_stats>(3), B<mallopt>(3), "
"B<open_memstream>(3)"
msgstr ""
"B<mallinfo>(3), B<malloc>(3), B<malloc_stats>(3), B<mallopt>(3), "
"B<open_memstream>(3)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.01 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."
