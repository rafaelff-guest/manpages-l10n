# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Baptiste Holcroft <jean-baptiste@holcroft.fr>, 2018
# Grégoire Scano <gregoire.scano@malloc.fr>, 2019
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2019-11-29 12:38+01:00\n"
"PO-Revision-Date: 2019-08-12 14:40+0800\n"
"Last-Translator: Grégoire Scano <gregoire.scano@malloc.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "WCSTOK"
msgstr "WCSTOK"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2019-03-06"
msgstr "6 mars 2019"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "wcstok - split wide-character string into tokens"
msgstr "wcstok - Extraire des mots d'une chaîne de caractères larges"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr "B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<wchar_t *wcstok(wchar_t *>I<wcs>B<, const wchar_t *>I<delim>B<, wchar_t **>I<ptr>B<);>\n"
msgstr "B<wchar_t *wcstok(wchar_t *>I<wcs>B<, const wchar_t *>I<delim>B<, wchar_t **>I<ptr>B<);>\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<wcstok>()  function is the wide-character equivalent of the "
"B<strtok>(3)  function, with an added argument to make it multithread-safe.  "
"It can be used to split a wide-character string I<wcs> into tokens, where a "
"token is defined as a substring not containing any wide-characters from "
"I<delim>."
msgstr ""
"La fonction B<wcstok>() est l'équivalent pour les caractères larges de la "
"fonction B<strtok>(3), avec un argument supplémentaire permettant de "
"l'employer dans un contexte multithread. On peut l'utiliser pour découper la "
"chaîne de caractères larges I<wcs> en éléments syntaxiques (tokens), définis "
"comme des sous-chaînes ne contenant aucun caractère large contenu dans la "
"chaîne I<delim>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The search starts at I<wcs>, if I<wcs> is not NULL, or at I<*ptr>, if I<wcs> "
"is NULL.  First, any delimiter wide-characters are skipped, that is, the "
"pointer is advanced beyond any wide-characters which occur in I<delim>.  If "
"the end of the wide-character string is now reached, B<wcstok>()  returns "
"NULL, to indicate that no tokens were found, and stores an appropriate value "
"in I<*ptr>, so that subsequent calls to B<wcstok>()  will continue to return "
"NULL.  Otherwise, the B<wcstok>()  function recognizes the beginning of a "
"token and returns a pointer to it, but before doing that, it zero-terminates "
"the token by replacing the next wide-character which occurs in I<delim> with "
"a null wide character (L\\(aq\\e0\\(aq), and it updates I<*ptr> so that "
"subsequent calls will continue searching after the end of recognized token."
msgstr ""
"La recherche démarre en I<wcs> si I<wcs> n'est pas NULL, ou en I<*ptr> si "
"I<wcs> est NULL. D'abord tous les caractères larges délimiteurs sont "
"ignorés, c'est-à-dire que le pointeur est placé au-delà de tous les "
"caractères se trouvant dans I<delim>. Si la fin de la chaîne est atteinte, "
"B<wcstok>() renvoie NULL pour indiquer qu'aucun mot n'a été trouvé, et "
"stocke dans I<*ptr> une valeur qui forcera les éventuels appels suivants de "
"B<wcstok>() à renvoyer NULL. Sinon, la fonction considère avoir atteint le "
"début d'un mot, et renvoie un pointeur dessus après l'avoir fait suivre par "
"un caractère large nul (L\\(aq\\e0\\(aq) (en remplaçant le premier caractère "
"se trouvant dans I<delim>). Elle met à jour le pointeur I<*ptr> pour qu'un "
"nouvel appel de B<wcstok>() continue la recherche à la suite du mot trouvé."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR DE RETOUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<wcstok>()  function returns a pointer to the next token, or NULL if no "
"further token was found."
msgstr ""
"La fonction B<wcstok>() renvoie un pointeur sur le mot suivant, ou NULL si "
"aucun mot n'a été trouvé."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consultez "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<wcstok>()"
msgstr "B<wcstok>()"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The original I<wcs> wide-character string is destructively modified during "
"the operation."
msgstr ""
"La chaîne de caractères larges I<wcs> originale est modifiée de manière "
"destructrice durant l'opération."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The following code loops over the tokens contained in a wide-character "
"string."
msgstr ""
"Le code suivant parcourt les mots contenus dans la chaîne de caractères "
"larges."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"wchar_t *wcs = ...;\n"
"wchar_t *token;\n"
"wchar_t *state;\n"
"for (token = wcstok(wcs, \" \\et\\en\", &state);\n"
"    token != NULL;\n"
"    token = wcstok(NULL, \" \\et\\en\", &state)) {\n"
"    ...\n"
"}\n"
msgstr ""
"wchar_t * wcs = ...;\n"
"wchar_t * token;\n"
"wchar_t * etat;\n"
"for (token = wcstok(wcs, \" \\et\\en\", &etat);\n"
"    token != NULL;\n"
"    token = wcstok(NULL, \" \\et\\en\", &etat)) {\n"
"    ...\n"
"}\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<strtok>(3), B<wcschr>(3)"
msgstr "B<strtok>(3), B<wcschr>(3)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.01 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."
