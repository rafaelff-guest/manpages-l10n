# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Nicolas François <nicolas.francois@centraliens.net>, 2008-2010.
# Bastien Scher <bastien0705@gmail.com>, 2011-2013.
# David Prévot <david@tilapin.org>, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: coreutils manpages\n"
"POT-Creation-Date: 2019-11-16 21:22+01:00\n"
"PO-Revision-Date: 2014-10-30 13:28-0400\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bits\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SHUF"
msgstr "SHUF"

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2019"
msgstr "Novembre 2019"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.31"
msgstr "GNU coreutils 8.31"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "shuf - generate random permutations"
msgstr "shuf - Générer des permutations aléatoires"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<shuf> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]"
msgstr "B<shuf> [I<\\,OPTION\\/>] ... [I<\\,FICHIER\\/>]"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<shuf> I<\\,-e \\/>[I<\\,OPTION\\/>]... [I<\\,ARG\\/>]..."
msgstr "B<shuf> I<\\,-e \\/>[I<\\,OPTION\\/>]... [I<\\,PARAM\\/>]..."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<shuf> I<\\,-i LO-HI \\/>[I<\\,OPTION\\/>]..."
msgstr "B<shuf> I<\\,-i BAS-HAUT \\/>[I<\\,OPTION\\/>]..."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Write a random permutation of the input lines to standard output."
msgstr ""
"Afficher sur la sortie standard une permutation aléatoire des lignes "
"d'entrée."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"L'entrée standard est lue quand I<FICHIER> est omis ou quand I<FICHIER> vaut "
"« - »."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Les paramètres obligatoires pour les options de forme longue le sont aussi "
"pour les options de forme courte."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-e>, B<--echo>"
msgstr "B<-e>, B<--echo>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "treat each ARG as an input line"
msgstr "interpréter chaque PARAM comme une ligne d'entrée"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-i>, B<--input-range>=I<\\,LO-HI\\/>"
msgstr "B<-i>, B<--input-range>=I<\\,DÉBUT-FIN\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "treat each number LO through HI as an input line"
msgstr ""
"interpréter chaque nombre entre I<DÉBUT> et I<FIN> comme une ligne d'entrée"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-n>, B<--head-count>=I<\\,COUNT\\/>"
msgstr "B<-n>, B<--head-count>=I<\\,LIGNES\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output at most COUNT lines"
msgstr "afficher au plus LIGNES lignes"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FICHIER\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "write result to FILE instead of standard output"
msgstr "écrire le résultat dans le I<FICHIER> à la place de la sortie standard"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--random-source>=I<\\,FILE\\/>"
msgstr "B<--random-source>=I<\\,FICHIER\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "get random bytes from FILE"
msgstr "obtenir les octets aléatoires depuis I<FICHIER>"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-r>, B<--repeat>"
msgstr "B<-r>, B<--repeat>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output lines can be repeated"
msgstr "les lignes affichées peuvent être répétées"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "line delimiter is NUL, not newline"
msgstr "le délimiteur de lignes est l’octet NULL, pas le changement de ligne"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display this help and exit"
msgstr "Afficher l'aide-mémoire et quitter"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output version information and exit"
msgstr "Afficher le nom et la version du logiciel et quitter"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Written by Paul Eggert."
msgstr "Écrit par Paul Eggert."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Aide en ligne de GNU coreutils : E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Signaler toute erreur de traduction à E<lt>https://translationproject.org/"
"team/E<gt>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2019 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2019 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou supérieures E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ce programme est un logiciel libre. Vous pouvez le modifier et le "
"redistribuer. Il n'y a AUCUNE GARANTIE dans la mesure autorisée par la loi."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/shufE<gt>"
msgstr ""
"Documentation complète : E<lt>I<https://www.gnu.org/software/coreutils/"
"shuf>E<gt>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) shuf invocation\\(aq"
msgstr ""
"aussi disponible localement à l’aide de la commande : info \\(aq(coreutils) "
"shuf invocation\\(aq"

#. type: TH
#: debian-buster
#, no-wrap
msgid "February 2019"
msgstr "Février 2019"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr "GNU coreutils 8.30"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Report shuf translation bugs to E<lt>https://translationproject.org/team/"
"E<gt>"
msgstr ""
"Signaler toute erreur de traduction de shuf à E<lt>I<https://"
"translationproject.org/team/fr>E<gt>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2018 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou supérieures E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/shufE<gt>"
msgstr ""
"Documentation complète : E<lt>I<https://www.gnu.org/software/coreutils/"
"shuf>E<gt>"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr "Août 2019"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr "Mars 2019"
