# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-05 21:44+01:00\n"
"PO-Revision-Date: 2015-07-05 18:06-0400\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "TASKSET"
msgstr "TASKSET"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "August 2014"
msgstr "août 2014"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "taskset - set or retrieve a process's CPU affinity"
msgstr "taskset - Définir ou récupérer l'affinité processeur d'un processus"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<taskset> [options] I<mask\\ command\\ >[I<argument>...]"
msgstr "B<taskset> [I<options>] I<masque> I<commande> [I<paramètre> ...]"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<taskset> [options] B<-p> [I<mask>]\\ I<pid>"
msgstr "B<taskset> [I<options>] B<-p> [I<masque>] I<PID>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<taskset> is used to set or retrieve the CPU affinity of a running process "
"given its I<pid>, or to launch a new I<command> with a given CPU affinity.  "
"CPU affinity is a scheduler property that \"bonds\" a process to a given set "
"of CPUs on the system.  The Linux scheduler will honor the given CPU "
"affinity and the process will not run on any other CPUs.  Note that the "
"Linux scheduler also supports natural CPU affinity: the scheduler attempts "
"to keep processes on the same CPU as long as practical for performance "
"reasons.  Therefore, forcing a specific CPU affinity is useful only in "
"certain applications."
msgstr ""
"B<taskset> est généralement utilisé pour récupérer ou définir l'affinité "
"processeur d'un processus en cours d'exécution en donnant son I<PID> ou de "
"lancer une nouvelle I<commande> avec une affinité processeur fournie. "
"L'affinité processeur est une propriété de l'ordonnanceur qui « lie » un "
"processus à un ensemble de processeurs donné sur un système. L'ordonnanceur "
"de Linux respectera l'affinité processeur et le processus ne s'exécutera sur "
"aucun autre processeur. Notez que l'ordonnanceur Linux gère également "
"l'affinité processeur dite « naturelle » : l'ordonnanceur essaie de "
"maintenir les processus sur le même processeur tant que cela a du sens pour "
"des raisons de performances. Ainsi, forcer une affinité processeur "
"spécifique n'est utile que dans certaines applications."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"The CPU affinity is represented as a bitmask, with the lowest order bit "
"corresponding to the first logical CPU and the highest order bit "
"corresponding to the last logical CPU.  Not all CPUs may exist on a given "
"system but a mask may specify more CPUs than are present.  A retrieved mask "
"will reflect only the bits that correspond to CPUs physically on the "
"system.  If an invalid mask is given (i.e., one that corresponds to no valid "
"CPUs on the current system) an error is returned.  The masks may be "
"specified in hexadecimal (with or without a leading \"0x\"), or as a CPU "
"list with the B<--cpu-list> option.  For example,"
msgstr ""
"L'affinité du processeur est représentée par un I<masque> binaire, avec le "
"bit de poids le plus faible correspondant au premier processeur logique et "
"le bit de poids le plus fort au dernier processeur logique. Tous les "
"processeurs peuvent ne pas exister dans un système donné, mais un I<masque> "
"peut indiquer plus de processeurs que ceux qui sont présents. Un I<masque> "
"récupéré n'aura que les bits correspondants aux processeurs présents "
"physiquement sur le système. Si un I<masque> erroné est fourni (c'est-à-"
"dire, un I<masque> qui correspond à un processeur absent sur le système "
"actuel) une erreur est alors renvoyée. Les masques sont généralement codés "
"en hexadécimal. Par exemple :"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<0x00000001>"
msgstr "B<0x00000001>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "is processor #0,"
msgstr "correspond au processeur nº 0,"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<0x00000003>"
msgstr "B<0x00000003>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "is processors #0 and #1,"
msgstr "correspond aux processeurs nº 0 et nº 1,"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<0xFFFFFFFF>"
msgstr "B<0xFFFFFFFF>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "is processors #0 through #31,"
msgstr "correspond à tous les processeurs (nº 0 à nº 31)."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<32>"
msgstr "B<32>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "is processors #1, #4, and #5,"
msgstr "correspond aux processeurs nº 0 et nº 1,"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<--cpu-list\\ 0-2,6>"
msgstr "B<-c>,\\ B<--cpu-list>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "is processors #0, #1, #2, and #6."
msgstr "correspond aux processeurs nº 0 et nº 1,"

#. type: TP
#: archlinux debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<--cpu-list\\ 0-10:2>"
msgstr "B<-c>,\\ B<--cpu-list>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid ""
"is processors #0, #2, #4, #6, #8 and #10. The suffix \":N\" specifies stride "
"in the range, for example 0-10:3 is interpreted as 0,3,6,9 list."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"When B<taskset> returns, it is guaranteed that the given program has been "
"scheduled to a legal CPU."
msgstr ""
"Lorsque B<taskset> répond, il est garanti que le programme donné a été "
"dirigé vers un processeur existant."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-a>,\\ B<--all-tasks>"
msgstr "B<-a>,\\ B<--all-tasks>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Set or retrieve the CPU affinity of all the tasks (threads) for a given PID."
msgstr ""
"Définir ou récupérer l'affinité processeur de toutes les tâches (threads) "
"d'un I<PID> donné."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-c>,\\ B<--cpu-list>"
msgstr "B<-c>,\\ B<--cpu-list>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"Interpret I<mask> as numerical list of processors instead of a bitmask.  "
"Numbers are separated by commas and may include ranges.  For example: "
"B<0,5,8-11>."
msgstr ""
"Indiquer une liste numérique de processeurs au lieu d'un masque binaire. Les "
"I<nombres> sont séparés par des virgules, et peuvent désigner des "
"intervalles. Par exemple : B<0,5,8-11>."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-p>,\\ B<--pid>"
msgstr "B<-p>,\\ B<--pid>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Operate on an existing PID and do not launch a new task."
msgstr "Agir sur un I<PID> existant et ne pas lancer de nouvelle tâche."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-V>,\\ B<--version>"
msgstr "B<-V>,\\ B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display version information and exit."
msgstr "Afficher le nom et la version du logiciel et quitter"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-h>,\\ B<--help>"
msgstr "B<-h>,\\ B<--help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display help text and exit."
msgstr "Afficher un texte d'aide puis quitter."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "USAGE"
msgstr "UTILISATION"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "The default behavior is to run a new command with a given affinity mask:"
msgstr "Le comportement par défaut est d'exécuter une nouvelle commande avec un masque d'affinité donné :"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<taskset> I<mask> I<command\\ >[I<arguments>]"
msgstr "B<taskset> I<masque> I<commande\\ >[I<paramètres>]"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "You can also retrieve the CPU affinity of an existing task:"
msgstr "Vous pouvez également récupérer l'affinité processeur d'une tâche existante :"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<taskset -p> I<pid>"
msgstr "B<taskset -p> I<PID>"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Or set it:"
msgstr "ou la modifier :"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<taskset -p> I<mask pid>"
msgstr "B<taskset -p> I<masque PID>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "PERMISSIONS"
msgstr "PERMISSIONS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"A user can change the CPU affinity of a process belonging to the same user.  "
"A user must possess B<CAP_SYS_NICE> to change the CPU affinity of a process "
"belonging to another user.  A user can retrieve the affinity mask of any "
"process."
msgstr ""
"Un utilisateur peut modifier l’affinité processeur d’un processus lui "
"appartenant. Un utilisateur doit posséder la capacité B<CAP_SYS_NICE> pour "
"modifier l'affinité processeur d'un processus appartenant à un autre "
"utilisateur. Un utilisateur peut récupérer le masque d'affinité de n’importe "
"quel processus."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"B<chrt>(1), B<nice>(1), B<renice>(1), B<sched_getaffinity>(2), "
"B<sched_setaffinity>(2)"
msgstr ""
"B<chrt>(1), B<nice>(1), B<renice>(1), B<sched_setaffinity>(2), "
"B<sched_getaffinity>(2)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "See B<sched>(7)  for a description of the Linux scheduling scheme."
msgstr ""
"Consultez B<sched_setscheduler>(2) pour une description de l'organisation de "
"l'ordonnancement de Linux."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Written by Robert M. Love."
msgstr "Écrit par Robert M. Love."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2004 Robert M. Love.  This is free software; see the source "
"for copying conditions.  There is NO warranty; not even for MERCHANTABILITY "
"or FITNESS FOR A PARTICULAR PURPOSE."
msgstr ""
"Copyright \\(co 2004 Robert M. Love. C'est un logiciel libre ; consultez les "
"sources pour les conditions de copie. Il n'y a AUCUNE garantie ; même pas de "
"VALEUR MARCHANDE ou d'ADÉQUATION À UNE UTILISATION PARTICULIÈRE."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"The taskset command is part of the util-linux package and is available from "
"https://www.kernel.org/pub/linux/utils/util-linux/."
msgstr ""
"La commande B<taskset> fait partie du paquet util-linux, elle est disponible "
"sur E<lt>I<ftp://ftp.kernel.org/pub/linux/utils/util-linux/>E<gt>."
