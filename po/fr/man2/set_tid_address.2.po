# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2020-02-10 09:31+01:00\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"man2/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SET_TID_ADDRESS"
msgstr "SET_TID_ADDRESS"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "set_tid_address - set pointer to thread ID"
msgstr ""
"set_tid_address - Positionner un pointeur vers un identifiant de thread (TID)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>linux/unistd.hE<gt>>\n"
msgstr "B<#include E<lt>linux/unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<long set_tid_address(int *>I<tidptr>B<);>\n"
msgstr "B<long set_tid_address(int *>I<tidptr>B<);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<Note>: There is no glibc wrapper for this system call; see NOTES."
msgstr ""
"I<Remarque> : il n'existe pas de fonction glibc autour de cet appel "
"système ; consultez B<NOTES>."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For each thread, the kernel maintains two attributes (addresses) called "
"I<set_child_tid> and I<clear_child_tid>.  These two attributes contain the "
"value NULL by default."
msgstr ""
"Pour chaque thread, Le noyau maintient deux attributs (adresses) nommés "
"I<set_child_tid> et I<clear_child_tid>. Ces deux attributs contiennent la "
"valeur NULL par défaut."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "I<set_child_tid>"
msgstr "I<set_child_tid>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If a thread is started using B<clone>(2)  with the B<CLONE_CHILD_SETTID> "
"flag, I<set_child_tid> is set to the value passed in the I<ctid> argument of "
"that system call."
msgstr ""
"Si un thread est démarré en utilisant B<clone>(2) avec l'attribut "
"B<CLONE_CHILD_SETTID>, I<set_child_tid> est définie à la valeur passée à "
"l'argument I<ctid> de cet appel système."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"When I<set_child_tid> is set, the very first thing the new thread does is to "
"write its thread ID at this address."
msgstr ""
"Lorsque I<set_child_tid> est remplie, la toute première chose que le nouveau "
"thread fait est d'écrire son ID de thread à cette adresse."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "I<clear_child_tid>"
msgstr "I<clear_child_tid>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If a thread is started using B<clone>(2)  with the B<CLONE_CHILD_CLEARTID> "
"flag, I<clear_child_tid> is set to the value passed in the I<ctid> argument "
"of that system call."
msgstr ""
"Si un thread est démarré en utilisant B<clone>(2) avec l'attribut "
"B<CLONE_CHILD_CLEARTID>, I<clear_child_tid> est définie à la valeur passée à "
"l'argument I<ctid> de cet appel système."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The system call B<set_tid_address>()  sets the I<clear_child_tid> value for "
"the calling thread to I<tidptr>."
msgstr ""
"L'appel système B<set_tid_address>() remplit la valeur I<clear_child_tid> "
"pour le thread appelant à I<tidptr>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"When a thread whose I<clear_child_tid> is not NULL terminates, then, if the "
"thread is sharing memory with other threads, then 0 is written at the "
"address specified in I<clear_child_tid> and the kernel performs the "
"following operation:"
msgstr ""
"Lorsqu'un thread dont I<clear_child_tid> n'est pas NULL se termine, alors, "
"si le thread partage de la mémoire avec d'autres threads, 0 est écrit à "
"l'adresse indiquée dans I<clear_child_tid> et le noyau réalise l'opération "
"suivante :"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "    futex(clear_child_tid, FUTEX_WAKE, 1, NULL, NULL, 0);\n"
msgstr "    futex(clear_child_tid, FUTEX_WAKE, 1, NULL, NULL, 0);\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The effect of this operation is to wake a single thread that is performing a "
"futex wait on the memory location.  Errors from the futex wake operation are "
"ignored."
msgstr ""
"L'effet de cette opération est de réveiller un simple thread qui réalise une "
"attente futex à l'emplacement de la mémoire. Les erreurs de l'opération de "
"réveil du futex sont ignorées."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR DE RETOUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<set_tid_address>()  always returns the caller's thread ID."
msgstr ""
"B<set_tid_address>() renvoie toujours l'identifiant du thread appelant."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<set_tid_address>()  always succeeds."
msgstr "B<set_tid_address>() réussit toujours."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This call is present since Linux 2.5.48.  Details as given here are valid "
"since Linux 2.5.49."
msgstr ""
"Cet appel est présent depuis Linux 2.5.48. Les détails fournis ici sont "
"valides depuis Linux 2.5.49."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "This system call is Linux-specific."
msgstr "Cet appel système est spécifique à Linux."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Glibc does not provide a wrapper for this system call; call it using "
"B<syscall>(2)."
msgstr ""
"La glibc ne fournit pas de fonction autour de cet appel système\\ ; utilisez "
"B<syscall>(2) pour l'appeler."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<clone>(2), B<futex>(2), B<gettid>(2)"
msgstr "B<clone>(2), B<futex>(2), B<gettid>(2)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: debian-unstable
msgid ""
"This page is part of release 5.05 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.05 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.01 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."
