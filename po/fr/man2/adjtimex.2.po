# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2012-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2020-02-10 09:29+01:00\n"
"PO-Revision-Date: 2018-09-10 20:56+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"time/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ADJTIMEX"
msgstr "ADJTIMEX"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2019-03-06"
msgstr "6 mars 2019"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "adjtimex, ntp_adjtime - tune kernel clock"
msgstr "adjtimex - Régler l'horloge du noyau (kernel clock)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>sys/timex.hE<gt>>\n"
msgstr "B<#include E<lt>sys/timex.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<int adjtimex(struct timex *>I<buf>B<);>\n"
msgstr "B<int adjtimex(struct timex *>I<buf>B<);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<int ntp_adjtime(struct timex *>I<buf>B<);>\n"
msgstr "B<int adjtimex(struct timex *>I<buf>B<);>\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"Linux uses David L.\\& Mills' clock adjustment algorithm (see RFC\\ 5905).  "
"The system call B<adjtimex>()  reads and optionally sets adjustment "
"parameters for this algorithm.  It takes a pointer to a I<timex> structure, "
"updates kernel parameters from (selected) field values, and returns the same "
"structure updated with the current kernel values.  This structure is "
"declared as follows:"
msgstr ""
"Linux utilise l'algorithme d'ajustement d'horloge de David L. Mills (RFC "
"1305). L'appel système B<adjtimex>() lit, et écrit éventuellement les "
"paramètres d'ajustement pour cet algorithme. Il utilise un pointeur sur une "
"structure I<timex> pour mettre à jour les paramètres du noyau avec les "
"valeurs de ses champs, et renvoyer la même structure avec les valeurs "
"actuelles du noyau. La structure est déclarée comme suit\\ :"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"struct timex {\n"
"    int  modes;      /* Mode selector */\n"
"    long offset;     /* Time offset; nanoseconds, if STA_NANO\n"
"                        status flag is set, otherwise\n"
"                        microseconds */\n"
"    long freq;       /* Frequency offset; see NOTES for units */\n"
"    long maxerror;   /* Maximum error (microseconds) */\n"
"    long esterror;   /* Estimated error (microseconds) */\n"
"    int  status;     /* Clock command/status */\n"
"    long constant;   /* PLL (phase-locked loop) time constant */\n"
"    long precision;  /* Clock precision\n"
"                        (microseconds, read-only) */\n"
"    long tolerance;  /* Clock frequency tolerance (read-only);\n"
"                        see NOTES for units */\n"
"    struct timeval time;\n"
"                     /* Current time (read-only, except for\n"
"                        ADJ_SETOFFSET); upon return, time.tv_usec\n"
"                        contains nanoseconds, if STA_NANO status\n"
"                        flag is set, otherwise microseconds */\n"
"    long tick;       /* Microseconds between clock ticks */\n"
"    long ppsfreq;    /* PPS (pulse per second) frequency\n"
"                        (read-only); see NOTES for units */\n"
"    long jitter;     /* PPS jitter (read-only); nanoseconds, if\n"
"                        STA_NANO status flag is set, otherwise\n"
"                        microseconds */\n"
"    int  shift;      /* PPS interval duration\n"
"                        (seconds, read-only) */\n"
"    long stabil;     /* PPS stability (read-only);\n"
"                        see NOTES for units */\n"
"    long jitcnt;     /* PPS count of jitter limit exceeded\n"
"                        events (read-only) */\n"
"    long calcnt;     /* PPS count of calibration intervals\n"
"                        (read-only) */\n"
"    long errcnt;     /* PPS count of calibration errors\n"
"                        (read-only) */\n"
"    long stbcnt;     /* PPS count of stability limit exceeded\n"
"                        events (read-only) */\n"
"    int tai;         /* TAI offset, as set by previous ADJ_TAI\n"
"                        operation (seconds, read-only,\n"
"                        since Linux 2.6.26) */\n"
"    /* Further padding bytes to allow for future expansion */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"The I<modes> field determines which parameters, if any, to set.  (As "
"described later in this page, the constants used for B<ntp_adjtime>()  are "
"equivalent but differently named.)  It is a bit mask containing a bitwise-"
"I<or> combination of zero or more of the following bits:"
msgstr ""
"Le champ I<modes> détermine les paramètres éventuels à écrire. Il contient "
"un OU binaire «\\ |\\ » entre les valeurs suivantes\\ :"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ADJ_OFFSET>"
msgstr ""

#.  commit 074b3b87941c99bc0ce35385b5817924b1ed0c23
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Set time offset from I<buf.offset>.  Since Linux 2.6.26, the supplied value "
"is clamped to the range (-0.5s, +0.5s).  In older kernels, an B<EINVAL> "
"error occurs if the supplied value is out of range."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ADJ_FREQUENCY>"
msgstr ""

#.  commit 074b3b87941c99bc0ce35385b5817924b1ed0c23
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Set frequency offset from I<buf.freq>.  Since Linux 2.6.26, the supplied "
"value is clamped to the range (-32768000, +32768000).  In older kernels, an "
"B<EINVAL> error occurs if the supplied value is out of range."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ADJ_MAXERROR>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set maximum time error from I<buf.maxerror>."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ADJ_ESTERROR>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set estimated time error from I<buf.esterror>."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ADJ_STATUS>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Set clock status bits from I<buf.status>.  A description of these bits is "
"provided below."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<ADJ_TIMECONST>"
msgstr "ADJTIME"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Set PLL time constant from I<buf.constant>.  If the B<STA_NANO> status flag "
"(see below) is clear, the kernel adds 4 to this value."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<ADJ_SETOFFSET> (since Linux 2.6.39)"
msgstr "B<CLOCK_THREAD_CPUTIME_ID> (depuis Linux 2.6.12)"

#.  commit 094aa1881fdc1b8889b442eb3511b31f3ec2b762
#.  Author: Richard Cochran <richardcochran@gmail.com>
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Add I<buf.time> to the current time.  If I<buf.status> includes the "
"B<ADJ_NANO> flag, then I<buf.time.tv_usec> is interpreted as a nanosecond "
"value; otherwise it is interpreted as microseconds."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<ADJ_MICRO> (since Linux 2.6.26)"
msgstr "B<CLOCK_THREAD_CPUTIME_ID> (depuis Linux 2.6.12)"

#.  commit eea83d896e318bda54be2d2770d2c5d6668d11db
#.  Author: Roman Zippel <zippel@linux-m68k.org>
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Select microsecond resolution."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<ADJ_NANO> (since Linux 2.6.26)"
msgstr "B<CLOCK_THREAD_CPUTIME_ID> (depuis Linux 2.6.12)"

#.  commit eea83d896e318bda54be2d2770d2c5d6668d11db
#.  Author: Roman Zippel <zippel@linux-m68k.org>
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Select nanosecond resolution.  Only one of B<ADJ_MICRO> and B<ADJ_NANO> "
"should be specified."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<ADJ_TAI> (since Linux 2.6.26)"
msgstr "B<CLOCK_THREAD_CPUTIME_ID> (depuis Linux 2.6.12)"

#.  commit 153b5d054ac2d98ea0d86504884326b6777f683d
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set TAI (Atomic International Time) offset from I<buf.constant>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<ADJ_TAI> should not be used in conjunction with B<ADJ_TIMECONST>, since "
"the latter mode also employs the I<buf.constant> field."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For a complete explanation of TAI and the difference between TAI and UTC, "
"see E<.UR http://www.bipm.org/en/bipm/tai/tai.html> I<BIPM> E<.UE>"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ADJ_TICK>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set tick value from I<buf.tick>."
msgstr ""

#.  In general, the other bits are ignored, but ADJ_OFFSET_SINGLESHOT 0x8001
#.  ORed with ADJ_NANO (0x2000) gives 0xa0001 == ADJ_OFFSET_SS_READ!!
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Alternatively, I<modes> can be specified as either of the following "
"(multibit mask) values, in which case other bits should not be specified in "
"I<modes>:"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ADJ_OFFSET_SINGLESHOT>"
msgstr ""

#.  In user space, ADJ_OFFSET_SINGLESHOT is 0x8001
#.  In kernel space it is 0x0001, and must be ANDed with ADJ_ADJTIME (0x8000)
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Old-fashioned B<adjtime>(): (gradually) adjust time by value specified in "
"I<buf.offset>, which specifies an adjustment in microseconds."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<ADJ_OFFSET_SS_READ> (functional since Linux 2.6.28)"
msgstr "B<CLOCK_PROCESS_CPUTIME_ID> (depuis Linux 2.6.12)"

#.  In user space, ADJ_OFFSET_SS_READ is 0xa001
#.  In kernel space there is ADJ_OFFSET_READONLY (0x2000) anded with
#.  ADJ_ADJTIME (0x8000) and ADJ_OFFSET_SINGLESHOT (0x0001) to give 0xa001)
#.  commit 52bfb36050c8529d9031d2c2513b281a360922ec
#.  commit 916c7a855174e3b53d182b97a26b2e27a29726a1
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Return (in I<buf.offset>)  the remaining amount of time to be adjusted after "
"an earlier B<ADJ_OFFSET_SINGLESHOT> operation.  This feature was added in "
"Linux 2.6.24, but did not work correctly until Linux 2.6.28."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"Ordinary users are restricted to a value of either 0 or "
"B<ADJ_OFFSET_SS_READ> for I<modes>.  Only the superuser may set any "
"parameters."
msgstr ""
"Les utilisateurs normaux sont limités à une valeur de I<modes> nulle, seul "
"le superutilisateur peut écrire les paramètres."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The I<buf.status> field is a bit mask that is used to set and/or retrieve "
"status bits associated with the NTP implementation.  Some bits in the mask "
"are both readable and settable, while others are read-only."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_PLL> (read-write)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Enable phase-locked loop (PLL) updates via B<ADJ_OFFSET>."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_PPSFREQ> (read-write)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Enable PPS (pulse-per-second) frequency discipline."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_PPSTIME> (read-write)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Enable PPS time discipline."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_FLL> (read-write)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Select frequency-locked loop (FLL) mode."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_INS> (read-write)"
msgstr ""

#
#.  John Stultz;
#.      Usually this is written as extending the day by one second,
#.      which is represented as:
#.         23:59:59
#.         23:59:60
#.         00:00:00
#.      But since posix cannot represent 23:59:60, we repeat the last second:
#.         23:59:59 + TIME_INS
#.         23:59:59 + TIME_OOP
#.         00:00:00 + TIME_WAIT
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Insert a leap second after the last second of the UTC day, thus extending "
"the last minute of the day by one second.  Leap-second insertion will occur "
"each day, so long as this flag remains set."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_DEL> (read-write)"
msgstr ""

#.  John Stultz:
#.      Similarly the progression here is:
#.         23:59:57 + TIME_DEL
#.         23:59:58 + TIME_DEL
#.         00:00:00 + TIME_WAIT
#.  FIXME Does there need to be a statement that it is nonsensical to set
#.  to set both STA_INS and STA_DEL?
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Delete a leap second at the last second of the UTC day.  Leap second "
"deletion will occur each day, so long as this flag remains set."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_UNSYNC> (read-write)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Clock unsynchronized."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_FREQHOLD> (read-write)"
msgstr ""

#.  Following text from John Stultz:
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Hold frequency.  Normally adjustments made via B<ADJ_OFFSET> result in "
"dampened frequency adjustments also being made.  So a single call corrects "
"the current offset, but as offsets in the same direction are made "
"repeatedly, the small frequency adjustments will accumulate to fix the long-"
"term skew."
msgstr ""

#.  According to the Kernel Application Program Interface document,
#.  STA_FREQHOLD is not used by the NTP version 4 daemon
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This flag prevents the small frequency adjustment from being made when "
"correcting for an B<ADJ_OFFSET> value."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_PPSSIGNAL> (read-only)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "A valid PPS (pulse-per-second) signal is present."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_PPSJITTER> (read-only)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "PPS signal jitter exceeded."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_PPSWANDER> (read-only)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "PPS signal wander exceeded."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_PPSERROR> (read-only)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "PPS signal calibration error."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_CLOCKERR> (read-only)"
msgstr ""

#.  Not set in current kernel (4.5), but checked in a few places
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Clock hardware fault."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STA_NANO> (read-only; since Linux 2.6.26)"
msgstr ""

#.  commit eea83d896e318bda54be2d2770d2c5d6668d11db
#.  Author: Roman Zippel <zippel@linux-m68k.org>
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Resolution (0 = microsecond, 1 = nanoseconds).  Set via B<ADJ_NANO>, cleared "
"via B<ADJ_MICRO>."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<STA_MODE> (since Linux 2.6.26)"
msgstr "B<CLOCK_THREAD_CPUTIME_ID> (depuis Linux 2.6.12)"

#.  commit eea83d896e318bda54be2d2770d2c5d6668d11db
#.  Author: Roman Zippel <zippel@linux-m68k.org>
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Mode (0 = Phase Locked Loop, 1 = Frequency Locked Loop)."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<STA_CLK> (read-only; since Linux 2.6.26)"
msgstr "B<CLOCK_THREAD_CPUTIME_ID> (depuis Linux 2.6.12)"

#.  commit eea83d896e318bda54be2d2770d2c5d6668d11db
#.  Author: Roman Zippel <zippel@linux-m68k.org>
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Clock source (0 = A, 1 = B); currently unused."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Attempts to set read-only I<status> bits are silently ignored."
msgstr ""

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ntp_adjtime ()"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<ntp_adjtime>()  library function (described in the NTP \"Kernel "
"Application Program API\", KAPI)  is a more portable interface for "
"performing the same task as B<adjtimex>().  Other than the following points, "
"it is identical to B<adjtime>():"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The constants used in I<modes> are prefixed with \"MOD_\" rather than \"ADJ_"
"\", and have the same suffixes (thus, B<MOD_OFFSET>, B<MOD_FREQUENCY>, and "
"so on), other than the exceptions noted in the following points."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<MOD_CLKA> is the synonym for B<ADJ_OFFSET_SINGLESHOT>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<MOD_CLKB> is the synonym for B<ADJ_TICK>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The is no synonym for B<ADJ_OFFSET_SS_READ>, which is not described in the "
"KAPI."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR DE RETOUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"On success, B<adjtimex>()  and B<ntp_adjtime>()  return the clock state; "
"that is, one of the following values:"
msgstr "S'il réussit, B<adjtimex>() renvoie l'état de l'horloge\\ :"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<TIME_OK>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Clock synchronized, no leap second adjustment pending."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<TIME_INS>"
msgstr "TIMES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Indicates that a leap second will be added at the end of the UTC day."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<TIME_DEL>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Indicates that a leap second will be deleted at the end of the UTC day."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<TIME_OOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Insertion of a leap second is in progress."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<TIME_WAIT>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"A leap-second insertion or deletion has been completed.  This value will be "
"returned until the next B<ADJ_STATUS> operation clears the B<STA_INS> and "
"B<STA_DEL> flags."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<TIME_ERROR>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The system clock is not synchronized to a reliable server.  This value is "
"returned when any of the following holds true:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Either B<STA_UNSYNC> or B<STA_CLOCKERR> is set."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<STA_PPSSIGNAL> is clear and either B<STA_PPSFREQ> or B<STA_PPSTIME> is set."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<STA_PPSTIME> and B<STA_PPSJITTER> are both set."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<STA_PPSFREQ> is set and either B<STA_PPSWANDER> or B<STA_PPSJITTER> is set."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The symbolic name B<TIME_BAD> is a synonym for B<TIME_ERROR>, provided for "
"backward compatibility."
msgstr ""

#.  commit 6b43ae8a619d17c4935c3320d2ef9e92bdeed05d changed to asynchronous
#.   operation, so we can no longer rely on the return code.
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Note that starting with Linux 3.4, the call operates asynchronously and the "
"return value usually will not reflect a state change caused by the call "
"itself."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "On failure, these calls return -1 and set I<errno>."
msgstr "En cas d'échec B<adjtimex>() renvoie -1 et remplit I<errno>."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<buf> does not point to writable memory."
msgstr ""
"I<buf> pointe en dehors de l'espace d'adressage accessible en écriture."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EINVAL> (kernels before Linux 2.6.26)"
msgstr ""

#.  From a quick glance, it appears there was no clamping or range check
#.  for buf.freq in kernels before 2.0
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"An attempt was made to set I<buf.freq> to a value outside the range "
"(-33554432, +33554432)."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"An attempt was made to set I<buf.offset> to a value outside the permitted "
"range.  In kernels before Linux 2.0, the permitted range was (-131072, "
"+131072).  From Linux 2.0 onwards, the permitted range was (-512000, "
"+512000)."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"An attempt was made to set I<buf.status> to a value other than those listed "
"above."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"An attempt was made to set I<buf.tick> to a value outside the range 900000/"
"B<HZ> to 1100000/B<HZ>, where B<HZ> is the system timer interrupt frequency."
msgstr ""
"Une tentative est faite de remplir I<buf.offset> en dehors de l'intervalle "
"-131071 à +131071, ou de mettre I<buf.status> a une valeur autre que celles "
"listées ci\\(hydessus, ou I<buf.tick> en dehors de l'intervalle 900000/B<HZ> "
"à 1100000/B<HZ>, où B<HZ> est la fréquence d'interruption de l'horloge "
"système."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"I<buf.modes> is neither 0 nor B<ADJ_OFFSET_SS_READ>, and the caller does not "
"have sufficient privilege.  Under Linux, the B<CAP_SYS_TIME> capability is "
"required."
msgstr ""
"I<buf.modes> est non nul, et l'appelant n'a pas suffisamment de privilèges. "
"Sous Linux, la capacité B<CAP_SYS_TIME> est nécessaire."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
msgid "B<ntp_adjtime>()"
msgstr "B<strftime>(3)"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Neither of these interfaces is described in POSIX.1"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"B<adjtimex>()  is Linux-specific and should not be used in programs intended "
"to be portable."
msgstr ""
"B<adjtimex>() est spécifique à Linux, et ne doit pas être employé dans des "
"programmes destinés à être portés sur d'autres systèmes. Consultez "
"B<adjtime>(3) pour une méthode plus portable, mais moins flexible, de "
"configurer l'horloge système."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The preferred API for the NTP daemon is B<ntp_adjtime>()."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"In struct I<timex>, I<freq>, I<ppsfreq>, and I<stabil> are ppm (parts per "
"million) with a 16-bit fractional part, which means that a value of 1 in one "
"of those fields actually means 2^-16 ppm, and 2^16=65536 is 1 ppm.  This is "
"the case for both input values (in the case of I<freq>)  and output values."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The leap-second processing triggered by B<STA_INS> and B<STA_DEL> is done by "
"the kernel in timer context.  Thus, it will take one tick into the second "
"for the leap second to be inserted or deleted."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"B<settimeofday>(2), B<adjtime>(3), B<ntp_gettime>(3), B<capabilities>(7), "
"B<time>(7), B<adjtimex>(8), B<hwclock>(8)"
msgstr ""
"B<settimeofday>(2), B<adjtime>(3), B<capabilities>(7), B<time>(7), "
"B<adjtimex>(8)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"E<.UR http://www.slac.stanford.edu/comp/unix/\\:package/\\:rtems/\\:src/\\:"
"ssrlApps/\\:ntpNanoclock/\\:api.htm> NTP \"Kernel Application Program "
"Interface\" E<.UE>"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: debian-unstable
msgid ""
"This page is part of release 5.05 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.05 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.01 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."
