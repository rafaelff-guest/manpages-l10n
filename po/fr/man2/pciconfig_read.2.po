# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2020-02-10 09:30+01:00\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"man2/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "PCICONFIG_READ"
msgstr "PCICONFIG_READ"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2016-07-17"
msgstr "17 juillet 2016"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"pciconfig_read, pciconfig_write, pciconfig_iobase - pci device information "
"handling"
msgstr ""
"pciconfig_read, pciconfig_write, pciconfig_iobase - Gestion d'information "
"des périphériques pci"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>pci.hE<gt>>\n"
msgstr "B<#include E<lt>pci.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<int pciconfig_read(unsigned long >I<bus>B<, unsigned long >I<dfn>B<,>\n"
"B<          unsigned long >I<off>B<, unsigned long >I<len>B<, void *>I<buf>B<);>\n"
"B<int pciconfig_write(unsigned long >I<bus>B<, unsigned long >I<dfn>B<,>\n"
"B<          unsigned long >I<off>B<, unsigned long >I<len>B<, void *>I<buf>B<);>\n"
"B<int pciconfig_iobase(long >I<which>B<, unsigned long >I<bus>B<,>\n"
"B<          unsigned long >I<devfn>B<);>\n"
msgstr ""
"B<int pciconfig_read(unsigned long >I<bus>B<, unsigned long >I<dfn>B<,>\n"
"B<          unsigned long >I<off>B<, unsigned long >I<len>B<, void *>I<buf>B<);>\n"
"B<int pciconfig_write(unsigned long >I<bus>B<, unsigned long >I<dfn>B<,>\n"
"B<          unsigned long >I<off>B<, unsigned long >I<len>B<, void *>I<buf>B<);>\n"
"B<int pciconfig_iobase(long >I<which>B<, unsigned long >I<bus>B<,>\n"
"B<          unsigned long >I<devfn>B<);>\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Most of the interaction with PCI devices is already handled by the kernel "
"PCI layer, and thus these calls should not normally need to be accessed from "
"user space."
msgstr ""
"La plupart des interactions avec les périphériques PCI est déjà gérée par la "
"couche PCI du noyau, aussi il ne devrait pas être nécessaire d'accéder à ces "
"appels à partir de l'espace utilisateur."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<pciconfig_read>()"
msgstr "B<pciconfig_read>()"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Reads to I<buf> from device I<dev> at offset I<off> value."
msgstr "Lit dans I<buf> à partir du périphérique I<dev> à la position I<off>."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<pciconfig_write>()"
msgstr "B<pciconfig_write>()"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Writes from I<buf> to device I<dev> at offset I<off> value."
msgstr ""
"Écrit à partir de I<buf> vers le périphérique I<dev> à la position I<off>."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<pciconfig_iobase>()"
msgstr "B<pciconfig_iobase>()"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"You pass it a bus/devfn pair and get a physical address for either the "
"memory offset (for things like prep, this is 0xc0000000), the IO base for "
"PIO cycles, or the ISA holes if any."
msgstr ""
"Vous lui passez une paire «\\ bus/devfn\\ » et vous récupérez une adresse "
"physique pour chacun des positions mémoire (pour des choses comme prep, "
"c'est 0xc0000000), l'adresse d'entrée-sortie (IO) de base pour les cycles "
"PIO ou bien les trous ISA s'il y en a."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR DE RETOUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"On success, zero is returned.  On error, -1 is returned and I<errno> is set "
"appropriately."
msgstr ""
"S'il réussit, cet appel système renvoie 0. S'il échoue, il renvoie -1 et "
"remplit I<errno> en conséquence."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Returns information on locations of various I/O regions in physical memory "
"according to the I<which> value.  Values for I<which> are: "
"B<IOBASE_BRIDGE_NUMBER>, B<IOBASE_MEMORY>, B<IOBASE_IO>, B<IOBASE_ISA_IO>, "
"B<IOBASE_ISA_MEM>."
msgstr ""
"Renvoie l'information de localisation de diverses régions d'entrées-sorties "
"dans la mémoire physique en fonction de la valeur de I<which>. Les valeurs "
"possibles pour I<which> sont\\ : B<IOBASE_BRIDGE_NUMBER>, B<IOBASE_MEMORY>, "
"B<IOBASE_IO>, B<IOBASE_ISA_IO>, B<IOBASE_ISA_MEM>."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<len> value is invalid.  This does not apply to B<pciconfig_iobase>()."
msgstr ""
"La valeur de I<len> n'est pas valide. Ceci ne s'applique pas à "
"B<pciconfig_iobase>()."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I/O error."
msgstr "Erreur d'entrée-sortie."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ENODEV>"
msgstr "B<ENODEV>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For B<pciconfig_iobase>(), \"hose\" value is NULL.  For the other calls, "
"could not find a slot."
msgstr ""
"Pour B<pciconfig_iobase>(), la valeur basse (« hose value ») est NULL. Pour "
"les autres appels, ne peut pas trouver une entrée."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The system has not implemented these calls (B<CONFIG_PCI> not defined)."
msgstr ""
"Le système n'a pas implémenté ces appels (B<CONFIG_PCI> n'est pas défini)."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr "B<EOPNOTSUPP>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This return value is valid only for B<pciconfig_iobase>().  It is returned "
"if the value for I<which> is invalid."
msgstr ""
"Cette valeur de retour est valide seulement pour B<pciconfig_iobase>(). Elle "
"correspond au cas où I<which> est invalide."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"User does not have the B<CAP_SYS_ADMIN> capability.  This does not apply to "
"B<pciconfig_iobase>()."
msgstr ""
"L'utilisateur n'a pas la capacité B<CAP_SYS_ADMIN>. Ceci ne s'applique pas à "
"B<pciconfig_iobase>()."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "These calls are Linux-specific, available since Linux 2.0.26/2.1.11."
msgstr ""
"Ces appels sont spécifiques à Linux, disponibles à partir des versions Linux "
"2.0.26/2.1.11."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<capabilities>(7)"
msgstr "B<capabilities>(7)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: debian-unstable
msgid ""
"This page is part of release 5.05 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.05 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.01 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."
