# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012, 2013.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011, 2012.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2020-02-10 09:30+01:00\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"process/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "GETGID"
msgstr "GETGID"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2019-03-06"
msgstr "6 mars 2019"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "getgid, getegid - get group identity"
msgstr "getgid, getegid - Obtenir l'identifiant du groupe"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr "B<#include E<lt>sys/types.hE<gt>>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<gid_t getgid(void);>"
msgstr "B<gid_t getgid(void);>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<gid_t getegid(void);>"
msgstr "B<gid_t getegid(void);>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<getgid>()  returns the real group ID of the calling process."
msgstr "B<getgid>() retourne le GID réel du processus appelant."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<getegid>()  returns the effective group ID of the calling process."
msgstr "B<getegid>() retourne le GID effectif du processus appelant."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "These functions are always successful."
msgstr "Ces fonctions réussissent toujours."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.3BSD."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The original Linux B<getgid>()  and B<getegid>()  system calls supported "
"only 16-bit group IDs.  Subsequently, Linux 2.4 added B<getgid32>()  and "
"B<getegid32>(), supporting 32-bit IDs.  The glibc B<getgid>()  and "
"B<getegid>()  wrapper functions transparently deal with the variations "
"across kernel versions."
msgstr ""
"Les appels système B<getgid>() et B<getegid>() originaux de Linux ne "
"géraient que des identifiants de groupe sur 16 bits. En conséquence, "
"Linux 2.4 a ajouté B<getgid32>() et B<getegid32>() qui prennent en charge "
"des identifiants 32 bits. Les fonctions B<getgid>() et B<getegid>() de la "
"glibc qui les encapsulent gèrent de manière transparente ces différences "
"entre noyaux."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"On Alpha, instead of a pair of B<getgid>()  and B<getegid>()  system calls, "
"a single B<getxgid>()  system call is provided, which returns a pair of real "
"and effective GIDs.  The glibc B<getgid>()  and B<getegid>()  wrapper "
"functions transparently deal with this.  See B<syscall>(2)  for details "
"regarding register mapping."
msgstr ""
"Les appels système B<getgid>() et B<getegid>() originaux de Linux ne "
"géraient que des identifiants de groupe sur 16 bits. En conséquence, "
"Linux 2.4 a ajouté B<getgid32>() et B<getegid32>() qui prennent en charge "
"des identifiants 32 bits. Les fonctions B<getgid>() et B<getegid>() de la "
"glibc qui les encapsulent gèrent de manière transparente ces différences "
"entre noyaux."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<getresgid>(2), B<setgid>(2), B<setregid>(2), B<credentials>(7)"
msgstr "B<getresgid>(2), B<setgid>(2), B<setregid>(2), B<credentials>(7)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: debian-unstable
msgid ""
"This page is part of release 5.05 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.05 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.01 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies "
"peuvent être trouvées à l'adresse \\%https://www.kernel.org/doc/man-pages/."
