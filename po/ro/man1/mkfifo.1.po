# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Laurențiu Buzdugan <lbuz@rolix.org>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2019-11-16 21:21+01:00\n"
"PO-Revision-Date: 2004-12-02 09:45+0100\n"
"Last-Translator: Laurențiu Buzdugan <lbuz@rolix.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 19.08.2\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MKFIFO"
msgstr "MKFIFO"

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2019"
msgstr "Noiembrie 2019"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.31"
msgstr "GNU coreutils 8.31"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "mkfifo - make FIFOs (named pipes)"
msgstr "mkfifo - creză FIFO-uri (pipe-uri cu nume)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<mkfifo> [I<\\,OPTION\\/>]... I<\\,NAME\\/>..."
msgstr "B<mkfifo> [I<\\,OPȚIUNE\\/>]... I<\\,DENUMIRE\\/>..."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid "Create named pipes (FIFOs) with the given NAMEs."
msgstr ""
"B<mkfifo> crează FIFO-uri (numite și  pipe-uri cu nume, sau \"named pipes\") "
"cu numele de fișiere specificate."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumentele obligatorii pentru opțiuni lungi sînt obligatorii și pentru "
"opțiunile scurte."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-m>, B<--mode>=I<\\,MODE\\/>"
msgstr "B<-m>, B<--mode>=I<\\,MOD\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "set file permission bits to MODE, not a=rw - umask"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-Z>"
msgstr "B<-Z>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "set the SELinux security context to default type"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--context>[=I<\\,CTX\\/>]"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"like B<-Z>, or if CTX is specified then set the SELinux or SMACK security "
"context to CTX"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display this help and exit"
msgstr ""
"Afișează un mesaj despre modul de folosire la ieșirea standard și termină cu "
"succes."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output version information and exit"
msgstr ""
"Afișează informații despre versiune la ieșirea standard, apoi termină cu "
"succes."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Written by David MacKenzie."
msgstr "Scris de David MacKenzie."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2019 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2019 Free Software Foundation, Inc. Licența GPLv3+: GNU GPL "
"versiunea 3 sau ulterioară E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sînteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDEȚI ȘI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "mkfifo(3)"
msgstr "mkfifo(3)"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/mkfifoE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) mkfifo invocation\\(aq"
msgstr ""

#. type: TH
#: debian-buster
#, no-wrap
msgid "February 2019"
msgstr "Februarie 2019"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr "GNU coreutils 8.30"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Report mkfifo translation bugs to E<lt>https://translationproject.org/team/"
"E<gt>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2018 Free Software Foundation, Inc. Licența GPLv3+: GNU GPL "
"versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/"
"mkfifoE<gt>"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr "August 2019"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr "Martie 2019"
