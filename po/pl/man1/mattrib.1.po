# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Wojtek Kotwica <wkotwica@post.pl>, 1999.
# Robert Luberda <robert@debian.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2019-12-08 13:31+01:00\n"
"PO-Revision-Date: 2014-11-15 08:24+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "mattrib"
msgstr "mattrib"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "10Dec18"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
#| msgid "mtools-4.0.18"
msgid "mtools-4.0.23"
msgstr "mtools-4.0.18"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Name"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "mattrib - change MSDOS file attribute flags"
msgstr "mattrib - zmienia atrybuty plików MS-DOS"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr "OSTRZEŻENIE"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""
"Ta strona podręcznika ekranowego została automatycznie wygenerowana z "
"dokumentacji texinfo pakietu mtools i może nie być kompletna i całkowicie "
"dokładna. Szczegóły można znaleźć na końcu strony."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Description"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"\\&\\&CW<Mattrib> is used to change MS-DOS file attribute flags. It has the "
"following syntax:"
msgstr ""
"\\&\\&CW<Mattrib> używany jest do zmiany flag atrybutów pliku MS-DOS. Ma "
"następującą składnię:"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"\\&\\&CW<mattrib> [\\&CW<-a|+a>] [\\&CW<-h|+h>] [\\&CW<-r|+r>] [\\&CW<-s|"
"+s>] [\\&CW<-/>] [\\&CW<-p>] [\\&CW<-X>] I<msdosfile> [ I<msdosfiles> "
"\\&... ]"
msgstr ""
"\\&\\&CW<mattrib> [\\&CW<-a|+a>] [\\&CW<-h|+h>] [\\&CW<-r|+r>] [\\&CW<-s|"
"+s>] [\\&CW<-/>] [\\&CW<-p>] [\\&CW<-X>] I<plikmsdos> [ I<plikimsdos> "
"\\&... ]"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"\\&\\&CW<Mattrib> adds attribute flags to an MS-DOS file (with the `\\&CW<"
"+>' operator) or remove attribute flags (with the `\\&CW<->' operator)."
msgstr ""
"\\&\\&CW<Mattrib> dodaje (gdy uruchomiony jest z operatorem \"\\&CW<+>\") "
"lub (z operatorem \"\\&CW<->\") flagi atrybutów pliku MS-DOS."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "\\&\\&CW<Mattrib> supports the following attribute bits:"
msgstr "\\&\\&CW<Mattrib> obsługuje następujące bity atrybutu:"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "\\&\\&CW<a>\\ "
msgstr "\\&\\&CW<a>\\ "

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Archive bit.  Used by some backup programs to indicate a new file."
msgstr ""
"Archiwacja. Używane przez niektóre programy wykonujące kopie zapasowe "
"(backup) do oznaczenia nowego pliku."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "\\&\\&CW<r>\\ "
msgstr "\\&\\&CW<r>\\ "

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Read-only bit.  Used to indicate a read-only file.  Files with this bit set "
"cannot be erased by \\&CW<DEL> nor modified."
msgstr ""
"Read-only. Używany do oznaczenia pliku tylko do odczytu. Pliki z ustawionym "
"tym bitem  nie mogą zostać skasowane przez \\&CW<DEL> ani zmodyfikowane."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "\\&\\&CW<s>\\ "
msgstr "\\&\\&CW<s>\\ "

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "System bit.  Used by MS-DOS to indicate a operating system file."
msgstr ""
"Systemowy. Używany przez MS-DOS do oznakowania pliku systemu operacyjnego."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "\\&\\&CW<h>\\ "
msgstr "\\&\\&CW<h>\\ "

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Hidden bit.  Used to make files hidden from \\&CW<DIR>."
msgstr ""
"Ukryty (hidden). Używany do ukrywania plików przed poleceniem \\&CW<DIR>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "\\&\\&CW<Mattrib> supports the following command line flags:"
msgstr "\\&\\&CW<Mattrib> przyjmuje następujące opcje linii poleceń:"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "\\&\\&CW</>\\ "
msgstr "\\&\\&CW</>\\ "

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Recursive.  Recursively list the attributes of the files in the "
"subdirectories."
msgstr "Rekurencja. Rekurencyjnie wyświetla atrybuty plików w podkatalogach."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "\\&\\&CW<X>\\ "
msgstr "\\&\\&CW<X>\\ "

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Concise. Prints the attributes without any whitespace padding.  If neither "
"the \"/\" option is given, nor the I<msdosfile> contains a wildcard, and "
"there is only one MS-DOS file parameter on the command line, only the "
"attribute is printed, and not the filename.  This option is convenient for "
"scripts"
msgstr ""
"Zwięźle. Wyświetla atrybuty bez jakiegokolwiek wyrównywania odstępami.  "
"Jeśli ani nie użyto opcji \"/\" i I<plikmsdos> nie zawiera znaków "
"uogólniających, i podano tylko jeden parametr określający plik, to "
"wyświetlany jest wyłącznie atrybut bez nazwy pliku. Opcja ta jest wygodna w "
"skryptach."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "\\&\\&CW<p>\\ "
msgstr "\\&\\&CW<p>\\ "

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Replay mode.  Outputs a series of mformat commands that will reproduce the "
"current situation, starting from a situation as left by untarring the MS-DOS "
"file system.  Commands are only output for attribute settings that differ "
"from the default (archive bit set for files, unset for directories).  This "
"option is intended to be used in addition to tar. The \\&CW<readonly> "
"attribute is not taken into account, as tar can set that one itself."
msgstr ""
"Tryb replay. Wyświetla jako wynik serię poleceń mattrib, które odtworzą "
"bieżące atrybuty, w stosunku do wyjściowej sytuacji, jaka może powstać przez "
"rozpakowanie archiwum tar systemu plików MS-DOS. Generowane są tylko "
"polecenia dla ustawień atrybutów różniących się od domyślnych (bit "
"archiwacji ustawiony dla plików, nieustawiony dla katalogów). Opcja ta jest "
"pomyślana jako dodatek do polecenia tar. Nie jest brany pod uwagę bit "
"\\&CW<readonly>, gdyż tar potrafi ustawiać go samodzielnie."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "See\\ Also"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Mtools' texinfo doc"
msgstr "Dokumentacja texinfo pakietu mtools"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr "PRZEGLĄDANIE DOKUMENTACJI TEXINFO"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""
"Ta strona podręcznika została utworzona automatycznie z dokumentacji texinfo "
"pakietu mtools. Proces ten jednak jest tylko przybliżony i niektóre "
"elementy, jak odnośniki czy indeksy, mogą być utracone. W rzeczywistości "
"elementy te nie mają właściwych odpowiedników w formacie stron podręcznika "
"ekranowego. Ponadto nie wszystkie informacje zostały przełożone na wersję "
"podręcznika ekranowego. Dlatego zdecydowanie zalecamy użycie oryginalnej "
"dokumentacji texinfo. Na końcu niniejszej strony znajdują się instrukcje, "
"jak przeglądać dokumentację w tym formacie."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Zdatną do wydrukowania postać dokumentacji texinfo można otrzymać, "
"uruchamiając poniższe polecenia:"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "To generate a html copy, run:"
msgstr "Aby utworzyć wersję html, należy uruchomić:"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""
"Już utworzone wersje html można znaleźć na stronie \\&\\&CW<\\(ifhttp://www."
"gnu.org/software/mtools/manual/mtools.html\\(is>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""
"Aby utworzyć kopię info (możliwą do przeglądania w trybie info Emacsa), "
"należy uruchomić:"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"Dokumentacja texinfo wygląda najlepiej wydrukowana lub w postaci html. W "
"wersji info niektóre przykłady są naprawdę trudne w czytaniu z powodu "
"konwencji cytowania używanych w formacie info."
