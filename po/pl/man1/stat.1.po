# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Robert Luberda <robert@debian.org>, 2002.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2019-11-16 21:22+01:00\n"
"PO-Revision-Date: 2016-05-01 18:10+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "STAT"
msgstr "STAT"

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2019"
msgstr "listopad 2019"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.31"
msgstr "GNU coreutils 8.31"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "stat - display file or file system status"
msgstr "stat - wyświetla status pliku lub systemu plików"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<stat> [I<\\,OPTION\\/>]... I<\\,FILE\\/>..."
msgstr "B<stat> [I<OPCJA>]... I<PLIK>..."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display file or file system status."
msgstr " "

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumenty, które są obowiązkowe dla długich opcji, są również obowiązkowe "
"dla krótkich."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-L>, B<--dereference>"
msgstr "B<-L>, B<--dereference>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "follow links"
msgstr "podąża za dowiązaniami"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-f>, B<--file-system>"
msgstr "B<-f>, B<--file-system>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display file system status instead of file status"
msgstr "wyświetla status systemu plików, zamiast statusu pliku"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-c>  B<--format>=I<\\,FORMAT\\/>"
msgstr "B<-c>  B<--format>=I<FORMAT>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"use the specified FORMAT instead of the default; output a newline after each "
"use of FORMAT"
msgstr ""
"używa podanego I<FORMATU> zamiast domyślnego; wypisuje znak nowego wiersza "
"po każdym użytym I<FORMACIE>"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--printf>=I<\\,FORMAT\\/>"
msgstr "B<--printf>=I<FORMAT>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"like B<--format>, but interpret backslash escapes, and do not output a "
"mandatory trailing newline; if you want a newline, include \\en in FORMAT"
msgstr ""
"jak B<--format>, ale interpretuje sekwencje specjalne z odwrotnym ukośnikiem "
"i nie wypisuje obowiązkowego znaku nowego wiersza. Aby go wypisać, należy "
"użyć B<\\en> w podanym I<FORMACIE>"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-t>, B<--terse>"
msgstr "B<-t>, B<--terse>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "print the information in terse form"
msgstr "wyświetla informacje w zwięzłej formie"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display this help and exit"
msgstr "wyświetla ten tekst i kończy pracę"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output version information and exit"
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The valid format sequences for files (without B<--file-system>):"
msgstr ""
"Prawidłowe sekwencje I<FORMATU> dla plików (jeżeli nie podano opcji B<--file-"
"system>):"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%a"
msgstr "B<%a>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "access rights in octal (note '#' and '0' printf flags)"
msgstr ""
"prawa dostępu ósemkowo (proszę zwrócić uwagę na flagi \"#\" i \"0\" printf)"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%A"
msgstr "B<%A>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "access rights in human readable form"
msgstr "prawa dostępu, w formie czytelnej dla człowieka"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%b"
msgstr "B<%b>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "number of blocks allocated (see %B)"
msgstr "liczba zajętych bloków (patrz B<%B>)"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%B"
msgstr "B<%B>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "the size in bytes of each block reported by %b"
msgstr "rozmiar każdego bloku podanego przez B<%b>, w bajtach"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%C"
msgstr "B<%C>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "SELinux security context string"
msgstr "łańcuch kontekstu bezpieczeństwa SELinux"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%d"
msgstr "B<%d>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "device number in decimal"
msgstr "numer urządzenia, w systemie dziesiętnym"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%D"
msgstr "B<%D>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "device number in hex"
msgstr "numer urządzenia, szesnastkowo"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%f"
msgstr "B<%f>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "raw mode in hex"
msgstr "tryb surowy, szesnastkowo"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%F"
msgstr "B<%F>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "file type"
msgstr "typ pliku"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%g"
msgstr "B<%g>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "group ID of owner"
msgstr "identyfikator grupy właściciela pliku"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%G"
msgstr "B<%G>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "group name of owner"
msgstr "nazwa grupy właściciela pliku"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%h"
msgstr "B<%h>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "number of hard links"
msgstr "liczba dowiązań zwykłych (twardych)"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%i"
msgstr "B<%i>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "inode number"
msgstr "numer i-węzła"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%m"
msgstr "B<%m>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "mount point"
msgstr "punkt montowania"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%n"
msgstr "B<%n>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "file name"
msgstr "nazwa pliku"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%N"
msgstr "B<%N>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "quoted file name with dereference if symbolic link"
msgstr ""
"nazwa pliku w cudzysłowach, rozwiązana, jeśli jest dowiązaniem symbolicznym"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%o"
msgstr "B<%o>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "optimal I/O transfer size hint"
msgstr "wskazówka nt. optymalnego transferu wejścia/wyjścia"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%s"
msgstr "B<%s>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "total size, in bytes"
msgstr "całkowity rozmiar, w bajtach"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%t"
msgstr "B<%t>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "major device type in hex, for character/block device special files"
msgstr ""
"główny (major) typ urządzenia szesnastkowo, do specjalnych plików urządzeń "
"znakowych/blokowych"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%T"
msgstr "B<%T>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "minor device type in hex, for character/block device special files"
msgstr ""
"poboczny (minor) typ urządzenia szesnastkowo, do specjalnych plików urządzeń "
"znakowych/blokowych"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%u"
msgstr "B<%u>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "user ID of owner"
msgstr "identyfikator właściciela pliku"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%U"
msgstr "B<%U>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "user name of owner"
msgstr "nazwa właściciela pliku"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%w"
msgstr "B<%w>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of file birth, human-readable; - if unknown"
msgstr ""
"czas utworzenia pliku, w formie czytelnej dla człowieka; B<-> jeśli jest "
"nieznany"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%W"
msgstr "B<%W>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of file birth, seconds since Epoch; 0 if unknown"
msgstr "czas utworzenia pliku, w sekundach od epoki; B<0> jeśli jest nieznany"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%x"
msgstr "B<%x>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last access, human-readable"
msgstr "czas ostatniego dostępu, w formie czytelnej dla człowieka"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%X"
msgstr "B<%X>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last access, seconds since Epoch"
msgstr "czas ostatniego dostępu, w sekundach od epoki"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%y"
msgstr "B<%y>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last data modification, human-readable"
msgstr "czas ostatniej modyfikacji danych, w formie czytelnej dla człowieka"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%Y"
msgstr "B<%Y>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last data modification, seconds since Epoch"
msgstr "czas ostatniej modyfikacji danych, w sekundach od epoki"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%z"
msgstr "B<%z>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last status change, human-readable"
msgstr "czas ostatniej zmiany statusu, w formie czytelnej dla człowieka"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%Z"
msgstr "B<%Z>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "time of last status change, seconds since Epoch"
msgstr "czas ostatniej zmiany statusu, w sekundach od epoki"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Valid format sequences for file systems:"
msgstr "Prawidłowe określenia formatu dla systemów plików:"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "free blocks available to non-superuser"
msgstr "liczba wolnych bloków dostępnych dla zwykłych użytkowników"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "total data blocks in file system"
msgstr "całkowita liczba bloków danych w systemie plików"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%c"
msgstr "B<%c>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "total file nodes in file system"
msgstr "całkowita liczba węzłów w systemie plików"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "free file nodes in file system"
msgstr "liczba wolnych węzłów plików w systemie plików"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "free blocks in file system"
msgstr "liczba wolnych bloków w systemie plików"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "file system ID in hex"
msgstr "identyfikator systemu plików, szesnastkowo"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%l"
msgstr "B<%l>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "maximum length of filenames"
msgstr "maksymalna długość nazw plików"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "block size (for faster transfers)"
msgstr "rozmiar bloku (w celu szybszych transferów)"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "%S"
msgstr "B<%S>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "fundamental block size (for block counts)"
msgstr "podstawowy rozmiar bloku (do zliczenia bloków)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "file system type in hex"
msgstr "identyfikator systemu plików, szesnastkowo"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "file system type in human readable form"
msgstr "typ, w formie czytelnej dla człowieka"

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "--terse is equivalent to the following FORMAT:"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o"
msgstr ""

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "--terse --file-system is equivalent to the following FORMAT:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "%n %i %l %t %s %S %b %f %a %c %d"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"NOTE: your shell may have its own version of stat, which usually supersedes "
"the version described here.  Please refer to your shell's documentation for "
"details about the options it supports."
msgstr ""
"B<UWAGA>: aktualnie używana powłoka może posiadać swoją wersję B<stat>, "
"która z reguły przesłania wersję opisaną w niniejszym podręczniku. Proszę "
"zapoznać się z dokumentacją używanej powłoki, aby dowiedzieć się szczegółów "
"o obsługiwanych opcjach. Aby wywołać opisaną tu wersję, można użyć polecenia "
"B<env> tzn. B<env stat> I<...>."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Written by Michael Meskes."
msgstr "Napisane przez Michaela Meskesa."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ZGŁASZANIE BŁĘDÓW"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
#| msgid ""
#| "GNU coreutils online help: E<lt>http://www.gnu.org/software/coreutils/"
#| "E<gt>"
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Strona internetowa z pomocą GNU coreutils: E<lt>http://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux mageia-cauldron
#, fuzzy
#| msgid ""
#| "Report stat translation bugs to E<lt>http://translationproject.org/team/"
#| "E<gt>"
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Zgłoszenia błędów w tłumaczeniu stat proszę wysyłać na adres E<lt>http://"
"translationproject.org/team/pl.htmlE<gt>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "PRAWA AUTORSKIE"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2019 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2019 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Jest to wolne oprogramowanie: można je zmieniać i rozpowszechniać. Nie ma "
"ŻADNEJ GWARANCJI, w granicach określonych przez prawo."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "stat(2), statfs(2)"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
#, fuzzy
#| msgid ""
#| "Full documentation at: E<lt>http://www.gnu.org/software/coreutils/"
#| "statE<gt>"
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/statE<gt>"
msgstr ""
"Pełna dokumentacja na stronie: E<lt>http://www.gnu.org/software/coreutils/"
"statE<gt>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) stat invocation\\(aq"
msgstr "lub lokalnie, za pomocą B<info \\(aq(coreutils) stat invocation\\(aq>"

#. type: TH
#: debian-buster
#, fuzzy, no-wrap
#| msgid "January 2016"
msgid "February 2019"
msgstr "styczeń 2016"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr "GNU coreutils 8.30"

#. type: Plain text
#: debian-buster debian-unstable
msgid "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o %C"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
#| msgid ""
#| "Report stat translation bugs to E<lt>http://translationproject.org/team/"
#| "E<gt>"
msgid ""
"Report stat translation bugs to E<lt>https://translationproject.org/team/"
"E<gt>"
msgstr ""
"Zgłoszenia błędów w tłumaczeniu stat proszę wysyłać na adres E<lt>http://"
"translationproject.org/team/pl.htmlE<gt>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2018 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
#| msgid ""
#| "Full documentation at: E<lt>http://www.gnu.org/software/coreutils/"
#| "statE<gt>"
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/statE<gt>"
msgstr ""
"Pełna dokumentacja na stronie: E<lt>http://www.gnu.org/software/coreutils/"
"statE<gt>"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr "marzec 2019"
