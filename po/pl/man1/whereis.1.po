# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Wojtek Kotwica <wkotwica@post.pl>, 1999.
# Robert Luberda <robert@debian.org>, 2013.
# Michał Kułach <michal.kulach@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2019-08-03 17:58+02:00\n"
"PO-Revision-Date: 2016-08-21 16:15+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "WHEREIS"
msgstr "WHEREIS"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "October 2014"
msgstr "październik 2014"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"whereis - locate the binary, source, and manual page files for a command"
msgstr ""
"whereis - znajdź pliki binarne, źródłowe i strony podręcznika dla danego "
"polecenia"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<whereis> [options] [B<-BMS> I<directory>... B<-f>] I<name>..."
msgstr "B<whereis> [I<opcje>] [B<-BMS> I<katalog>...  B<-f>] I<nazwa_pliku>..."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<whereis> locates the binary, source and manual files for the specified "
"command names.  The supplied names are first stripped of leading pathname "
"components and any (single) trailing extension of the form B<.>I<ext> (for "
"example: B<.c>)  Prefixes of B<s.> resulting from use of source code control "
"are also dealt with.  B<whereis> then attempts to locate the desired program "
"in the standard Linux places, and in the places specified by B<$PATH> and B<"
"$MANPATH>."
msgstr ""
"B<whereis> lokalizuje pliki binarne, źródłowe i podręczniki systemowe dla "
"zadanych nazw poleceń. Z podanych nazw wstępnie usuwana jest początkowa "
"składowa określająca ścieżkę oraz ewentualne (pojedyncze) kończące "
"rozszerzenie postaci B<.>I<ext>, np. B<.c>. Przedrostki B<s.> wynikające z "
"użycia kontroli kodu źródłowego są również obsługiwane. Następnie B<whereis> "
"usiłuje zlokalizować położenie żądanego programu, posługując się listą "
"standardowych miejsc linuksowych oraz w miejscach określonych zmiennymi B<"
"$PATH> i B<$MANPATH>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The search restrictions (options B<-b>, B<-m> and B<-s>)  are cumulative and "
"apply to the subsequent I<name> patterns on the command line.  Any new "
"search restriction resets the search mask.  For example,"
msgstr ""
"Ograniczenia wyszukiwania (opcje B<-b>, B<-m> i B<-s>) łączą się i "
"są stosowane do kolejnych wzorów I<nazw> w wierszu poleceń. Każde nowe "
"ograniczenie wyszukiwania resetuje maskę szukania, np."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<whereis -bm ls tr -m gcc>"
msgstr "B<whereis -bm ls tr -m gcc>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"searches for \"ls\" and \"tr\" binaries and man pages, and for \"gcc\" man "
"pages only."
msgstr ""
"wyszukuje stron podręcznika i plików binarnych do \"ls\" i \"tr\", a do \"gcc"
"\" jedynie stron podręcznika systemowego."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The options B<-B>, B<-M> and B<-S> reset search paths for the subsequent "
"I<name> patterns.  For example,"
msgstr ""
"Opcje B<-B>, B<-M> i B<-S> resetują ścieżki szukania dla kolejnych wzorców "
"I<nazw>, np."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<whereis -m ls -M /usr/share/man/man1 -f cal>"
msgstr "B<whereis -m ls -M /usr/share/man/man1 -f cal>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"searches for \"ls\" man pages in all default paths, but for \"cal\" in the /"
"usr/share/man/man1 directory only."
msgstr ""
"szuka podręczników systemowych do \"ls\" we wszystkich domyślnych ścieżkach, "
"a do \"cal\" jedynie w katalogu /usr/share/man/man1."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Search for binaries."
msgstr "Szuka plików binarnych."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Search for manuals."
msgstr "Szuka podręczników systemowych."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Search for sources."
msgstr "Szuka źródeł."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-u>"
msgstr "B<-u>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Only show the command names that have unusual entries.  A command is said to "
"be unusual if it does not have just one entry of each explicitly requested "
"type.  Thus 'B<whereis -m -u *>' asks for those files in the current "
"directory which have no documentation file, or more than one."
msgstr ""
"Pokazuje jedynie nazwy poleceń z nietypowymi wpisami, tzn. te, które nie "
"mają jednej pozycji dla każdego zadanego typu.  Zatem \"B<whereis -m -u *>\" "
"wyszukuje pliki w bieżącym katalogu, które nie mają dokumentacji lub mają "
"więcej niż jedną."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-B >I<list>"
msgstr "B<-B >I<lista>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Limit the places where B<whereis> searches for binaries, by a whitespace-"
"separated list of directories."
msgstr ""
"Ogranicza miejsca, gdzie B<whereis> szuka plików binarnych do oddzielonej "
"spacjami listy katalogów."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-M >I<list>"
msgstr "B<-M >I<lista>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Limit the places where B<whereis> searches for manuals and documentation in "
"Info format, by a whitespace-separated list of directories."
msgstr ""
"Ogranicza miejsca, gdzie B<whereis> szuka podręczników systemowych i "
"dokumentacji w formacie Info do oddzielonej spacjami listy katalogów."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-S >I<list>"
msgstr "B<-S >I<lista>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Limit the places where B<whereis> searches for sources, by a whitespace-"
"separated list of directories."
msgstr ""
"Ogranicza miejsca, gdzie B<whereis> szuka źródeł do oddzielonej spacjami "
"listy katalogów."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Terminates the directory list and signals the start of filenames.  It "
"I<must> be used when any of the B<-B>, B<-M>, or B<-S> options is used."
msgstr ""
"Kończy listę katalogów i sygnalizuje początek nazw plików. I<Musi> być "
"użyte, jeśli zastosowano jedną z opcji B<-B>, B<-M> lub B<-S>."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Output the list of effective lookup paths that B<whereis> is using.  When "
"none of B<-B>, B<-M>, or B<-S> is specified, the option will output the hard-"
"coded paths that the command was able to find on the system."
msgstr ""
"Wypisuje listę efektywnie przeszukiwanych ścieżek, której używa B<whereis>. "
"Gdy nie poda się żadnej z opcji B<-B>, B<-M> ani B<-S>, wypisane zostaną "
"ustalone na sztywno ścieżki, które polecenie było w stanie znaleźć w tym "
"systemie."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<--help>"
msgid "B<-h>, B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
#| msgid "display this help and exit"
msgid "Display help text and exit."
msgstr "wyświetla ten tekst i kończy pracę"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<--version>"
msgid "B<-V>, B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
#| msgid "output version information and exit"
msgid "Display version information and exit."
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EXAMPLE"
msgstr "PRZYKŁAD"

# zmiana /usr/man -> /usr/share/man
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
#| msgid ""
#| "To find all files in B</usr/\\:bin> which are not documented in B</usr/\\:"
#| "man/\\:man1> or have no source in B</usr/\\:src>:"
msgid ""
"To find all files in I</usr/\\:bin> which are not documented in I</usr/\\:"
"man/\\:man1> or have no source in I</usr/\\:src>:"
msgstr ""
"Aby znaleźć wszystkie pliki w I</usr/bin>, które nie mają dokumentacji w I</"
"usr/share/man/man1> ani źródła w I</usr/src>:"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<cd /usr/bin>"
msgstr "B<cd /usr/bin>"

# zmiana /usr/man -> /usr/share/man
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<whereis -u -ms -M /usr/man/man1 -S /usr/src -f *>"
msgstr "B<whereis -u -ms -M /usr/share/man/man1 -S /usr/src -f *>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "FILE SEARCH PATHS"
msgstr "ŚCIEŻKI PRZESZUKIWANIA PLIKÓW"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"By default B<whereis> tries to find files from hard-coded paths, which are "
"defined with glob patterns.  The command attempts to use the contents of B<"
"$PATH> and B<$MANPATH> environment variables as default search path.  The "
"easiest way to know what paths are in use is to add the B<-l> listing "
"option.  Effects of the B<-B>, B<-M>, and B<-S> are displayed with B<-l>."
msgstr ""
"Domyślnie B<whereis> próbuje znajdować pliki w zakodowanych na sztywno "
"ścieżkach, które są zdefiniowane za pomocą masek. Polecenie próbuje "
"skorzystać z zawartości zmiennych środowiskowych B<$PATH> i B<$MANPATH> jako "
"domyślnych ścieżek wyszukiwania. Najprostszą metodą poznania tych ścieżek "
"jest skorzystanie z opcji B<-l>. Działanie opcji B<-B>, B<-M> i B<-S> jest "
"odzwierciedlone przez B<-l>."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ŚRODOWISKO"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "WHEREIS_DEBUG=all"
msgstr "WHEREIS_DEBUG=all"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "enables debug output."
msgstr "włącza wyjście debugowania."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "DOSTĘPNOŚĆ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
#| msgid ""
#| "The whereis command is part of the util-linux package and is available "
#| "from E<.UR ftp://\\:ftp.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/"
#| "> Linux Kernel Archive E<.UE .>"
msgid ""
"The whereis command is part of the util-linux package and is available from "
"E<.UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> "
"Linux Kernel Archive E<.UE .>"
msgstr ""
"Polecenie whereis jest częścią pakietu util-linux i jest dostępne z E<.UR "
"ftp://\\:ftp.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Archiwum "
"jądra Linux E<.UE .>"
