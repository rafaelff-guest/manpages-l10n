# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2019-11-16 21:21+01:00\n"
"PO-Revision-Date: 2016-04-25 19:51+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MKFIFO"
msgstr "MKFIFO"

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2019"
msgstr "listopad 2019"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.31"
msgstr "GNU coreutils 8.31"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "mkfifo - make FIFOs (named pipes)"
msgstr "mkfifo - tworzy FIFO (potoki nazwane)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<mkfifo> [I<\\,OPTION\\/>]... I<\\,NAME\\/>..."
msgstr "B<mkfifo> [I<OPCJA>]... I<NAZWA>..."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Create named pipes (FIFOs) with the given NAMEs."
msgstr "Tworzy potoki nazwane (FIFO) o podanych I<NAZWACH>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumenty, które są obowiązkowe dla długich opcji, są również obowiązkowe "
"dla krótkich."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-m>, B<--mode>=I<\\,MODE\\/>"
msgstr "B<-m>, B<--mode>=I<TRYB>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "set file permission bits to MODE, not a=rw - umask"
msgstr ""
"ustawia I<TRYB> uprawnień, zamiast domyślnego B<a=rw> minus bity ustawione w "
"B<umask>"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-Z>"
msgstr "B<-Z>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "set the SELinux security context to default type"
msgstr "ustawia kontekst bezpieczeństwa SELinux na domyślny"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--context>[=I<\\,CTX\\/>]"
msgstr "B<--context>[=I<KONTEKST>]"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"like B<-Z>, or if CTX is specified then set the SELinux or SMACK security "
"context to CTX"
msgstr ""
"jak B<-Z> lub jeśli poda się I<KONTEKST> - ustawia kontekst bezpieczeństwa "
"SELinux lub SMACK na I<KONTEKST>"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display this help and exit"
msgstr "wyświetla ten tekst i kończy pracę"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output version information and exit"
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Written by David MacKenzie."
msgstr "Napisane przez Davida MacKenzie."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ZGŁASZANIE BŁĘDÓW"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
#| msgid ""
#| "GNU coreutils online help: E<lt>http://www.gnu.org/software/coreutils/"
#| "E<gt>"
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Strona internetowa z pomocą GNU coreutils: E<lt>http://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux mageia-cauldron
#, fuzzy
#| msgid ""
#| "Report mkfifo translation bugs to E<lt>http://translationproject.org/team/"
#| "E<gt>"
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Zgłoszenia błędów w tłumaczeniu mkfifo proszę wysyłać na adres E<lt>http://"
"translationproject.org/team/pl.htmlE<gt>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "PRAWA AUTORSKIE"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2019 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2019 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Jest to wolne oprogramowanie: można je zmieniać i rozpowszechniać. Nie ma "
"ŻADNEJ GWARANCJI, w granicach określonych przez prawo."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "mkfifo(3)"
msgstr "B<mkfifo>(3)"

#. type: Plain text
#: archlinux mageia-cauldron
#, fuzzy
#| msgid ""
#| "Full documentation at: E<lt>http://www.gnu.org/software/coreutils/"
#| "mkfifoE<gt>"
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/mkfifoE<gt>"
msgstr ""
"Pełna dokumentacja na stronie: E<lt>http://www.gnu.org/software/coreutils/"
"mkfifoE<gt>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) mkfifo invocation\\(aq"
msgstr ""
"lub lokalnie, za pomocą B<info \\(aq(coreutils) mkfifo invocation\\(aq>"

#. type: TH
#: debian-buster
#, fuzzy, no-wrap
#| msgid "January 2016"
msgid "February 2019"
msgstr "styczeń 2016"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr "GNU coreutils 8.30"

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
#| msgid ""
#| "Report mkfifo translation bugs to E<lt>http://translationproject.org/team/"
#| "E<gt>"
msgid ""
"Report mkfifo translation bugs to E<lt>https://translationproject.org/team/"
"E<gt>"
msgstr ""
"Zgłoszenia błędów w tłumaczeniu mkfifo proszę wysyłać na adres E<lt>http://"
"translationproject.org/team/pl.htmlE<gt>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2018 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
#| msgid ""
#| "Full documentation at: E<lt>http://www.gnu.org/software/coreutils/"
#| "mkfifoE<gt>"
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/"
"mkfifoE<gt>"
msgstr ""
"Pełna dokumentacja na stronie: E<lt>http://www.gnu.org/software/coreutils/"
"mkfifoE<gt>"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr "marzec 2019"
