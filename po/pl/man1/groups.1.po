# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Wojtek Kotwica <wkotwica@post.pl>, 2000.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2019-08-11 13:48+02:00\n"
"PO-Revision-Date: 2016-04-28 19:08+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "GROUPS"
msgstr "GROUPS"

#. type: TH
#: debian-buster
#, fuzzy, no-wrap
#| msgid "January 2016"
msgid "February 2019"
msgstr "styczeń 2016"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr "GNU coreutils 8.30"

#. type: TH
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: debian-buster debian-unstable mageia-cauldron
msgid "groups - print the groups a user is in"
msgstr "groups - wyświetla grupy, w których jest użytkownik"

#. type: SH
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: debian-buster debian-unstable mageia-cauldron
msgid "B<groups> [I<\\,OPTION\\/>]... [I<\\,USERNAME\\/>]..."
msgstr "B<groups> [I<OPCJA>]... [I<NAZWA-UŻYTKOWNIKA>]..."

#. type: SH
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: debian-buster debian-unstable mageia-cauldron
msgid ""
"Print group memberships for each USERNAME or, if no USERNAME is specified, "
"for the current process (which may differ if the groups database has "
"changed)."
msgstr ""
"Wyświetla grupy, do których przynależy każda I<NAZWA-UŻYTKOWNIKA> lub gdy "
"jej nie podano - bieżący proces (po zmianach bazy danych grup mogą wystąpić "
"różnice)."

#. type: TP
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: debian-buster debian-unstable mageia-cauldron
msgid "display this help and exit"
msgstr "wyświetla ten tekst i kończy pracę"

#. type: TP
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: debian-buster debian-unstable mageia-cauldron
msgid "output version information and exit"
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: SH
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-buster debian-unstable mageia-cauldron
msgid "Written by David MacKenzie and James Youngman."
msgstr "Napisane przez Davida MacKenzie i Jamesa Youngmana."

#. type: SH
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ZGŁASZANIE BŁĘDÓW"

#. type: Plain text
#: debian-buster debian-unstable mageia-cauldron
#, fuzzy
#| msgid ""
#| "GNU coreutils online help: E<lt>http://www.gnu.org/software/coreutils/"
#| "E<gt>"
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Strona internetowa z pomocą GNU coreutils: E<lt>http://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
#| msgid ""
#| "Report groups translation bugs to E<lt>http://translationproject.org/team/"
#| "E<gt>"
msgid ""
"Report groups translation bugs to E<lt>https://translationproject.org/team/"
"E<gt>"
msgstr ""
"Zgłoszenia błędów w tłumaczeniu groups proszę wysyłać na adres E<lt>http://"
"translationproject.org/team/pl.htmlE<gt>"

#. type: SH
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "PRAWA AUTORSKIE"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2018 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-buster debian-unstable mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Jest to wolne oprogramowanie: można je zmieniać i rozpowszechniać. Nie ma "
"ŻADNEJ GWARANCJI, w granicach określonych przez prawo."

#. type: SH
#: debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: debian-buster debian-unstable mageia-cauldron
msgid "getent(1)"
msgstr "B<getent>(1)"

#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
#| msgid ""
#| "Full documentation at: E<lt>http://www.gnu.org/software/coreutils/"
#| "groupsE<gt>"
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/"
"groupsE<gt>"
msgstr ""
"Pełna dokumentacja na stronie: E<lt>http://www.gnu.org/software/coreutils/"
"groupsE<gt>"

#. type: Plain text
#: debian-buster debian-unstable mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) groups invocation\\(aq"
msgstr ""
"lub lokalnie, za pomocą B<info \\(aq(coreutils) groups invocation\\(aq>"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr "marzec 2019"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.31"
msgstr "GNU coreutils 8.31"

#. type: Plain text
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "Report groups translation bugs to E<lt>http://translationproject.org/team/"
#| "E<gt>"
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Zgłoszenia błędów w tłumaczeniu groups proszę wysyłać na adres E<lt>http://"
"translationproject.org/team/pl.htmlE<gt>"

#. type: Plain text
#: mageia-cauldron
msgid ""
"Copyright \\(co 2019 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2019 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "Full documentation at: E<lt>http://www.gnu.org/software/coreutils/"
#| "groupsE<gt>"
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/groupsE<gt>"
msgstr ""
"Pełna dokumentacja na stronie: E<lt>http://www.gnu.org/software/coreutils/"
"groupsE<gt>"
