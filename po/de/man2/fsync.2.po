# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Chris Leick <c.leick@vollbio.de>, 2010-2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
# Helge Kreutzmann <debian@helgefjell.de>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2020-02-10 09:30+01:00\n"
"PO-Revision-Date: 2019-08-04 09:34+0200\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "FSYNC"
msgstr "FSYNC"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2019-03-06"
msgstr "6. März 2019"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"fsync, fdatasync - synchronize a file's in-core state with storage device"
msgstr ""
"fsync, fdatasync - den gepufferten Dateiinhalt mit einem Datenträger "
"synchronisieren"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<int fsync(int >I<fd>B<);>"
msgstr "B<int fsync(int >I<fd>B<);>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<int fdatasync(int >I<fd>B<);>"
msgstr "B<int fdatasync(int >I<fd>B<);>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr "Mit Glibc erforderliche Makros (siehe B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<fsync>():\n"
"    Glibc 2.16 and later:\n"
"        No feature test macros need be defined\n"
"    Glibc up to and including 2.15:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE\n"
"            || /* since glibc 2.8: */ _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
msgstr ""
"B<fsync>():\n"
"    Glibc 2.16 und neuer:\n"
"        Es müssen keine Feature-Test-Makros definiert werden\n"
"    Glibc bis zu einschließlich 2.15:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE\n"
"            || /* seit Glibc 2.8: */ _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<fdatasync>():\n"
"    _POSIX_C_SOURCE\\ E<gt>=\\ 199309L || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
msgstr ""
"B<fdatasync>():\n"
"    _POSIX_C_SOURCE\\ E<gt>=\\ 199309L || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<fsync>()  transfers (\"flushes\") all modified in-core data of (i.e., "
"modified buffer cache pages for) the file referred to by the file descriptor "
"I<fd> to the disk device (or other permanent storage device) so that all "
"changed information can be retrieved even if the system crashes or is "
"rebooted.  This includes writing through or flushing a disk cache if "
"present.  The call blocks until the device reports that the transfer has "
"completed."
msgstr ""
"B<fsync>() überträgt (»leert«) alle veränderten gepufferten Daten (d.h. "
"geänderte Seiten des Pufferzwischenspeichers) von der Datei, auf die der "
"Dateideskriptor I<fd> weist, auf die Platte (oder ein anderes dauerhaftes "
"Speichermedium), so dass sämtliche Änderungsinformationen sogar nach einem "
"Absturz oder Neustart des Systems wieder hergestellt werden können. Dies "
"beinhaltet das Schreiben oder Leeren eines Plattenzwischenspeichers, falls "
"vorhanden. Der Aufruf blockiert bis das Gerät meldet, dass die Übertragung "
"vollständig ist."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"As well as flushing the file data, B<fsync>()  also flushes the metadata "
"information associated with the file (see B<inode>(7))."
msgstr ""
"Neben dem Leeren der Dateidaten leert B<fsync>() auch alle Metadaten-"
"Informationen, die mit der Datei verknüpft sind (siehe B<inode>(7))."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Calling B<fsync>()  does not necessarily ensure that the entry in the "
"directory containing the file has also reached disk.  For that an explicit "
"B<fsync>()  on a file descriptor for the directory is also needed."
msgstr ""
"Der Aufruf von B<fsync>() garantiert nicht, dass der Verzeichniseintrag der "
"Datei die Platte erreicht. Dafür wird auch ein explizites B<fsync>() auf "
"einem Dateideskriptor des Verzeichnisses benötigt."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<fdatasync>()  is similar to B<fsync>(), but does not flush modified "
"metadata unless that metadata is needed in order to allow a subsequent data "
"retrieval to be correctly handled.  For example, changes to I<st_atime> or "
"I<st_mtime> (respectively, time of last access and time of last "
"modification; see B<inode>(7))  do not require flushing because they are not "
"necessary for a subsequent data read to be handled correctly.  On the other "
"hand, a change to the file size (I<st_size>, as made by say "
"B<ftruncate>(2)), would require a metadata flush."
msgstr ""
"B<fdatasync>() ist B<fsync>() ähnlich, leert aber keine geänderten "
"Metadaten, sofern diese Metadaten nicht benötigt werden, um die korrekte "
"Handhabung einer nachfolgenden Datenabfrage zu ermöglichen. Änderungen an "
"I<st_atime> oder I<st_mtime> (Zeit des letzten Zugriffs und Zeit der letzten "
"Änderung; siehe B<inode>(7)) erfordern zum Beispiel keine Leerung, da sie "
"nicht für die korrekte Handhabung eines nachfolgenden Lesens der Daten "
"benötigt werden. Andererseits würde die Änderung der Dateigröße (I<st_size>, "
"wie sie von B<ftruncate>(2) vorgenommen wird) ein Leeren der Metadaten "
"erfordern."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The aim of B<fdatasync>()  is to reduce disk activity for applications that "
"do not require all metadata to be synchronized with the disk."
msgstr ""
"Das Ziel von B<fdatasync>() ist die Verminderung der Plattenaktivität durch "
"Anwendungen, die nicht das Synchronisieren aller Metadaten mit der Platte "
"erfordern."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"On success, these system calls return zero.  On error, -1 is returned, and "
"I<errno> is set appropriately."
msgstr ""
"Bei Erfolg gibt dieser Systemaufruf Null zurück. Bei einem Fehler wird -1 "
"zurückgegeben und I<errno> entsprechend gesetzt."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<fd> is not a valid open file descriptor."
msgstr "I<fd> ist kein zulässiger offener Dateideskriptor."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#.  commit 088737f44bbf6378745f5b57b035e57ee3dc4750
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"An error occurred during synchronization.  This error may relate to data "
"written to some other file descriptor on the same file.  Since Linux 4.13, "
"errors from write-back will be reported to all file descriptors that might "
"have written the data which triggered the error.  Some filesystems (e.g., "
"NFS) keep close track of which data came through which file descriptor, and "
"give more precise reporting.  Other filesystems (e.g., most local "
"filesystems) will report errors to all file descriptors that were open on "
"the file when the error was recorded."
msgstr ""
"Während der Synchronisation trat ein Fehler auf. Der Fehler kann sich auf "
"Daten beziehen, die in einen anderen Dateideskriptor auf der gleichen Datei "
"geschrieben wurden. Seit Linux 4.13 werden Fehler vom Zurückschreiben an "
"alle Dateideskriptoren, die Daten geschrieben haben könnten, die den Fehler "
"auslösten, berichtet. Einige Dateisystem (z.B. NFS) halten genau nach, "
"welche Daten durch welchen Dateideskriptor gekommen sind und berichten "
"genauer. Andere Dateisysteme (z.B. die meisten lokalen Dateisysteme) "
"berichten Fehler an alle Dateideskriptoren, die zum Zeitpunkt der "
"Aufzeichnung des Fehlers offen waren."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Disk space was exhausted while synchronizing."
msgstr "Beim Synchronisieren wurde der Plattenplatz erschöpft."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EROFS>, B<EINVAL>"
msgstr "B<EROFS>, B<EINVAL>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"I<fd> is bound to a special file (e.g., a pipe, FIFO, or socket)  which does "
"not support synchronization."
msgstr ""
"I<fd> ist mit einer Spezialdatei (z.B. einer PIPE, einem FIFO oder einem "
"Socket) verbunden, die keine Synchronisation unterstützt."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ENOSPC>, B<EDQUOT>"
msgstr "B<ENOSPC>, B<EDQUOT>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"I<fd> is bound to a file on NFS or another filesystem which does not "
"allocate space at the time of a B<write>(2)  system call, and some previous "
"write failed due to insufficient storage space."
msgstr ""
"I<fd> ist an eine Datei auf einem NFS- oder anderem Dateisystem, das keinen "
"Platz zum Zeitpunkt des Systemaufrufs B<write>(2) zuweist, gebunden und ein "
"vorhergehender Schreibzugriff schlug aufgrund mangelndem Plattenplatz fehl."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.3BSD."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#.  POSIX.1-2001: It shall be defined to -1 or 0 or 200112L.
#.  -1: unavailable, 0: ask using sysconf().
#.  glibc defines them to 1.
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"On POSIX systems on which B<fdatasync>()  is available, "
"B<_POSIX_SYNCHRONIZED_IO> is defined in I<E<lt>unistd.hE<gt>> to a value "
"greater than 0.  (See also B<sysconf>(3).)"
msgstr ""
"Auf POSIX-Systemen, auf denen B<fdatasync>() verfügbar ist, ist "
"B<_POSIX_SYNCHRONIZED_IO> in I<E<lt>unistd.hE<gt>> als ein Wert größer als 0 "
"definiert. (Siehe auch B<sysconf>(3).)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"On some UNIX systems (but not Linux), I<fd> must be a I<writable> file "
"descriptor."
msgstr ""
"Auf einigen UNIX-Systemen (aber nicht Linux) muss I<fd> ein I<schreibbarer> "
"Dateideskriptor sein."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"In Linux 2.2 and earlier, B<fdatasync>()  is equivalent to B<fsync>(), and "
"so has no performance advantage."
msgstr ""
"Bis einschließlich Linux 2.2 entspricht B<fdatasync>() B<fsync>() und "
"steigert daher nicht die Leistung."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<fsync>()  implementations in older kernels and lesser used filesystems "
"do not know how to flush disk caches.  In these cases disk caches need to be "
"disabled using B<hdparm>(8)  or B<sdparm>(8)  to guarantee safe operation."
msgstr ""
"Die B<fsync>()-Implementierungen in älteren Kerneln oder weniger benutzten "
"Dateisystemen wissen nicht, wie Plattenzwischenspeicher geleert werden. In "
"diesen Fällen müssen Plattenzwischenspeicher mittels B<hdparm>(8) oder "
"B<sdparm>(8) deaktiviert werden, um sicheres Funktionieren zu garantieren."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<sync>(1), B<bdflush>(2), B<open>(2), B<posix_fadvise>(2), B<pwritev>(2), "
"B<sync>(2), B<sync_file_range>(2), B<fflush>(3), B<fileno>(3), B<hdparm>(8), "
"B<mount>(8)"
msgstr ""
"B<sync>(1), B<bdflush>(2), B<open>(2), B<posix_fadvise>(2), B<pwritev>(2), "
"B<sync>(2), B<sync_file_range>(2), B<fflush>(3), B<fileno>(3), B<hdparm>(8), "
"B<mount>(8)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: archlinux debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.04 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: debian-unstable
msgid ""
"This page is part of release 5.05 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.05 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.01 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
