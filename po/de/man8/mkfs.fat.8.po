# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.16\n"
"POT-Creation-Date: 2019-10-03 12:24+02:00\n"
"PO-Revision-Date: 2019-12-03 19:37+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 19.08.2\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MKFS.FAT"
msgstr "MKFS.FAT"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2016-01-25"
msgstr "25. Januar 2016"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "dosfstools 4.1"
msgstr "Dosfstools 4.1"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#.  ----------------------------------------------------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<mkfs.fat> - create an MS-DOS filesystem under Linux"
msgstr "B<mkfs.fat> - ein MS-DOS-Dateisystem unter Linux anlegen"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#.  ----------------------------------------------------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<mkfs.fat> [I<OPTIONS>] I<DEVICE> [I<BLOCK-COUNT>]"
msgstr "B<mkfs.fat> [I<OPTIONEN>] I<GERÄT> [I<BLOCKANZAHL>]"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#.  ----------------------------------------------------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<mkfs.fat> is used to create an MS-DOS filesystem under Linux on a device "
"(usually a disk partition).  I<DEVICE> is the special file corresponding to "
"the device (e.g. /dev/sdXX).  I<BLOCK-COUNT> is the number of blocks on the "
"device.  If omitted, B<mkfs.fat> automatically determines the filesystem "
"size."
msgstr ""
"B<mkfs.fat> wird unter Linux zum Erstellen eines MS-DOS-Dateisystems auf "
"einem Gerät (üblicherweise einer Plattenpartition) verwendet. Das I<GERÄT> "
"ist eine Spezialdatei, die zu einem Gerät gehört (zum Beispiel /dev/sdXX). "
"Die I<BLOCKANZAHL> ist die Anzahl der Blöcke auf dem Gerät. Falls diese "
"nicht angegeben ist, ermittelt B<mkfs.fat> die Dateisystemgröße automatisch."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Normally, for any filesystem except very small ones, B<mkfs.fat> will align "
"all the data structures to cluster size, to make sure that as long as the "
"partition is properly aligned, so will all the data structures in the "
"filesystem.  This option disables alignment; this may provide a handful of "
"additional clusters of storage at the expense of a significant performance "
"degradation on RAIDs, flash media or large-sector hard disks."
msgstr ""
"deaktiviert die Ausrichtung der Datenstrukturen an der Clustergröße. "
"Normalerweise richtet B<mkfs.fat> für jedes Dateisystem die Datenstrukturen "
"auf diese Weise aus, außer bei sehr kleinen Dateisystemen. Durch die "
"Deaktivierung werden einige wenige Speichercluster zusätzlich "
"bereitgestellt, was aber in RAID-Verbünden, auf Flash-Speichermedien oder "
"Festplatten mit großen Sektoren deutlich auf Kosten der Performance geht."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B< -A>"
msgstr "B< -A>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Use Atari variation of the MS-DOS filesystem.  This is default if B<mkfs."
"fat> is run on an Atari, then this option turns off Atari format.  There are "
"some differences when using Atari format: If not directed otherwise by the "
"user, B<mkfs.fat> will always use 2 sectors per cluster, since GEMDOS "
"doesn't like other values very much.  It will also obey the maximum number "
"of sectors GEMDOS can handle.  Larger filesystems are managed by raising the "
"logical sector size.  Under Atari format, an Atari-compatible serial number "
"for the filesystem is generated, and a 12 bit FAT is used only for "
"filesystems that have one of the usual floppy sizes (720k, 1.2M, 1.44M, "
"2.88M), a 16 bit FAT otherwise.  This can be overridden with the B<-F> "
"option.  Some PC-specific boot sector fields aren't written, and a boot "
"message (option B<-m>) is ignored."
msgstr ""
"verwendet die Atari-Variante des MS-DOS-Dateisystems. Dies ist die Vorgabe, "
"wenn B<mkfs.fat> auf einem Atari ausgeführt wird; in diesem Fall schaltet "
"diese Option das Atari-Format aus. Es gibt einige Unterschiede bei der "
"Verwendung des Atari-Formats: Falls vom Benutzer nicht anders angewiesen, "
"verwendet B<mkfs.fat> immer zwei Sektoren pro Cluster, das GEMDOS andere "
"Werte nicht besonders mag. Es folgt auch der maximalen Sektorgröße, mit der "
"GEMDOS umgehen kann. Größere Dateisysteme werden durch Erhöhen der logischen "
"Sektorgröße verwaltet. Unter dem Atari-Format wird eine Atari-kompatible "
"Seriennummer für das Dateisystem erzeugt, und für Dateisysteme einer der "
"üblichen Diskettengrößen (720kB, 1.2MB, 1.44MB, 2.88MB) wird ein 12-Bit-FAT "
"verwendet, anderenfalls ein 16-Bit-FAT. Dies kann mit der Option B<-F> außer "
"Kraft gesetzt werden. Einige PC-spezifische Bootsektor-Felder werden nicht "
"geschrieben und eine Bootmeldung (Option B<-m>) wird ignoriert."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-b> I<SECTOR-OF-BACKUP>"
msgstr "B<-b> I<SICHERUNGSSEKTOR>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Selects the location of the backup boot sector for FAT32.  Default depends "
"on number of reserved sectors, but usually is sector 6.  The backup must be "
"within the range of reserved sectors."
msgstr ""
"gibt den Ort des Boot-Sicherungssektors für FAT32 an. Die Vorgabe ist von "
"der Anzahl der reservierten Sektoren abhängig, aber üblicherweise ist es der "
"Sektor 6. Die Sicherung muss innerhalb des Bereichs der reservierten "
"Sektoren liegen."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-c>"
msgstr "B<-c>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Check the device for bad blocks before creating the filesystem."
msgstr ""
"prüft das Gerät auf fehlerhafte Blöcke, bevor das Dateisystem angelegt wird."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-C>"
msgstr "B<-C>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Create the file given as I<DEVICE> on the command line, and write the to-be-"
"created filesystem to it.  This can be used to create the new filesystem in "
"a file instead of on a real device, and to avoid using B<dd> in advance to "
"create a file of appropriate size.  With this option, the I<BLOCK-COUNT> "
"must be given, because otherwise the intended size of the filesystem "
"wouldn't be known.  The file created is a sparse file, which actually only "
"contains the meta-data areas (boot sector, FATs, and root directory).  The "
"data portions won't be stored on the disk, but the file nevertheless will "
"have the correct size.  The resulting file can be copied later to a floppy "
"disk or other device, or mounted through a loop device."
msgstr ""
"erstellt die als I<GERÄT> in der Befehlszeile angegebene Datei und schreibt "
"das zu erstellende Dateisystem hinein. Dies kann dazu verwendet werden, das "
"neue Dateisystem in einer Datei statt auf einem realen Gerät zu erzeugen und "
"zu vermeiden, vorab B<dd> zu verwenden, um eine Datei der entsprechenden "
"Größe zu erzeugen. Mit dieser Option muss die I<BLOCKANZAHL> angegeben "
"werden, da anderenfalls die beabsichtigte Größe des Dateisystems nicht "
"bekannt wäre. Die erzeugte Datei ist eine Sparse-Datei (Datei mit Löchern), "
"die tatsächlich nur die Metadatenbereiche enthält (Bootsektor, FATs und "
"Wurzelverzeichnis). Die Datenportionen werden nicht auf der Platte "
"gespeichert, aber die Datei wird dennoch die korrekte Größe haben. Die "
"entstandene Datei kann später auf eine Diskette oder ein anderes Gerät "
"geschrieben oder über ein Loop-Gerät eingehängt werden."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-D> I<DRIVE-NUMBER>"
msgstr "B<-D> I<LAUFWERKSNUMMER>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Specify the BIOS drive number to be stored in the FAT boot sector.  This "
"value is usually 0x80 for hard disks and 0x00 for floppy devices or "
"partitions to be used for floppy emulation."
msgstr ""
"gibt die im FAT-Bootsektor zu speichernde BIOS-Laufwerksnummer an. Diese "
"Nummer ist üblicherweise 0x80 für Festplatten und 0x00 für "
"Diskettenlaufwerke oder Partitionen, die als Disketten-Emulation verwendet "
"werden sollen."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-f> I<NUMBER-OF-FATS>"
msgstr "B<-f> I<ANZAHL-DER-FATS>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Specify the number of file allocation tables in the filesystem.  The default "
"is 2."
msgstr ""
"gibt die Anzahl der Dateizuweisungstabellen (»File allocation tables«) im "
"Dateisystem an. Die Vorgabe ist 2."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-F> I<FAT-SIZE>"
msgstr "B<-F> I<FAT-GRÖSSE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Specifies the type of file allocation tables used (12, 16 or 32 bit).  If "
"nothing is specified, B<mkfs.fat> will automatically select between 12, 16 "
"and 32 bit, whatever fits better for the filesystem size."
msgstr ""
"gibt den Typ der verwendeten Dateizuweisungstabellen (FATs) an (12, 16 oder "
"32 Bit). Falls nichts angegeben wird, wählt B<mkfs.fat> automatisch zwischen "
"12, 16 und 32 Bit, je nachdem, was sich am besten für die Größe des "
"Dateisystems eignet."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-h> I<NUMBER-OF-HIDDEN-SECTORS>"
msgstr "B<-h> I<ANZAHL-DER-VERSTECKTEN-SEKTOREN>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Select the number of hidden sectors in the volume.  Apparently some digital "
"cameras get indigestion if you feed them a CF card without such hidden "
"sectors, this option allows you to satisfy them."
msgstr ""
"wählt die Anzahl der versteckten Sektoren im Datenträger. Anscheinend stören "
"sich einige Digitalkameras daran, wenn Sie sie mit einer CF-Karte bestücken, "
"die keine solchen versteckten Sektoren enthält. Mit dieser Option können Sie "
"diese zufriedenstellen."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-i> I<VOLUME-ID>"
msgstr "B<-i> I<DATENTRÄGERKENNUNG>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Sets the volume ID of the newly created filesystem; I<VOLUME-ID> is a 32-bit "
"hexadecimal number (for example, 2e24ec82).  The default is a number which "
"depends on the filesystem creation time."
msgstr ""
"legt die Datenträgerkennung (ID) des neu erstellten Dateisystems fest; die "
"I<DATENTRÄGERKENNUNG> ist eine 32-Bit-Hexadezimalzahl (zum Beispiel "
"2e24ec82). Die Vorgabe ist eine Zahl, die aus der Erstellungszeit des "
"Dateisystems abgeleitet wird."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-I>"
msgstr "B<-I>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"It is typical for fixed disk devices to be partitioned so, by default, you "
"are not permitted to create a filesystem across the entire device.  B<mkfs."
"fat> will complain and tell you that it refuses to work.  This is different "
"when using MO disks.  One doesn't always need partitions on MO disks.  The "
"filesystem can go directly to the whole disk.  Under other OSes this is "
"known as the 'superfloppy' format.  This switch will force B<mkfs.fat> to "
"work properly."
msgstr ""
"Für Plattenlaufwerke ist es typisch, dass sie partitioniert werden, doch es "
"ist Ihnen standardmäßig nicht erlaubt, ein Dateisystem auf dem gesamten "
"Gerät anzulegen. B<mkfs.fat> reklamiert eine solchen Versuch und verweigert "
"die Arbeit. Das ist bei MO-Medien anders. Sie werden dort nicht immer "
"Partitionen benötigen. Das Dateisystem kann sich direkt über das gesamte "
"Medium erstrecken. Unter anderen Betriebssystemen ist dies als das "
"»Superfloppy«-Format bekannt. Dieser Schalter weist B<mkfs.fat> an, korrekt "
"zu arbeiten."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-l> I<FILENAME>"
msgstr "B<-l> I<DATEI>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Read the bad blocks list from I<FILENAME>."
msgstr "liest die Liste der fehlerhaften Blöcke aus der angegebenen I<DATEI>."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-m> I<MESSAGE-FILE>"
msgstr "B<-m> I<MELDUNGSDATEI>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Sets the message the user receives on attempts to boot this filesystem "
"without having properly installed an operating system.  The message file "
"must not exceed 418 bytes once line feeds have been converted to carriage "
"return-line feed combinations, and tabs have been expanded.  If the filename "
"is a hyphen (-), the text is taken from standard input."
msgstr ""
"legt die Meldung fest, die der Benutzer bei Versuchen erhält, das "
"Dateisystem zu booten, ohne dass ein Betriebssystem korrekt installiert ist. "
"Die Größe der Meldungsdatei darf 418 Byte nicht überschreiten, wobei zu "
"beachten ist, dass Zeilenvorschübe in Wagenrücklauf/Zeilenvorschub-"
"Kombinationen umgewandelt und Tabulatoren expandiert werden. Falls der "
"Dateiname ein Minuszeichen (-) ist, wird der Text aus der Standardeingabe "
"gelesen."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-M> I<FAT-MEDIA-TYPE>"
msgstr "B<-M> I<FAT-MEDIENTYP>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Specify the media type to be stored in the FAT boot sector.  This value is "
"usually 0xF8 for hard disks and is 0xF0 or a value from 0xF9 to 0xFF for "
"floppies or partitions to be used for floppy emulation."
msgstr ""
"gibt den Medientyp an, der im FAT-Bootsektor gespeichert werden soll. Dieser "
"Wert ist üblicherweise 0xF8 für Festplatten und 0xF0 oder ein Wert von 0xF9 "
"bis 0xFF für Disketten oder Partitionen, die als Disketten-Emulation "
"verwendet werden sollen."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-n> I<VOLUME-NAME>"
msgstr "B<-n> I<DATENTRÄGERNAME>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Sets the volume name (label) of the filesystem.  The volume name can be up "
"to 11 characters long.  The default is no label."
msgstr ""
"legt den Datenträgernamen (die Bezeichnung) des Dateisystems fest. Der "
"Datenträgername darf bis zu 11 Zeichen lang sein. Die Vorgabe ist keine "
"Bezeichnung."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-r> I<ROOT-DIR-ENTRIES>"
msgstr "B<-r> I<EINTRÄGE-IM-WURZELVERZEICHNIS>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Select the number of entries available in the root directory.  The default "
"is 112 or 224 for floppies and 512 for hard disks."
msgstr ""
"wählt die Anzahl der im Wurzelverzeichnis verfügbaren Einträge. Die Vorgabe "
"ist 112 oder 224 für Disketten und 512 für Festplatten."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-R> I<NUMBER-OF-RESERVED-SECTORS>"
msgstr "B<-R> I<ANZAHL-DER-RESERVIERTEN-SEKTOREN>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Select the number of reserved sectors.  With FAT32 format at least 2 "
"reserved sectors are needed, the default is 32.  Otherwise the default is 1 "
"(only the boot sector)."
msgstr ""
"wählt die Anzahl der reservierten Sektoren. Im FAT32-Format werden "
"mindestens zwei reservierte Sektoren benötigt, die Vorgabe ist 32. "
"Andernfalls ist die Vorgabe 1 (nur der Bootsektor)."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-s> I<SECTORS-PER-CLUSTER>"
msgstr "B<-s> I<SEKTOREN-PRO-CLUSTER>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Specify the number of disk sectors per cluster.  Must be a power of 2, i.e. "
"1, 2, 4, 8, ... 128."
msgstr ""
"wählt die Anzahl der Plattensektoren pro Cluster. Diese Anzahl muss eine "
"Zweierpotenz sein, also 1, 2, 4, 8, … 128."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-S> I<LOGICAL-SECTOR-SIZE>"
msgstr "B<-S> I<LOGISCHE-SEKTORGRÖSSE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Specify the number of bytes per logical sector.  Must be a power of 2 and "
"greater than or equal to 512, i.e. 512, 1024, 2048, 4096, 8192, 16384, or "
"32768.  Values larger than 4096 are not conforming to the FAT file system "
"specification and may not work everywhere."
msgstr ""
"wählt die Anzahl der Bytes pro logischem Sektor. Diese Anzahl muss eine "
"Zweierpotenz und größer oder gleich 512 sein, also 512, 1024, 2048, 4096, "
"8192, 16384 oder 32768. Größere Werte als 4096 sind nicht zur FAT-"
"Dateisystemspezifikation konform und könnten nicht überall funktionieren."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Verbose execution."
msgstr "gibt ausführliche Meldungen aus."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--invariant>"
msgstr "B<--invariant>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Use constants for normally randomly generated or time based data such as "
"volume ID and creation time.  Multiple runs of B<mkfs.fat> on the same "
"device create identical results with this option.  Its main purpose is "
"testing B<mkfs.fat>."
msgstr ""
"verwendet konstante statt zufällig erzeugte Werte oder zeitbasierte Daten "
"für Datenträgerkennung und Erstellungszeit. Mit dieser Option liefern "
"mehrfache Durchläufe von B<mkfs.fat> auf dem gleichen Gerät identische "
"Ergebnisse. Der Hauptzweck dieser Option besteht im Testen von B<mkfs.fat>."

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#.  ----------------------------------------------------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display option summary and exit."
msgstr ""
"zeigt eine Zusammenfassung der Befehlszeilenoptionen an und beendet das "
"Programm."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#.  ----------------------------------------------------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<mkfs.fat> can not create boot-able filesystems.  This isn't as easy as you "
"might think at first glance for various reasons and has been discussed a lot "
"already.  B<mkfs.fat> simply will not support it ;)"
msgstr ""
"B<mkfs.fat> kann keine bootfähigen Dateisysteme anlegen. Das ist aus "
"verschiedenen Gründen nicht so einfach, wie Sie auf den ersten Blick "
"vielleicht denken und wurde bereits ausgiebig diskutiert. B<mkfs.fat> wird "
"dies schlichtweg nicht unterstützen ;)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<fatlabel>(8)"
msgstr "B<fatlabel>(8)"

#.  ----------------------------------------------------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<fsck.fat>(8)"
msgstr "B<fsck.fat>(8)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "HOMEPAGE"
msgstr "HOMEPAGE"

#.  ----------------------------------------------------------------------------
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The home for the B<dosfstools> project is its E<.UR https://github.com/"
"dosfstools/dosfstools> GitHub project page E<.UE .>"
msgstr ""
"Die Heimat des Projekts B<dosfstools> ist seine E<.UR https://github.com/"
"dosfstools/dosfstools> GitHub-Projektseite E<.UE .>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<dosfstools> were written by E<.MT werner.almesberger@\\:lrc.di.epfl.ch> "
"Werner Almesberger E<.ME ,> E<.MT Roman.Hodek@\\:informatik.\\:uni-erlangen."
"de> Roman Hodek E<.ME ,> and others.  The current maintainer is E<.MT aeb@\\:"
"debian.org> Andreas Bombe E<.ME .>"
msgstr ""
"B<dosfstools> wurde von E<.MT werner.almesberger@\\:lrc.di.epfl.ch> Werner "
"Almesberger E<.ME ,> E<.MT Roman.Hodek@\\:informatik.\\:uni-erlangen.de>, "
"Roman Hodek und anderen geschrieben. Der aktuelle Betreuer ist E<.MT aeb@\\:"
"debian.org> Andreas Bombe E<.ME .>"
