# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.16\n"
"POT-Creation-Date: 2020-01-05 21:57+01:00\n"
"PO-Revision-Date: 2020-01-06 00:47+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 19.08.3\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "CHECKUPDATES"
msgstr "CHECKUPDATES"

#. type: TH
#: archlinux
#, no-wrap
msgid "2019-12-25"
msgstr "25. Dezember 2019"

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib 1\\&.3\\&.0"
msgstr "Pacman-contrib 1\\&.3\\&.0"

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib Manual"
msgstr "Pacman-contrib-Handbuch"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux
msgid "checkupdates - safely print a list of pending updates"
msgstr ""
"checkupdates - eine Liste der ausstehenden Aktualisierungen sicher ausgeben"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux
msgid "I<checkupdates> [options]"
msgstr "I<checkupdates> [Optionen]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux
msgid ""
"Checks for pending updates using a separate Pacman database located in "
"B<TMPDIR> if B<CHECKUPDATES_DB> is not set and outputs a list of updates "
"with the old and new version\\&."
msgstr ""
"Prüft mittels einer separaten Pacman-Datenbank in B<TMPDIR> auf ausstehende "
"Aktualisierungen (falls B<CHECKUPDATES_DB> nicht gesetzt ist) und gibt eine "
"Liste der Aktualisierungen mit der alten und neuen Version aus\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux
msgid "B<-d, --download>"
msgstr "B<-d, --download>"

#. type: Plain text
#: archlinux
msgid "Download any pending updates to the pacman cache\\&."
msgstr ""
"lädt jegliche ausstehenden Aktualisierungen in den Zwischenspeicher von "
"Pacman herunter\\&."

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr "B<-h, --help>"

#. type: Plain text
#: archlinux
msgid "Display syntax and command line options\\&."
msgstr "zeigt die Optionen der Syntax und Befehlszeile an\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: Plain text
#: archlinux
msgid "B<CHECKUPDATES_DB>"
msgstr "B<CHECKUPDATES_DB>"

#. type: Plain text
#: archlinux
msgid "Override the default update db location\\&."
msgstr ""
"setzt den vorgegebenen Ort der Aktualisierungsdatenbank außer Kraft\\&."

#. type: Plain text
#: archlinux
msgid "B<TMPDIR>"
msgstr "B<TMPDIR>"

#. type: Plain text
#: archlinux
msgid "Overrides the default I</tmp> temporary directory\\&."
msgstr "setzt das vorgegebene I</tmp> als temporäres Verzeichnis außer Kraft."

#. type: SH
#: archlinux
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux
msgid "On exit, checkpkg will return one of the following error codes\\&."
msgstr "Beim Beenden gibt Checkpkg einen der folgenden Fehlercodes aus\\&."

#. type: Plain text
#: archlinux
msgid "0"
msgstr "0"

#. type: Plain text
#: archlinux
msgid "Normal exit condition\\&."
msgstr "Normales Beenden\\&."

#. type: Plain text
#: archlinux
msgid "1"
msgstr "1"

#. type: Plain text
#: archlinux
msgid "Unknown cause of failure\\&."
msgstr "Fehler mit unbekannter Ursache\\&."

#. type: Plain text
#: archlinux
msgid "2"
msgstr "2"

#. type: Plain text
#: archlinux
msgid "No updates are available\\&."
msgstr "Es sind keine Aktualisierungen verfügbar\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux
msgid "B<pacman>(8), B<pacman.conf>(5)"
msgstr "B<pacman>(8), B<pacman.conf>(5)"

#. type: SH
#: archlinux
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux
msgid ""
"Bugs? You must be kidding; there are no bugs in this software\\&. But if we "
"happen to be wrong, send us an email with as much detail as possible to "
"pacman-contrib@lists\\&.archlinux\\&.org\\&."
msgstr ""
"Fehler? Sie machen wohl Witze, es gibt keine Fehler in dieser Software\\&. "
"Nun ja, sollte unsere Annahme doch falsch sein, senden Sie uns eine E-Mail "
"(auf Englisch) mit so vielen Details wie möglich an pacman-contrib@lists\\&."
"archlinux\\&.org\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: archlinux
msgid "Current maintainers:"
msgstr "Derzeitige Betreuer:"

#. type: Plain text
#: archlinux
msgid "Johannes LÃ¶thberg E<lt>johannes@kyriasis\\&.comE<gt>"
msgstr "Johannes Löthberg E<lt>johannes@kyriasis\\&.comE<gt>"

#. type: Plain text
#: archlinux
msgid "Daniel M\\&. Capella E<lt>polycitizen@gmail\\&.comE<gt>"
msgstr "Daniel M\\&. Capella E<lt>polycitizen@gmail\\&.comE<gt>"

#. type: Plain text
#: archlinux
msgid ""
"For additional contributors, use git shortlog -s on the pacman-contrib\\&."
"git repository\\&."
msgstr ""
"Informationen zu weiteren Mitwirkenden erhalten Sie, wenn Sie den Befehl "
"B<git shortlog -s> im Repositorium pacman-contrib\\&.git aufrufen\\&."
