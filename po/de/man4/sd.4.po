# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Schulze <joey@infodrom.org>
# Norbert Kümin <norbert.kuemin@lugs.ch>
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2019-11-29 11:42+01:00\n"
"PO-Revision-Date: 2018-02-03 21:25+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SD"
msgstr "SD"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "sd - driver for SCSI disk drives"
msgstr "sd - Treiber für SCSI-Platten"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<#include E<lt>linux/hdreg.hE<gt>        >/* for HDIO_GETGEO */\n"
"B<#include E<lt>linux/fs.hE<gt>           >/* for BLKGETSIZE and BLKRRPART */\n"
msgstr ""
"B<#include E<lt>linux/hdreg.hE<gt>        >/* für HDIO_GETGEO */\n"
"B<#include E<lt>linux/fs.hE<gt>           >/* für BLKGETSIZE und BLKRRPART */\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFIGURATION"
msgstr "KONFIGURATION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The block device name has the following form: B<sd>I<lp,> where I<l> is a "
"letter denoting the physical drive, and I<p> is a number denoting the "
"partition on that physical drive.  Often, the partition number, I<p>, will "
"be left off when the device corresponds to the whole drive."
msgstr ""
"Der Name für das blockorientierte Gerät hat die Form: B<sd>I<lp>. Darin gibt "
"I<l> das physikalische Laufwerk und I<p> die Partitionsnummer darauf an. Die "
"Partitionsnummer wird weggelassen, wenn das ganze Laufwerk angesprochen "
"werden soll."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"SCSI disks have a major device number of 8, and a minor device number of the "
"form (16 * I<drive_number>) + I<partition_number>, where I<drive_number> is "
"the number of the physical drive in order of detection, and "
"I<partition_number> is as follows:"
msgstr ""
"SCSI-Platten haben eine Haupt-Gerätenummer (Major Device Number) von 8 und "
"eine Neben-Gerätenummer (Minor Device Number) der Form (16 * "
"I<Laufwerksnummer>) + I<Partitionsnummer>. Die I<Laufwerksnummer> entspricht "
"der Nummer des physikalischen Laufwerks in der Reihenfolge der Erkennung. "
"Die I<Partitionsnummer> wird wie folgt angegeben:"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "+3"
msgstr "+3"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "partition 0 is the whole drive"
msgstr "Partition 0 ist das ganze Laufwerk;"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "partitions 1\\(en4 are the DOS \"primary\" partitions"
msgstr "Partition 1-4 sind die »primären« DOS-Partitionen;"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "partitions 5\\(en8 are the DOS \"extended\" (or \"logical\") partitions"
msgstr "Partition 5-8 sind »erweiterte« (oder »logische«) DOS-Partitionen."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For example, I</dev/sda> will have major 8, minor 0, and will refer to all "
"of the first SCSI drive in the system; and I</dev/sdb3> will have major 8, "
"minor 19, and will refer to the third DOS \"primary\" partition on the "
"second SCSI drive in the system."
msgstr ""
"Beispielsweise ist für I</dev/sda> Major 8, Minor 0 und bezieht sich auf das "
"gesamte erste SCSI-Laufwerk im System. I</dev/sdb3> hat Major 8, Minor 19 "
"und bezieht sich auf die dritte »primäre« DOS-Partition des zweiten SCSI-"
"Laufwerkes im System."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"At this time, only block devices are provided.  Raw devices have not yet "
"been implemented."
msgstr ""
"Derzeit werden nur blockorientierte Geräte unterstützt. Direkter "
"Gerätezugriff ist noch nicht implementiert."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The following I<ioctl>s are provided:"
msgstr "Die folgenden Aufrufe von B<ioctl>(2) werden unterstützt:"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<HDIO_GETGEO>"
msgstr "B<HDIO_GETGEO>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Returns the BIOS disk parameters in the following structure:"
msgstr "Liefert die BIOS-Plattenparameter in der folgenden Struktur:"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"struct hd_geometry {\n"
"    unsigned char  heads;\n"
"    unsigned char  sectors;\n"
"    unsigned short cylinders;\n"
"    unsigned long  start;\n"
"};\n"
msgstr ""
"struct hd_geometry {\n"
"    unsigned char  heads;\n"
"    unsigned char  sectors;\n"
"    unsigned short cylinders;\n"
"    unsigned long  start;\n"
"};\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "A pointer to this structure is passed as the B<ioctl>(2)  parameter."
msgstr ""
"Ein Zeiger auf diese Struktur wird als Parameter an B<ioctl>(2) übergeben."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The information returned in the parameter is the disk geometry of the drive "
"I<as understood by DOS!> This geometry is I<not> the physical geometry of "
"the drive.  It is used when constructing the drive's partition table, "
"however, and is needed for convenient operation of B<fdisk>(1), "
"B<efdisk>(1), and B<lilo>(1).  If the geometry information is not available, "
"zero will be returned for all of the parameters."
msgstr ""
"Die in diesem Parameter gelieferten Informationen entsprechen der "
"Plattengeometrie des Laufwerkes, I<wie sie von DOS verstanden wird!> Diese "
"Geometrie entspricht I<nicht> der physikalischen Geometrie des Laufwerks. "
"Sie werden bei der Erstellung der Partitionstabelle des Laufwerks verwendet "
"und betreffen die Programme B<fdisk>(1), B<efdisk>(1) und B<lilo>(8). Wenn "
"die Geometrie nicht ermittelt werden kann, wird für alle Parameter Null "
"zurückgegeben."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<BLKGETSIZE>"
msgstr "B<BLKGETSIZE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Returns the device size in sectors.  The B<ioctl>(2)  parameter should be a "
"pointer to a I<long>."
msgstr ""
"Ergibt die Gerätegröße in Sektoren. Der Parameter für B<ioctl>(2) sollte ein "
"Zeiger auf eine Variable des Datentyps B<long> sein."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<BLKRRPART>"
msgstr "B<BLKRRPART>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Forces a reread of the SCSI disk partition tables.  No parameter is needed."
msgstr ""
"Erzwingt ein erneutes Lesen der Partitionstabelle der SCSI-Platte. Keine "
"Parameter erforderlich."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The SCSI B<ioctl>(2)  operations are also supported.  If the B<ioctl>(2)  "
"parameter is required, and it is NULL, then B<ioctl>(2)  fails with the "
"error B<EINVAL>."
msgstr ""
"Die SCSI B<ioctl>(2)-Operationen werden ebenfalls unterstützt. Wird der "
"B<ioctl>(2)-Parameter benötigt und dieser ist NULL, so schlägt B<ioctl>(2) "
"mit dem Fehler B<EINVAL> fehl."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "I</dev/sd[a-h]>"
msgstr "I</dev/sd[a-h]>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "the whole device"
msgstr "das ganze Gerät"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "I</dev/sd[a-h][0-8]>"
msgstr "I</dev/sd[a-h][0-8]>"

#. .SH SEE ALSO
#. .BR scsi (4)
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "individual block partitions"
msgstr "individuelle Partitionen"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.04 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.01 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
