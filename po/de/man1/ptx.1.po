# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Karl Eichwalder <ke@suse.de>
# Lutz Behnke <lutz.behnke@gmx.de>
# Michael Piefel <piefel@debian.org>
# Michael Schmidt <michael@guug.de>
# Chris Leick <c.leick@vollbio.de>, 2010.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2019-11-16 21:21+01:00\n"
"PO-Revision-Date: 2019-03-22 23:08+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "PTX"
msgstr "PTX"

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2019"
msgstr "November 2019"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.31"
msgstr "GNU Coreutils 8.31"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "ptx - produce a permuted index of file contents"
msgstr "ptx - Einen umgestellten Index von Dateiinhalten erstellen"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<ptx> [I<\\,OPTION\\/>]... [I<\\,INPUT\\/>]...  I<\\,(without -G)\\/>"
msgstr "B<ptx> [I<\\,OPTION\\/>]… [I<\\,EINGABE\\/>]… I<\\,(ohne -G)\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<ptx> I<\\,-G \\/>[I<\\,OPTION\\/>]... [I<\\,INPUT \\/>[I<\\,OUTPUT\\/>]]"
msgstr ""
"B<ptx> I<\\,-G \\/>[I<\\,OPTION\\/>]… [I<\\,EINGABE \\/>[I<\\,AUSGABE\\/>]]"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Output a permuted index, including context, of the words in the input files."
msgstr ""
"Einen vertauschten Index der Wörter der Eingabedateien einschließlich "
"Kontext ausgeben."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "With no FILE, or when FILE is -, read standard input."
msgstr "liest ohne DATEI oder wenn DATEI »-« ist, aus der Standardeingabe."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Die obligatorischen Argumente für Optionen sind für deren Kurz- und Langform "
"gleich."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-A>, B<--auto-reference>"
msgstr "B<-A>, B<--auto-reference>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output automatically generated references"
msgstr "Automatisch generierte Referenzen ausgeben"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-G>, B<--traditional>"
msgstr "B<-G>, B<--traditional>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "behave more like System V 'ptx'"
msgstr "Verhalten von »ptx« eher an System-V anpassen"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-F>, B<--flag-truncation>=I<\\,STRING\\/>"
msgstr "B<-F>, B<--flag-truncation>=I<\\,ZEICHENKETTE\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "use STRING for flagging line truncations.  The default is '/'"
msgstr ""
"ZEICHENKETTE benutzen, um Zeilenkürzungen zu markieren. Vorgabe ist »/«"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-M>, B<--macro-name>=I<\\,STRING\\/>"
msgstr "B<-M>, B<--macro-name>=I<\\,ZEICHENKETTE\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "macro name to use instead of 'xx'"
msgstr "Makroname, der statt »xx« zu benutzen ist"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-O>, B<--format>=I<\\,roff\\/>"
msgstr "B<-O>, B<--format>=I<\\,roff\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "generate output as roff directives"
msgstr "Ausgabe als Roff‐Anweisungen erzeugen"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-R>, B<--right-side-refs>"
msgstr "B<-R>, B<--right-side-refs>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "put references at right, not counted in B<-w>"
msgstr "Referenzen nach rechts setzen, in B<-w> nicht gezählt"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-S>, B<--sentence-regexp>=I<\\,REGEXP\\/>"
msgstr "B<-S>, B<--sentence-regexp>=I<\\,REGULÄRER_AUSDRUCK\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "for end of lines or end of sentences"
msgstr "Für Zeilen‐ oder Satzende"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-T>, B<--format>=I<\\,tex\\/>"
msgstr "B<-T>, B<--format>=I<\\,tex\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "generate output as TeX directives"
msgstr "Ausgabe als TeX‐Anweisungen erzeugen"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-W>, B<--word-regexp>=I<\\,REGEXP\\/>"
msgstr "B<-W>, B<--word-regexp>=I<\\,REGULÄRER_AUSDRUCK\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "use REGEXP to match each keyword"
msgstr "REGULÄRER_AUSDRUCK benutzen, um jedes Schlüsselwort zu vergleichen"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-b>, B<--break-file>=I<\\,FILE\\/>"
msgstr "B<-b>, B<--break-file>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "word break characters in this FILE"
msgstr "Wortendezeichen in dieser Datei"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-f>, B<--ignore-case>"
msgstr "B<-f>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "fold lower case to upper case for sorting"
msgstr "Zum Sortieren Klein‐ in Großschreibung wandeln"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-g>, B<--gap-size>=I<\\,NUMBER\\/>"
msgstr "B<-g>, B<--gap-size>=I<\\,ZAHL\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "gap size in columns between output fields"
msgstr "Größe des Zwischenraums zwischen Ausgabefeldern"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-i>, B<--ignore-file>=I<\\,FILE\\/>"
msgstr "B<-i>, B<--ignore-file>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "read ignore word list from FILE"
msgstr "Liste zu ignorierender Wörter aus DATEI lesen"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-o>, B<--only-file>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--only-file>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "read only word list from this FILE"
msgstr "Wortliste nur aus dieser DATEI lesen"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-r>, B<--references>"
msgstr "B<-r>, B<--references>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "first field of each line is a reference"
msgstr "Erstes Feld jeder Zeile ist eine Referenz"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<-t>, B<--typeset-mode> - not implemented -"
msgstr "B<-t>, B<--typeset-mode> – nicht implementiert –"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-w>, B<--width>=I<\\,NUMBER\\/>"
msgstr "B<-w>, B<--width>=I<\\,ZAHL\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output width in columns, reference excluded"
msgstr "Breite in der Spalten ausgeben, ohne die Referenz"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "display this help and exit"
msgstr "zeigt Hilfeinformationen an und beendet das Programm."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "output version information and exit"
msgstr "gibt Versionsinformationen aus und beendet das Programm."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Written by F. Pinard."
msgstr "Geschrieben von F. Pinard."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Onlinehilfe für GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Melden Sie Fehler in der Übersetzung an E<lt>https://translationproject.org/"
"team/de.htmlE<gt>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2019 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2019 Free Software Foundation, Inc. Lizenz GPLv3+: GNU GPL "
"Version 3 oder neuer E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dies ist freie Software: Sie können sie verändern und weitergeben. Es gibt "
"KEINE GARANTIE, soweit gesetzlich zulässig."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux mageia-cauldron
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/ptxE<gt>"
msgstr ""
"Vollständige Dokumentation unter: E<lt>https://www.gnu.org/software/"
"coreutils/ptxE<gt>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) ptx invocation\\(aq"
msgstr "oder lokal verfügbar mit: info \\(aq(coreutils) ptx invocation\\(aq"

#. type: TH
#: debian-buster
#, no-wrap
msgid "February 2019"
msgstr "Februar 2019"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr "GNU Coreutils 8.30"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Report ptx translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Berichten Sie Fehler in der Übersetzung von ptx an E<lt>https://"
"translationproject.org/team/de.htmlE<gt>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2018 Free Software Foundation, Inc. Lizenz GPLv3+: GNU GPL "
"Version 3 oder neuer E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/ptxE<gt>"
msgstr ""
"Vollständige Dokumentation unter: E<lt>https://www.gnu.org/software/"
"coreutils/ptxE<gt>"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr "August 2019"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr "März 2019"
