# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2019-08-03 17:58+02:00\n"
"PO-Revision-Date: 2017-08-01 20:46+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "repoquery"
msgstr "repoquery"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "17 October 2005"
msgstr "17. Oktober 2005"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "Panu Matilainen"
msgstr "Panu Matilainen"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-buster debian-unstable
msgid "repoquery - query information from Yum repositories"
msgstr "repoquery - Informationen aus Yum-Paketquellen abfragen"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<repoquery> [options] E<lt>item ...E<gt>"
msgstr "B<repoquery> [Optionen] E<lt>Objekt …E<gt>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<repoquery> -a [options]"
msgstr "B<repoquery> -a [Optionen]"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"B<repoquery> is a program for querying information from YUM repositories "
"similarly to rpm queries."
msgstr ""
"B<repoquery> ist ein Programm zur Abfrage von Informationen aus Yum-"
"Paketquellen, analog zu RPM-Abfragen."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "GENERAL OPTIONS"
msgstr "ALLGEMEINE OPTIONEN"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--querytags>"
msgstr "B<--querytags>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "List valid queryformat tags and exit.."
msgstr ""
"listet zulässige Formatabfragekennzeichnungen auf und beendet das Programm."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-v, --version>"
msgstr "B<-v, --version>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Report program version and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--repoid=E<lt>repoE<gt>>"
msgstr "B<--repoid=E<lt>PaketquelleE<gt>>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Specify which repository to query. Using this option disables all "
"repositories not explicitly enabled with --repoid option (can be used "
"multiple times). By default repoquery uses whatever repositories are enabled "
"in YUM configuration."
msgstr ""
"gibt die abzufragende Paketquelle an. Diese Option deaktiviert alle "
"Paketquellen, die nicht mit der Option --repoid aktiviert werden (dies kann "
"mehrmals angegeben werden). Standardmäßig benutzt Repoquery sämtliche in der "
"YUM-Konfiguration enthaltenen und aktivierten Paketquellen."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--enablerepo=E<lt>repoE<gt>>"
msgstr "B<--enablerepo=E<lt>PaketquelleE<gt>>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"In addition to the default set, query the given additional repository, even "
"if it is disabled in YUM configuration.  Can be used multiple times."
msgstr ""
"fragt zusätzlich zu den vorgegebenen Paketquellen eine weitere Quelle ab, "
"selbst wenn diese in der YUM-Konfiguration deaktiviert ist. Dies kann "
"mehrmals angegeben werden."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--disablerepo=E<lt>repoE<gt>>"
msgstr "B<--disablerepo=E<lt>PaketquelleE<gt>>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Do not query the given repository, even if it is enabled in YUM "
"configuration.  Can be used multiple times."
msgstr ""
"überspringt die Abfrage der angegebenen Paketquelle, selbst wenn diese in "
"der YUM-Konfiguration aktiviert ist. Dies kann mehrmals angegeben werden."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--repofrompath=E<lt>repoidE<gt>,E<lt>path/urlE<gt>>"
msgstr "B<--repofrompath=E<lt>Paketquellen-IDE<gt>,E<lt>Pfad/URLE<gt>>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Specify a path or url to a repository (same path as in a baseurl) to add to "
"the repositories for this query. This option can be used multiple times. If "
"you want to view only the pkgs from this repository combine this with --"
"repoid. The repoid for the repository is specified by E<lt>repoidE<gt>."
msgstr ""
"gibt einen Pfad oder eine URL zu einer Paketquelle an (gleicher Pfad wie in "
"der Basis-URL), um für diese Abfrage Paketquellen hinzuzufügen. Diese Option "
"kann mehrmals angegeben werden. Falls nur die Pakete aus dieser Paketquelle "
"angezeigt werden sollen, kombinieren Sie diese Option mit --repoid. Die "
"Paketquellen-ID wird durch E<lt>Paketquellen-IDE<gt> angegeben."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--plugins>"
msgstr "B<--plugins>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Enable YUM plugin support."
msgstr "aktiviert die Unterstützung für Yum-Plugins."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-q, --query>"
msgstr "B<-q, --query>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "For rpmquery compatibility, doesn't do anything."
msgstr ""
"hat keinen Effekt, es ist nur für die rpmquery-Kompatibilität vorhanden."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-h, --help>"
msgstr "B<-h, --help>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Help; display a help message and then quit\\&."
msgstr "zeigt eine Hilfemeldung an und beendet das Programm\\&."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--quiet>"
msgstr "B<--quiet>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Run quietly: no warnings printed to stderr."
msgstr ""
"aktiviert den stillen Modus: Es werden keine Warnungen in die "
"Standardfehlerausgabe geleitet."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--verbose>"
msgstr "B<--verbose>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Produce verbose output."
msgstr "zeigt ausführliche Informationen an."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-C, --cache>"
msgstr "B<-C, --cache>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Tells repoquery to run entirely from YUM cache - does not download any "
"metadata or update the cache. Queries in this mode can fail or give partial/"
"incorrect results if the cache isn't fully populated beforehand with eg "
"\"yum makecache\"."
msgstr ""
"führt dazu, dass repoquery vollständig aus dem Yum-Zwischenspeicher "
"ausgeführt wird. Es werden weder Metadaten heruntergeladen noch der "
"Zwischenspeicher aktualisiert. Abfragen in diesem Modus können scheitern "
"oder unvollständige und inkorrekte Ergebnisse liefern, wenn der "
"Zwischenspeicher zuvor nicht mit beispielsweise »yum makechache« aufgebaut "
"wurde."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--tempcache>"
msgstr "B<--tempcache>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Create and use a private cache instead of the main YUM cache. This is used "
"by default when run as non-root user."
msgstr ""
"erstellt und verwendet einen privaten Zwischenspeicher, anstatt den Haupt-"
"Zwischenspeicher von Yum zu nutzen. Dies ist die Voreinstellung, wenn das "
"Programm als gewöhnlicher Benutzer ausgeführt wird."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-c E<lt>config fileE<gt>, --config=E<lt>config fileE<gt>>"
msgstr "B<-c E<lt>KonfigurationsdateiE<gt>, --config=E<lt>KonfigurationsdateiE<gt>>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Use alternative config file (default is /etc/yum.conf)."
msgstr ""
"verwendet eine alternative Konfigurationsdatei (Vorgabe ist /etc/yum.conf)."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--releasever=version>"
msgstr "B<--releasever=Version>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Pretend the current release version is the given string. This is very useful "
"when combined with --installroot. You can also use --releasever=/ to take "
"the releasever information from outside the installroot.  Note that with the "
"default upstream cachedir, of /var/cache/yum, using this option will corrupt "
"your cache (and you can use $releasever in your cachedir configuration to "
"stop this)."
msgstr ""
"setzt die angegebene Zeichenkette als aktuelle Release-Version fest. Dies "
"ist in Verbindung mit --installroot sinnvoll. Sie können auch --releasever=/ "
"angeben, um die Informationen zur Release-Version von einem Ort außerhalb "
"der Installationswurzel zu beziehen. Beachten Sie: Wenn Sie das vorgegebene "
"Zwischenspeicherverzeichnis /var/cache/yum verwenden, kann diese Option "
"Ihren Zwischenspeicher beschädigen. Um dies zu verhindern, können Sie die "
"Variable $releasever in der Konfiguration Ihres Zwischenspeicher-"
"Verzeichnisses verwenden."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--setopt=option=value>"
msgstr "B<--setopt=Option=Wert>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Set any config option in yum config or repo files. For options in the global "
"config just use: --setopt=option=value for repo options use: --setopt=repoid."
"option=value"
msgstr ""
"setzt beliebige Konfigurationsoptionen in Konfigurations- oder "
"Paketquellendateien von Yum. Die globale Konfiguration ändern Sie mit --"
"setopt=Option=Wert, die Paketquellen mit --setopt=Paketquellen-ID."
"Option=Wert."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "PACKAGE QUERY OPTIONS"
msgstr "OPTIONEN ZUR PAKETABFRAGE"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-i, --info>"
msgstr "B<-i, --info>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Show general information about package similarly to \"rpm -qi\""
msgstr ""
"zeigt allgemeine Informationen zu einem Paket an, ähnlich wie »rpm -qi«."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-l, --list>"
msgstr "B<-l, --list>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "List files in package."
msgstr "listet die Dateien im Paket auf."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-R, --requires>"
msgstr "B<-R, --requires>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "List package dependencies."
msgstr "listet die Abhängigkeiten des Pakets auf."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--resolve>"
msgstr "B<--resolve>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"When used with --requires, resolve capabilities to originating packages."
msgstr ""
"löst in Verbindung mit --requires die Fähigkeiten zu den Ursprungspaketen "
"auf."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--provides>"
msgstr "B<--provides>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "List capabilities package provides."
msgstr "listet die Fähigkeiten auf, die das Paket bereitstellt."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--obsoletes>"
msgstr "B<--obsoletes>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "List capabilities obsoleted by package."
msgstr "listet die Fähigkeiten auf, die das Paket als obsolet markiert."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--conflicts>"
msgstr "B<--conflicts>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "List capabilities conflicting with package."
msgstr "listet die Fähigkeiten auf, mit denen das Paket im Konflikt steht."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--changelog>"
msgstr "B<--changelog>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "List package changelog."
msgstr "zeigt das Änderungsprotokoll des Pakets an."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--location>"
msgstr "B<--location>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Show a location where the package could be downloaded from.  For example: "
"B<wget `repoquery --location yum`>"
msgstr ""
"zeigt einen Ort an, von dem das Paket heruntergeladen werden kann. Beispiel: "
"B<wget `repoquery --location yum`>"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-s, --source>"
msgstr "B<-s, --source>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Show package source RPM name."
msgstr "zeigt den Source-RPM-Namen des Pakets an."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--srpm>"
msgstr "B<--srpm>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Operate on corresponding source RPM."
msgstr "arbeitet mit dem entsprechenden Quellpaket."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--groupmember PACKAGE>"
msgstr "B<--groupmember PAKET>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "List the repodata groups (yumgroups.xml) belongs to (if any)."
msgstr ""
"listet die repodata-Gruppen auf (yumgroups.xml), zu denen das Paket gehört "
"(falls zutreffend)."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--nvr>"
msgstr "B<--nvr>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Use name-version-release output format (rpm query default)"
msgstr ""
"verwendet Name-Version-Release als Ausgabeformat (Voreinstellung für die RPM-"
"Abfrage)."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--nevra>"
msgstr "B<--nevra>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Use name-epoch:version-release.architecture output format (default)"
msgstr ""
"verwendet Name-Epoche:Version-Release.Architektur als Ausgabeformat "
"(Voreinstellung)."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--envra>"
msgstr "B<--envra>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Use epoch:name-version-release.architecture output format (easier to parse "
"than nevra)"
msgstr ""
"verwendet Epoche:Name-Version-Release.Architektur als Ausgabeformat. Dies "
"lässt sich einfacher auswerten als Name-Epoche:Version-Release.Architektur."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--qf=FORMAT, --queryformat=FORMAT>"
msgstr "B<--qf=FORMAT, --queryformat=FORMAT>"

# http://php.net/manual/de/function.sprintf.php
#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Specify custom output format for queries. You can add \":date\", \":day\" "
"and \":isodate\" to all the tags that are a time, and you can add \":k\", \":"
"m\", \":g\", \":t\" and \":h\" to sizes. You can also specify field width as "
"in sprintf (Eg. %-20{name})"
msgstr ""
"gibt ein benutzerdefiniertes Ausgabeformat für die Abfragen an. Sie können »:"
"date«, »:day« und »:isodate« zu allen Zeitangaben-Tags hinzufügen sowie »:"
"k«, »:m«, »:g«, »:t« und »:h« zu Größenangaben-Tags. Außerdem können Sie "
"Feldbreiten wie in der PHP-Funktion sprintf eingeben, zum Beispiel "
"%-20{name}."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--output [text|ascii-tree|dot-tree]>"
msgstr "B<--output [text|ascii-tree|dot-tree]>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Output format which can be used with --requires/--whatrequires/--obsoletes/--"
"conflicts.  Default output is 'text'."
msgstr ""
"Dieses Ausgabeformat kann mit »--requires«, »--whatrequires«, »--obsoletes« "
"und »--conflicts« verwendet werden. Die Voreinstellung ist »text«."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--level [all|any int]>"
msgstr "B<--level [all|any int]>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"In combination with --output ascii-tree|dot-tree this option specifies the "
"number of level to print on the tree. Default level is 'all'."
msgstr ""
"gibt in Kombination mit --output ascii-tree|dot-tree die Anzahl der in der "
"Baumansicht anzuzeigenden Ebenen an. Die Voreinstellung ist »all«."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "PACKAGE SELECTION OPTIONS"
msgstr "OPTIONEN ZUR PAKETAUSWAHL"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-a, --all>"
msgstr "B<-a, --all>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Query all available packages (for rpmquery compatibility / shorthand for "
"repoquery '*')"
msgstr ""
"sucht nach allen verfügbaren Paketen (für Kompatibilität zu rpmquery, Kürzel "
"für repoquery '*')"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-f, --file FILE>"
msgstr "B<-f, --file DATEI>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Query package owning FILE."
msgstr "sucht nach Paketen, die diese DATEI enthalten."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--whatobsoletes CAPABILITY>"
msgstr "B<--whatobsoletes FÄHIGKEIT>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Query all packages that obsolete CAPABILITY."
msgstr "sucht nach Paketen, die diese FÄHIGKEIT als obsolet markieren."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--whatconflicts CAPABILITY>"
msgstr "B<--whatconflicts FÄHIGKEIT>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Query all packages that conflict with CAPABILITY."
msgstr "sucht nach Paketen, die Konflikte mit dieser FÄHIGKEIT verursachen."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--whatprovides CAPABILITY>"
msgstr "B<--whatprovides FÄHIGKEIT>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Query all packages that provide CAPABILITY."
msgstr "sucht nach Paketen, die diese FÄHIGKEIT bereitstellen."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--whatrequires CAPABILITY>"
msgstr "B<--whatrequires FÄHIGKEIT>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Query all packages that require CAPABILITY."
msgstr "sucht nach Paketen, die diese FÄHIGKEIT benötigen."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--alldeps>"
msgstr "B<--alldeps>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"When used with --whatrequires, look for non-explicit dependencies in "
"addition to explicit ones (e.g. files and Provides in addition to package "
"names).  This is the default."
msgstr ""
"wird zusammen mit --whatrequires verwendet, um zusätzlich zu den expliziten "
"Abhängigkeiten auch nicht-explizite Abhängigkeiten einzubeziehen. So werden "
"zum Beispiel außer Paketnamen auch enthaltene Dateien und »Provides:«-Tags "
"berücksichtigt. Dies ist die Voreinstellung."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--exactdeps>"
msgstr "B<--exactdeps>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"When used with --whatrequires, search for dependencies only exactly as "
"given.  This is effectively the opposite of --alldeps."
msgstr ""
"sucht in Verbindung mit --whatrequires nach exakt den angegebenen "
"Abhängigkeiten. Dies ist effektiv das Gegenteil von --alldeps."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--recursive>"
msgstr "B<--recursive>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "When used with --whatrequires, query packages recursively."
msgstr "sucht in Verbindung mit --whatrequires rekursiv nach Paketen."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--archlist=ARCH1[,ARCH2...]>"
msgstr "B<--archlist=ARCH1[,ARCH2…]>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Limit the query to packages of given architecture(s). Valid values are all "
"architectures known to rpm/yum such as 'i386' and 'src' for source RPMS. "
"Note that repoquery will now change yum's \"arch\" to the first value in the "
"archlist. So \"--archlist=i386,i686\" will change yum's canonical arch to "
"i386, but allow packages of i386 and i686."
msgstr ""
"begrenzt die Abfrage der Pakete auf die angegebene(n) Architektur(en). "
"Zulässige Werte sind hierbei alle Architekturen, die RPM bzw. Yum bekannt "
"sind, beispielsweise »i386«, oder »src« für Quellpakete. Beachten Sie, dass "
"repoquery den Wert von »arch« in Yum auf den ersten Wert in der "
"Architekturenliste ändert. So ändert »--archlist=i386,i686« die kanonische "
"Architektur von Yum zu i386, wobei Pakete für i386 und auch i686 erlaubt "
"sind."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--pkgnarrow=WHAT>"
msgstr "B<--pkgnarrow=WAS>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Limit what packages are considered for the query. Valid values for WHAT are: "
"installed, available, recent, updates, extras, all and repository (default)."
msgstr ""
"begrenzt die bei der Abfrage zu berücksichtigenden Pakete. Zulässige Werte "
"für WAS sind »installed«, »available«, »recent«, »updates«, »extras«, »all« "
"und »repository« (Voreinstellung)."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--installed>"
msgstr "B<--installed>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Restrict query ONLY to installed pkgs - disables all repos and only acts on "
"rpmdb."
msgstr ""
"beschränkt die Abfrage auf installierte Pakete. Dadurch werden alle "
"Paketquellen deaktiviert und ausschließlich die RPM-Datenbank genutzt."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "GROUP QUERY OPTIONS"
msgstr "OPTIONEN ZUR GRUPPENABFRAGE"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Show general information about group."
msgstr "zeigt allgemeine Informationen zur Gruppe an."

#. type: Plain text
#: debian-buster debian-unstable
msgid "List packages belonging to (required by) group."
msgstr ""
"listet Pakete auf, die zu dieser Gruppe gehören (das heißt, die von dieser "
"Gruppe benötigt werden)."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--grouppkgs=WHAT>"
msgstr "B<--grouppkgs=WAS>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Specify what type of packages are queried from groups. Valid values for WHAT "
"are all, mandatory, default, optional."
msgstr ""
"gibt an, welche Pakettypen aus den Gruppen abgefragt werden sollten. "
"Zulässige Werte sind »all«, »mandatory«, »default« und »optional«."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<--requires>"
msgstr "B<--requires>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "List groups required by group."
msgstr "listet Gruppen auf, die von einer Gruppe benötigt werden."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "GROUP SELECTION OPTIONS"
msgstr "OPTIONEN ZUR GRUPPENAUSWAHL"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Query all available groups."
msgstr "fragt alle verfügbaren Gruppen ab."

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-g, --group>"
msgstr "B<-g, --group>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Query groups instead of packages."
msgstr "fragt Gruppen anstelle von Paketen ab."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "List all packages whose name contains 'perl':"
msgstr "Alle Pakete auflisten, deren Name »perl« enthält:"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<repoquery '*perl*'>"
msgstr "B<repoquery '*perl*'>"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "List all packages depending on openssl:"
msgstr "Alle Pakete auflisten, die von openssl abhängen:"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<repoquery --whatrequires openssl>"
msgstr "B<repoquery --whatrequires openssl>"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "List all package names and the repository they come from, nicely formatted:"
msgstr "Alle Paketnamen auflisten sowie die Paketquellen, aus denen Sie stammen, sauber formatiert:"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<repoquery -a --qf \"%-20{repoid} %{name}\">"
msgstr "B<repoquery -a --qf \"%-20{repoid} %{name}\">"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "List name and summary of all available updates (if any), nicely formatted:"
msgstr "Name und Zusammenfassung aller verfügbaren Aktualisierungen auflisten (falls verfügbar, sauber formatiert):"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<repoquery -a --pkgnarrow=updates --qf \"%{name}:\\en%{summary}\\en\">"
msgstr ""
"B<repoquery -a --pkgnarrow=updates --qf \"%{name}:\\en%{summary}\\en\">"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "List optional packages in base group:"
msgstr "Optionale Pakete in der »base«-Gruppe auflisten:"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<repoquery -g --grouppkgs=optional -l base>"
msgstr "B<repoquery -g --grouppkgs=optional -l base>"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "List build requirements from 'anaconda' source rpm:"
msgstr "Bauabhängigkeiten des Quellpakets »anaconda« auflisten:"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<repoquery --requires anaconda.src>"
msgstr "B<repoquery --requires anaconda.src>"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "List packages which BuildRequire gail-devel"
msgstr "Pakete auflisten, die zur Erstellung das Paket gail-devel benötigen:"

#. type: Plain text
#: debian-buster debian-unstable
#, no-wrap
msgid ""
"B<repoquery --archlist=src --whatrequires gail-devel>\n"
"  NB: This command will only work if you have repositories enabled which include srpms.\n"
msgstr ""
"B<repoquery --archlist=src --whatrequires gail-devel>\n"
"  Hinweis: Dieser Befehl funktioniert nur dann, wenn Paketquellen aktiviert sind, die Quellpakete enthalten.\n"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "MISC"
msgstr "SONSTIGES"

#. type: IP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<Specifying package names>"
msgstr "B<Angeben von Paketnamen>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "A package can be referred to in all queries with any of the following:"
msgstr ""
"Ein Paket kann in allen Abfragen aufgrund einer oder mehrerer der folgenden "
"Eigenschaften berücksichtigt werden:"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<name>"
msgstr "B<name>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<name.arch>"
msgstr "B<name.arch>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<name-ver>"
msgstr "B<name-ver>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<name-ver-rel>"
msgstr "B<name-ver-rel>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<name-ver-rel.arch>"
msgstr "B<name-ver-rel.arch>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<name-epoch:ver-rel.arch>"
msgstr "B<name-epoch:ver-rel.arch>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<epoch:name-ver-rel.arch>"
msgstr "B<epoch:name-ver-rel.arch>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "For example: B<repoquery -l kernel-2.4.1-10.i686>"
msgstr "Beispiel: B<repoquery -l kernel-2.4.1-10.i686>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Additionally wildcards (shell-style globs) can be used."
msgstr "Zusätzliche Platzhalter (Textschnipsel im Shell-Stil) sind zulässig."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"As repoquery uses YUM libraries for retrieving all the information, it "
"relies on YUM configuration for its default values like which repositories "
"to use. Consult YUM documentation for details:"
msgstr ""
"Da repoquery die Yum-Bibliotheken zum Ermitteln aller Informationen nutzt, "
"bezieht es die Standardwerte zu den zu verwendenden Paketquellen aus der Yum-"
"Konfiguration. Details hierzu finden Sie in der Yum-Dokumentation:"

#. type: Plain text
#: debian-buster debian-unstable
#, no-wrap
msgid ""
"/etc/yum.conf\n"
"/etc/yum/repos.d/\n"
"/var/cache/yum/\n"
msgstr ""
"/etc/yum.conf\n"
"/etc/yum/repos.d/\n"
"/var/cache/yum/\n"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

# FIXME Incorrect formatting s/I<yum.conf (5)>/I<yum.conf>(5)/
#. type: Plain text
#: debian-buster debian-unstable
#, no-wrap
msgid ""
"I<yum.conf (5)>\n"
"http://yum.baseurl.org/\n"
msgstr ""
"I<yum.conf>(5)\n"
"http://yum.baseurl.org/\n"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: debian-buster debian-unstable
#, no-wrap
msgid "See the Authors file included with this program.\n"
msgstr "Lesen Sie die mit diesem Programm gelieferte Autorenliste.\n"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"There are of course no bugs, but should you find any, you should first "
"consult the FAQ section on http://yum.baseurl.org/wiki/Faq and if "
"unsuccessful in finding a resolution contact the mailing list: yum-"
"devel@lists.baseurl.org.  To file a bug use http://bugzilla.redhat.com for "
"Fedora/RHEL/Centos related bugs and http://yum.baseurl.org/report for all "
"other bugs."
msgstr ""
"Es gibt natürlich keine Fehler, sollten Sie dennoch einen entdecken, ziehen "
"Sie zuerst den Abschnitt der häufig gestellten Fragen auf http://yum.baseurl."
"org/wiki/Faq zu Rate. Sollten Sie dort keine Lösung finden, kontaktieren Sie "
"die (englischsprachige) Mailingliste auf yum-devel@lists.baseurl.org. Für "
"Fehlermeldungen benutzen Sie http://bugzilla.redhat.com für Fehler in Fedora/"
"RHEL/Centos und http://yum.baseurl.org/report für sonstige Fehler."
