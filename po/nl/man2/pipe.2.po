# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2020-02-10 09:30+01:00\n"
"PO-Revision-Date: 2019-11-01 21:52+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: Dutch <debian-l10n-german@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 19.08.2\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "PIPE"
msgstr "PIPE"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2019-03-06"
msgstr "6 maart 2019"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programmeurs Handleiding"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "pipe, pipe2 - create pipe"
msgstr "pipe, pipe2 - maak pipe"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"/* On Alpha, IA-64, MIPS, SuperH, and SPARC/SPARC64; see NOTES */\n"
"B<struct fd_pair {>\n"
"B<long fd[2];>\n"
"B<};>\n"
"B<struct fd_pair pipe();>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"/* On all other architectures */\n"
"B<int pipe(int >I<pipefd>B<[2]);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
"B<#include E<lt>fcntl.hE<gt>>              /* Obtain O_* constant definitions */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<int pipe2(int >I<pipefd>B<[2], int >I<flags>B<);>\n"
msgstr "B<int pipe2(int >I<pipe_bes_ind>B<[2], int >I<vlaggen>B<);>\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"B<pipe>()  creates a pipe, a unidirectional data channel that can be used "
"for interprocess communication.  The array I<pipefd> is used to return two "
"file descriptors referring to the ends of the pipe.  I<pipefd[0]> refers to "
"the read end of the pipe.  I<pipefd[1]> refers to the write end of the "
"pipe.  Data written to the write end of the pipe is buffered by the kernel "
"until it is read from the read end of the pipe.  For further details, see "
"B<pipe>(7)."
msgstr ""
"B<pipe>() maakt tweeling bestandindicators, die wijzen naar een pijp-\"inode"
"\", en plaatst ze in de tabel waarI<bes_ind> naar wijst. I<bes_ind[0]> is om "
"te lezen, I<bes_ind[1]> is om te schrijven."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If I<flags> is 0, then B<pipe2>()  is the same as B<pipe>().  The following "
"values can be bitwise ORed in I<flags> to obtain different behavior:"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<O_CLOEXEC>"
msgstr "B<O_CLOEXEC>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Set the close-on-exec (B<FD_CLOEXEC>)  flag on the two new file "
"descriptors.  See the description of the same flag in B<open>(2)  for "
"reasons why this may be useful."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<O_DIRECT> (since Linux 3.4)"
msgstr "B<O_DIRECT> (sinds Linux 3.4)"

#.  commit 9883035ae7edef3ec62ad215611cb8e17d6a1a5d
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Create a pipe that performs I/O in \"packet\" mode.  Each B<write>(2)  to "
"the pipe is dealt with as a separate packet, and B<read>(2)s from the pipe "
"will read one packet at a time.  Note the following points:"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Writes of greater than B<PIPE_BUF> bytes (see B<pipe>(7))  will be split "
"into multiple packets.  The constant B<PIPE_BUF> is defined in I<E<lt>limits."
"hE<gt>>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If a B<read>(2)  specifies a buffer size that is smaller than the next "
"packet, then the requested number of bytes are read, and the excess bytes in "
"the packet are discarded.  Specifying a buffer size of B<PIPE_BUF> will be "
"sufficient to read the largest possible packets (see the previous point)."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Zero-length packets are not supported.  (A B<read>(2)  that specifies a "
"buffer size of zero is a no-op, and returns 0.)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Older kernels that do not support this flag will indicate this via an "
"B<EINVAL> error."
msgstr ""

#.  commit 0dbf5f20652108106cb822ad7662c786baaa03ff
#.  FIXME . But, it is not possible to specify O_DIRECT when opening a FIFO
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Since Linux 4.5, it is possible to change the B<O_DIRECT> setting of a pipe "
"file descriptor using B<fcntl>(2)."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<O_NONBLOCK>"
msgstr "B<O_NONBLOCK>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Set the B<O_NONBLOCK> file status flag on the open file descriptions "
"referred to by the new file descriptors.  Using this flag saves extra calls "
"to B<fcntl>(2)  to achieve the same result."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"On success, zero is returned.  On error, -1 is returned, I<errno> is set "
"appropriately, and I<pipefd> is left unchanged."
msgstr ""
"Bij succes wordt nul teruggegeven. Bij falen wordt -1 teruggegeven en wordt "
"I<errno> naar behoren gezet."

#.  http://austingroupbugs.net/view.php?id=467
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"On Linux (and other systems), B<pipe>()  does not modify I<pipefd> on "
"failure.  A requirement standardizing this behavior was added in "
"POSIX.1-2016.  The Linux-specific B<pipe2>()  system call likewise does not "
"modify I<pipefd> on failure."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<pipefd> is not valid."
msgstr "I<pipe_bes_ind> is ongeldig."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "(B<pipe2>())  Invalid value in I<flags>."
msgstr "(B<pipe2>()) Ongeldige waarde in I<vlaggen>."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr "Teveel bestandindicators worden door het proces gebruikt."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr "De grens aan het aantal open bestanden van het systeem is bereikt."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The user hard limit on memory that can be allocated for pipes has been "
"reached and the caller is not privileged; see B<pipe>(7)."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<pipe2>()  was added to Linux in version 2.6.27; glibc support is available "
"starting with version 2.9."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#.  See http://math-atlas.sourceforge.net/devel/assembly/64.psabi.1.33.ps.Z
#.  for example, section 3.2.1 "Registers and the Stack Frame".
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The SystemV ABI on some architectures allows the use of more than one "
"register for returning multiple values; several architectures (namely, "
"Alpha, IA-64, MIPS, SuperH, and SPARC/SPARC64)  (ab)use this feature in "
"order to implement the B<pipe>()  system call in a functional manner: the "
"call doesn't take any arguments and returns a pair of file descriptors as "
"the return value on success.  The glibc B<pipe>()  wrapper function "
"transparently deals with this.  See B<syscall>(2)  for information regarding "
"registers used for storing second file descriptor."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<pipe>(): POSIX.1-2001, POSIX.1-2008."
msgstr "B<pipe>(): POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<pipe2>()  is Linux-specific."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EXAMPLE"
msgstr "VOORBEELD"

#.  fork.2 refers to this example program.
#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The following program creates a pipe, and then B<fork>(2)s to create a child "
"process; the child inherits a duplicate set of file descriptors that refer "
"to the same pipe.  After the B<fork>(2), each process closes the file "
"descriptors that it doesn't need for the pipe (see B<pipe>(7)).  The parent "
"then writes the string contained in the program's command-line argument to "
"the pipe, and the child reads this string a byte at a time from the pipe and "
"echoes it on standard output."
msgstr ""

#. type: SS
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Program source"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>sys/wait.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>sys/wait.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int pipefd[2];\n"
"    pid_t cpid;\n"
"    char buf;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int pipefd[2];\n"
"    pid_t cpid;\n"
"    char buf;\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>stringE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>stringE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    if (pipe(pipefd) == -1) {\n"
"        perror(\"pipe\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (pipe(pipefd) == -1) {\n"
"        perror(\"pipe\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    cpid = fork();\n"
"    if (cpid == -1) {\n"
"        perror(\"fork\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    cpid = fork();\n"
"    if (cpid == -1) {\n"
"        perror(\"fork\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    if (cpid == 0) {    /* Child reads from pipe */\n"
"        close(pipefd[1]);          /* Close unused write end */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"        while (read(pipefd[0], &buf, 1) E<gt> 0)\n"
"            write(STDOUT_FILENO, &buf, 1);\n"
msgstr ""
"        while (read(pipefd[0], &buf, 1) E<gt> 0)\n"
"            write(STDOUT_FILENO, &buf, 1);\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"        write(STDOUT_FILENO, \"\\en\", 1);\n"
"        close(pipefd[0]);\n"
"        _exit(EXIT_SUCCESS);\n"
msgstr ""
"        write(STDOUT_FILENO, \"\\en\", 1);\n"
"        close(pipefd[0]);\n"
"        _exit(EXIT_SUCCESS);\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"    } else {            /* Parent writes argv[1] to pipe */\n"
"        close(pipefd[0]);          /* Close unused read end */\n"
"        write(pipefd[1], argv[1], strlen(argv[1]));\n"
"        close(pipefd[1]);          /* Reader will see EOF */\n"
"        wait(NULL);                /* Wait for child */\n"
"        exit(EXIT_SUCCESS);\n"
"    }\n"
"}\n"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<fork>(2), B<read>(2), B<socketpair>(2), B<splice>(2), B<tee>(2), "
"B<vmsplice>(2), B<write>(2), B<popen>(3), B<pipe>(7)"
msgstr ""
"B<fork>(2), B<read>(2), B<socketpair>(2), B<splice>(2), B<tee>(2), "
"B<vmsplice>(2), B<write>(2), B<popen>(3), B<pipe>(7)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON"

#. type: Plain text
#: archlinux debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 5.04 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#. type: Plain text
#: debian-unstable
msgid ""
"This page is part of release 5.05 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 5.05 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 5.01 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."
